.PHONY : test clean all default pqljar

PQLIB=../pqlib/pqlib.jar
PQLC=./dist/bootstrap/bin/javac -cp ${CLASSPATH}:${PQLIB}
MKDIR=mkdir

default: all

pqljar:
	${MKDIR} -p ./build/bootstrap/classes
	cp ${PQLIB} ./build/pqlib.jar
	cp ${PQLIB} ./build/bootstrap/pqlib.jar
	(cd build/bootstrap/classes; rm -rf edu/umass; jar xvf ../../pqlib.jar)

all:	pqljar
	ant -f make/build.xml
	cp ${PQLIB} ./dist/lib/pqlib.jar

clean:
	rm -f `find . -name "*.class"`

test:
	(cd pqltest; ./test-all.sh)

etest:
	(cd pqltest; ./test-all.sh eval)

sfailtest:
	(cd pqltest; ./test-all.sh sfail)

