/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package com.sun.tools.javac.comp.pqlcomp;

import java.util.*;

import com.sun.tools.javac.code.*;
import com.sun.tools.javac.jvm.*;
import com.sun.tools.javac.tree.*;
import com.sun.tools.javac.tree.pql.*;
import com.sun.tools.javac.util.*;
import com.sun.tools.javac.util.JCDiagnostic.DiagnosticPosition;
import com.sun.tools.javac.util.List;

import com.sun.tools.javac.code.Symbol.*;
import com.sun.tools.javac.tree.JCTree.*;
import com.sun.tools.javac.code.Type.*;

import com.sun.tools.javac.jvm.Target;

import com.sun.tools.javac.comp.Attr;
import com.sun.tools.javac.comp.PQLOptimiser.Annotation;

import static com.sun.tools.javac.code.Flags.*;
import static com.sun.tools.javac.code.Kinds.*;
import static com.sun.tools.javac.code.TypeTags.*;
import static com.sun.tools.javac.jvm.ByteCodes.*;

import edu.umass.pql.*;
import edu.umass.pql.Env;

public class ReductionJoinContext extends ExplicitlyStatefulSingleJoinContext
{
    // Limited functionality, so use with care!
    SimpleBuildContext b_init_stmts;
    SimpleBuildContext b_exit_stmts;

    Reductor reductor;
    ContinuationContextFactory inner_ccf;
    InitCommitter init_committer;
    String outer_label;

    boolean parallel;

    boolean is_toplevel_reduction;

    JCExpression latest_type; // FIXME: HACK
    JCExpression latest_default_value = null; // FIXME: HACK

    public ReductionJoinContext(ToClassTranslatorContext parent,
				ContinuationContextFactory ccf,
				Reductor[] reductors,
				boolean parallel)
    {
	super(parent, ccf);
	this.parallel = parallel;
	this.is_toplevel_reduction = isToplevelReduction(parent);

	if (reductors.length != 1)
	    throw new RuntimeException("inappropriate number of reductors: " + reductors.length);

	this.reductor = reductors[0];
	this.outer_label = this.freshLabel("npreduct");

	this.b_init_stmts = new SimpleBuildContext(this);
	this.b_exit_stmts = new SimpleBuildContext(this);
	this.init_committer = createInitCommitter(this.reductor, b_init_stmts, b_exit_stmts);

	this.inner_ccf = new ReductionContinuationContext.Factory(reductors, outer_label, init_committer);
    }

    boolean
    isToplevelReduction(ToClassTranslatorContext p)
    {
	if (p instanceof ToplevelJoinTranslatorContext)
	    return true;
	else
	    return false;
    }

    // @Override
    // public BasicBuildContext
    // createNewEvaluationState()
    // {
    // 	throw new UnsupportedOperationException();
    // }

    boolean closed_internally = false;
    boolean must_loop_over_body;

    @Override
    protected void
    closeInternal(StateBlock init, StateBlock eval)
    {
//	try {throw new RuntimeException(); } catch (Exception e) { e.printStackTrace(); }

// Is the below correct?
final String state_temp_var = b_init_stmts.freshLocal(b.int_type, b.id("state"));
//this.b_init_stmts.add(b.assign(state_temp_var, b.id("state"))
// this.b_exit_stmts.add(b.funapp("System.out.print", b.stringLiteral("[state] recovering state ")));
// this.b_exit_stmts.add(b.funapp("System.out.println", b.id(state_temp_var)));
this.b_exit_stmts.add(b.assign("state", b.id(state_temp_var)));

	this.b_exit_stmts.succeed(); // finish up


	final ListBuffer<JCTree> exits = this.b_exit_stmts.getBody();
	final ListBuffer<JCTree> inner_body = eval.code;
	eval.code = b_init_stmts.getBody();
	if (is_toplevel_reduction) {
	    init.code.add(b.assign("result", latest_default_value));
	} else {
	}
	eval.code.add(b.labelled(this.outer_label,
				 b.doLoop(b.booleanLiteral(this.must_loop_over_body),
					  b.block(b.flatStmtList(inner_body)))));
	eval.code.appendList(exits.toList());
	eval.is_terminated = false;
	this.getParentTranslatorContext().closeInternal(init, eval);
	this.closed_internally = true;
    }

    @Override
    public void
    doBuildJoin(Join j, Annotation annotation)
    {
	this.must_loop_over_body = !annotation.isLinear();
	annotation.translateJoin(this);
    }

    @Override
    public void
    close(Annotation a)
    {
	if (!closed_internally)
	    throw new RuntimeException("Either join was not built or child didn't call closeInternal()");
	// nop; we handle this in closeInternal()
    }

    // ================================================================================

    public JoinTranslatorContext
    newExplicitlyStatefulJoinTranslatorContext()
    {
	return new ImplicitlyStatefulSingleJoinContext(this, this.inner_ccf, true);
    }

    public JoinTranslatorContext
    newImplicitlyStatefulJoinTranslatorContext()
    {
	return new ExplicitlyStatefulSingleJoinContext(this, this.inner_ccf);
    }

    public JoinTranslatorContext
    newConjunctiveJoinTranslatorContext()
    {
	return new ConjunctiveJoinContext(this, this.inner_ccf);
    }

    // ================================================================================

    public InitCommitter
    createInitCommitter(Reductor r, SimpleBuildContext init, SimpleBuildContext exit)
    {
	final InitCommitter ic = new InitCommitter(init, exit);
	r.accept(ic);

	if (r.getInnerReductor() != null) {
	    ic.inner_reductor = createInitCommitter(r.getInnerReductor(), new SimpleBuildContext(this), null);
	    ic.inner_init_stmts = ic.inner_reductor.init_stmts;
	}

	return ic;
    }

    class InitCommitter implements edu.umass.pql.il.reductor.ReductorVisitor {
	JCExpression default_value;
	SimpleBuildContext init_stmts;
	SimpleBuildContext exit_stmts; // is null <=> translating inner reductor

	// The following is defined for the inner reductor, if present:
	SimpleBuildContext inner_init_stmts;

	InitCommitter inner_reductor;

	JCExpression container; // sets, maps, accumulators, ...

	boolean isInnerReductor()
	{
	    return this.exit_stmts == null;
	}

	public InitCommitter(SimpleBuildContext my_init_stmts, SimpleBuildContext my_exit_stmts)
	{
	    this.init_stmts = my_init_stmts;
	    this.exit_stmts = my_exit_stmts;
	}

	JCExpression getDefaultValue()
	{
	    return b.copy(this.default_value);
	}

	void
	setValueInit(String var_name, Object type_, JCExpression default_value)
	{
	    final JCExpression unboxed_type_expression;
	    final JCExpression boxed_type_expression;

	    // Very ugly
	    if (type_ instanceof Type) {
		final Type type = (Type) type_;
		unboxed_type_expression = b.typeExpr(type);
		boxed_type_expression = b.typeExpr(b.boxedType(type));
	    } else {
		boxed_type_expression = unboxed_type_expression = (JCExpression) type_;
	    }

	    if (!this.isInnerReductor()/* && latest_default_value == null*/) {
		latest_default_value = default_value;
	    }
	    if (is_toplevel_reduction)
		this.init_stmts.add(b.varWithInit(var_name, unboxed_type_expression, b.cast(boxed_type_expression, b.id("this.result"))/*default_value*/));
	    else
		this.init_stmts.add(b.varWithInit(var_name, unboxed_type_expression, b.cast(boxed_type_expression,default_value)));
	    this.container = b.id(var_name);
	    if (default_value == null)
		this.default_value = b.nullLiteral();
	    else
		this.default_value = default_value;
	}

	@Override
	public void
	visitForall(edu.umass.pql.il.reductor.Forall r)
	{
	    throw new UnsupportedReductionException("Unsupported reductor " + r);
	}

	@Override
	public void
	visitExists(edu.umass.pql.il.reductor.Exists r)
	{
	    throw new UnsupportedReductionException("Unsupported reductor " + r);
	}

	@Override
	public void
	visitArray(edu.umass.pql.il.reductor.ArrayReductor r)
	{
	    throw new UnsupportedOperationException();
	}

	@Override
	public void
	visitEquality(edu.umass.pql.il.reductor.EqualityReductor r)
	{
	    final String local_name = "eqvar_" + ReductionJoinContext.this.getFreshLocalVarNumber();
	    this.setValueInit(local_name, b.syms.objectType, b.nullLiteral());
	}

	void visitSimpleContainer(Reductor r, String field_prefix, int result_index, String type, JCExpression ... constructor_args)
	{
	    String local_name = field_prefix + ReductionJoinContext.this.getFreshLocalVarNumber();
	    this.setValueInit(local_name, b.id(type), b.newObjectStmt(b.id(type), constructor_args));

	    if (!this.isInnerReductor())
		exit_stmts.storeVar(r.getArg(result_index), b.id(local_name));
	}

	@Override
	public void
	visitDefaultMap(edu.umass.pql.il.reductor.DefaultMapReductor r)
	{
	    visitSimpleContainer(r, "dmap_", 3, "edu.umass.pql.container.PDefaultMap", init_stmts.loadVar(r.getArg(0)));
	}

	@Override
	public void
	visitMap(edu.umass.pql.il.reductor.MapReductor r)
	{
	    visitSimpleContainer(r, "map_", 2, "edu.umass.pql.container.PMap");
	}

	@Override
	public void
	visitSet(edu.umass.pql.il.reductor.SetReductor r)
	{
	    visitSimpleContainer(r, "set_", 1, "edu.umass.pql.container.PSet");
	}

	@Override
	public void
	visitMethodAdapter(edu.umass.pql.il.reductor.MethodAdapterReductor r)
	{
	    final String local_name = "accumulator_" + ReductionJoinContext.this.getFreshLocalVarNumber();
	    final Symbol.MethodSymbol method = (Symbol.MethodSymbol)r.getReductorObject();
	    final Type ty = method.type.asMethodType().restype;
	    final JCExpression variable_type = b.typeExpr(ty);

	    final JCExpression init_value;

	    if (ty.isPrimitive()) {
		switch (ty.tag) {
		case BYTE:
		    init_value = b.byteLiteral((Byte) method.getAttributeByType(b.syms.pqlDefaultValueByteClass));
		    break;
		case CHAR:
		    init_value = b.charLiteral((Character) method.getAttributeByType(b.syms.pqlDefaultValueCharClass));
		    break;
		case SHORT:
		    init_value = b.shortLiteral((Short) method.getAttributeByType(b.syms.pqlDefaultValueShortClass));
		    break;
		case INT:
		    init_value = b.intLiteral((Integer) method.getAttributeByType(b.syms.pqlDefaultValueIntegerClass));
		    break;
		case LONG:
		    init_value = b.longLiteral((Long) method.getAttributeByType(b.syms.pqlDefaultValueLongClass));
		    break;
		case FLOAT:
		    init_value = b.floatLiteral((Float) method.getAttributeByType(b.syms.pqlDefaultValueFloatClass));
		    break;
		case DOUBLE:
		    init_value = b.doubleLiteral((Double) method.getAttributeByType(b.syms.pqlDefaultValueDoubleClass));
		    break;
		case BOOLEAN:
		    init_value = b.booleanLiteral((Boolean) method.getAttributeByType(b.syms.pqlDefaultValueBooleanClass));
		    break;
		default:
		    throw new RuntimeException("Unsupported primitive type: " + ty);
		}
	    } else
		init_value = b.nullLiteral();

	    this.setValueInit(local_name, ty, init_value);
	    if (!this.isInnerReductor())
		this.exit_stmts.storeVar(r.getArg(1), b.id(local_name));
	}
    }

    @Override
    public ParallelAccess
    parallelAccessFields(Type counter_type)
    {
	if (this.parallel && this.getParentTranslatorContext().getParallelismSupported()) { // Parallelism!
	    this.getParentTranslatorContext().setParallelismDesired();
	    this.parallel = false; // Only provide parallel hooks for the first request
	    return new ParallelAccess("range_pos",
				      "range_stop");
	} else
	    return super.parallelAccessFields(counter_type);
    }

    /**
     * Disable parallelism support
     *
     * Any permissible parallelism is handled right here, so we hand out the parallel access ourselves.
     */
    @Override
    public boolean
    getParallelismSupported()
    {
	return false;
    }
}
