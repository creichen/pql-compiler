/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package com.sun.tools.javac.comp.pqlcomp;

import java.util.*;

import com.sun.tools.javac.code.*;
import com.sun.tools.javac.jvm.*;
import com.sun.tools.javac.tree.*;
import com.sun.tools.javac.tree.pql.*;
import com.sun.tools.javac.util.*;
import com.sun.tools.javac.util.JCDiagnostic.DiagnosticPosition;
import com.sun.tools.javac.util.List;

import com.sun.tools.javac.code.Symbol.*;
import com.sun.tools.javac.tree.JCTree.*;
import com.sun.tools.javac.code.Type.*;

import com.sun.tools.javac.jvm.Target;

import com.sun.tools.javac.comp.Attr;
import com.sun.tools.javac.comp.PQLOptimiser.Annotation;

import static com.sun.tools.javac.code.Flags.*;
import static com.sun.tools.javac.code.Kinds.*;
import static com.sun.tools.javac.code.TypeTags.*;
import static com.sun.tools.javac.jvm.ByteCodes.*;

import edu.umass.pql.*;
import edu.umass.pql.Env;

public class ExplicitlyStatefulSingleJoinContext extends ImplicitlyStatefulSingleJoinContext
{
    ArrayList<BasicBuildContext> gen_code;

    public ExplicitlyStatefulSingleJoinContext(ToClassTranslatorContext tctc, ContinuationContextFactory ccf)
    {
	super(tctc, ccf, false);
    }

    // In the explicitly stateful case, the eval state may handle multiple states.
    // We capture additional states in here.
    int last_init_state_id = -1;

    @Override
    public BasicBuildContext
    createNewEvaluationState()
    {
	// invoked during construction time so can't make it a regular initialiser
	if (gen_code == null)
	    gen_code = new ArrayList<BasicBuildContext>();

	BasicBuildContext bc = super.createNewEvaluationState();
	last_init_state_id = bc.getStateId();
	this.gen_code.add(bc);
	return bc;
    }

    @Override
    public void
    close(Annotation a)
    {
	// ordererd to skip this one?
	if (this.wasSkipped())
	    return;

	ListBuffer<JCTree> eval = this.eval.getBody();
	boolean eval_is_terminated = this.eval_detail.hasUnconditionalReturn();

	if (this.gen_code.size() > 2) {
	    // more than the two basic states?  Need switch
	    ListBuffer<JCCase> cases = new ListBuffer<JCCase>();
	    for (int i = 1; i < this.gen_code.size(); i++) {
		BasicBuildContext bc = this.gen_code.get(i);
		cases.add(b.switchCase(bc.getStateId(), bc.getBody().toList()));
	    }

	    eval = new ListBuffer<JCTree>();
	    eval.add(b.switchAnalysis(b.id("this.state"), cases.toList()));
	    eval_is_terminated = false;
	}

	final StateBlock eval_sb = new StateBlock(eval, this.eval.getStateId(), eval_is_terminated);
	// Had additional states?
	if (this.last_init_state_id >= 0)
	    eval_sb.addStateId(last_init_state_id);

	this.getParentTranslatorContext().closeInternal(this.init.getStateBlock(), eval_sb);
    }
}
