/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package com.sun.tools.javac.comp;

import java.util.*;

import com.sun.tools.javac.code.*;
import com.sun.tools.javac.jvm.*;
import com.sun.tools.javac.tree.*;
import com.sun.tools.javac.tree.pql.*;
import com.sun.tools.javac.util.*;
import com.sun.tools.javac.util.JCDiagnostic.DiagnosticPosition;
import com.sun.tools.javac.util.List;

import com.sun.tools.javac.code.Symbol.*;
import com.sun.tools.javac.tree.JCTree.*;
import com.sun.tools.javac.code.Type.*;

import com.sun.tools.javac.jvm.Target;

import com.sun.tools.javac.comp.pqlcomp.*;

import static com.sun.tools.javac.code.Flags.*;
import static com.sun.tools.javac.code.Kinds.*;
import static com.sun.tools.javac.code.TypeTags.*;
import static com.sun.tools.javac.jvm.ByteCodes.*;

import edu.umass.pql.*;
import edu.umass.pql.Env;

/**
 * Optimisation support for PQL query compilation
 *
 * Assumption:  PQLOptimiser is called on all joins of a query in a depth-first manner.
 */
public class PQLOptimiser
{
    Attr attr;
    JCTree base_tree;
    static int CBLOCKS = 0; // counter for conjunctive blocks (debug)

    public static final int IS_LINEAR_0		= PQLOptimiserAnalysis.IS_LINEAR_0;
    public static final int IS_LINEAR_1		= PQLOptimiserAnalysis.IS_LINEAR_1;
    public static final int IS_LINEAR_MAX_1	= PQLOptimiserAnalysis.IS_LINEAR_MAX_1;
    public static final int IS_NONLINEAR	= PQLOptimiserAnalysis.IS_NONLINEAR;

    public PQLOptimiser(Attr attr, JCTree tree_owner, PQLContext context)
    {
	this.attr = attr;
	this.base_tree = tree_owner;
	this.maker = attr.make;
	this.builder = new Builder(attr.names, attr.syms, attr.types, attr.make, context);
    }

    TreeMaker maker;
    Builder builder;

    PQLOptimiserAnalysis.IsLinearChecker linearity_checker = new PQLOptimiserAnalysis.IsLinearChecker();

    public Join
    annotateJoin(Join j)
    {
	j.accept(linearity_checker);
	final int linearity = linearity_checker.linearity;
	Annotation a = new Annotation(j, linearity);

	// ----------------------------------------
	a.close(); // FIXME: later on we'll only call this on demand
	// ----------------------------------------

	j.setDecoration(a);
	return j;
    }


    public class Annotation
    {
	// ----------------------------------------
	// phase 1: basic content storage
	public Join join;
	public int linearity_level;

	/**
	 * Do we know for certain that this join produces at most one binding?
	 */
	public boolean
	isLinear()
	{
	    return this.linearity_level <= IS_LINEAR_MAX_1;
	}

	/**
	 * Do we know for certain that this join will always succeed with exactly one binding?
	 */
	public boolean
	isGuaranteedSuccess()
	{
	    return this.linearity_level == IS_LINEAR_1;
	}

	public Annotation(Join j, int linearity)
	{
	    this.join = j;
	    this.linearity_level = linearity;
	}

	// ----------------------------------------
	// phase 2: code fragment generation: initialiser and evaluator (given a context)

	/**
	 * Construct the initialiser and evalutor for a given build context.  This method is invoked
	 * from a `close' through the BuildContext, either of a superior control structure or of the same Annotation instance.
	 */
	public void
	translateJoin(final JoinTranslatorContextFactory tc_factory)
	{
	    class TJV extends JoinVisitor {
		JoinTranslatorContext tc;
		Builder b;
		BuildContext eval;
		BuildContext init;

		void relinquishStates()
		{
		    if (this.tc != null)
			tc.relinquishStates(this.eval.getStateId(), this.init.getStateId());
		    this.tc = null;
		    this.eval = this.init = null;
		}

		void setup(JoinTranslatorContext tc)
		{
		    this.tc = tc;
		    b = tc.b;
		    eval = tc.eval;
		    init = tc.init;
		}

		public TJV()
		{
		    setup(tc_factory.newImplicitlyStatefulJoinTranslatorContext());
		}

		void
		makeExplicitlyStateful()
		{
		    this.relinquishStates();
		    setup(tc_factory.newExplicitlyStatefulJoinTranslatorContext());
		}


		@Override public void
		visitDefault(Join j) {
		    // tc.addToInitialiser(b.funapp("body.reset", tc.var_env()));
		    // tc.assertTrue(b.funapp("body.next", tc.var_env()));
		    System.err.println("OPT: Skipping>> " + j);
		    tc.skip();
		};

		public void
		visitBinop(int op, Join j)
		{
		    eval.unifyVar(j.getArg(2), b.binOp(op, eval.loadVar(j.getArg(0)), eval.loadVar(j.getArg(1))));
		}

		@Override public void
		visitAdd(Join j) {
		    visitBinop(JCTree.PLUS, j);
		}

		@Override public void
		visitMul(Join j) {
		    visitBinop(JCTree.MUL, j);
		}

		@Override public void
		visitSub(Join j) {
		    visitBinop(JCTree.MINUS, j);
		}

		@Override public void
		visitDiv(Join j) {
		    JCExpression temp = eval.loadIntoTemp(j.getArg(1));
		    eval.assertFalse(b.isEqual(temp, b.literalZeroForVar(j.getArg(1))));
		    eval.unifyVar(j.getArg(2), b.binOp(JCTree.DIV, eval.loadVar(j.getArg(0)), temp));
		}

		@Override public void
		visitMod(Join j) {
		    JCExpression temp = eval.loadIntoTemp(j.getArg(1));
		    eval.assertFalse(b.binOp(JCTree.LE, temp, b.literalZeroForVar(j.getArg(1))));
		    eval.unifyVar(j.getArg(2), b.binOp(JCTree.MOD, eval.loadVar(j.getArg(0)), temp));
		}

		@Override public void
		visitBitOr(Join j) {
		    visitBinop(JCTree.BITOR, j);
		}

		@Override public void
		visitBitAnd(Join j) {
		    visitBinop(JCTree.BITAND, j);
		}

		@Override public void
		visitBitXor(Join j) {
		    visitBinop(JCTree.BITXOR, j);
		}

		@Override public void
		visitBitShl(Join j) {
		    visitBinop(JCTree.SL, j);
		}

		@Override public void
		visitBitShr(Join j) {
		    visitBinop(JCTree.SR, j);
		}

		@Override public void
		visitBitSshr(Join j) {
		    visitBinop(JCTree.USR, j);
		}

		@Override public void
		visitTrue(Join j) {
		    eval.succeed();
		}

		@Override public void
		visitFalse(Join j) {
		    eval.fail();
		}

		@Override public void
		visitEq(Join j) {
		    if (Env.isWildcard(j.getArg(1)))
			return;
		    if (Env.isWriteVar(j.getArg(1))) {
			eval.storeVar(j.getArg(1), eval.loadVarImprecise(j.getArg(0)));
		    } else
			// read var:
			eval.assertTrue(b.binOp(JCTree.EQ, eval.loadVarImprecise(j.getArg(0)), eval.loadVarImprecise(j.getArg(1))));
		}

		@Override public void
		visitEqEquals(Join j) {
		    if (Env.isWildcard(j.getArg(1)))
			return;
		    if (Env.isWriteVar(j.getArg(1))) {
			eval.storeVar(j.getArg(1), eval.loadVarImprecise(j.getArg(0)));
		    } else
			// read var:
			eval.assertTrue(b.methodapp(eval.loadVarImprecise(j.getArg(0)), "equals", eval.loadVarImprecise(j.getArg(1))));
		}

		@Override public void
		visitNeq(Join j) {
		    eval.assertFalse(b.binOp(JCTree.EQ, eval.loadVar(j.getArg(0)), eval.loadVar(j.getArg(1))));
		}

		@Override public void
		visitNeqEquals(Join j) {
		    eval.assertFalse(b.methodapp(eval.loadVar(j.getArg(0)), "equals", eval.loadVar(j.getArg(1))));
		}

		@Override public void
		visitLt(Join j) {
		    eval.assertTrue(b.binOp(JCTree.LT, eval.loadVar(j.getArg(0)), eval.loadVar(j.getArg(1))));
		}

		@Override public void
		visitLte(Join j) {
		    eval.assertTrue(b.binOp(JCTree.LE, eval.loadVar(j.getArg(0)), eval.loadVar(j.getArg(1))));
		}


		public void
		visitUnop(int op, Join j)
		{
		    eval.unifyVar(j.getArg(1), b.unOp(op, eval.loadVar(j.getArg(0))));
		}

		@Override public void
		visitBitInv(Join j) {
		    visitUnop(JCTree.COMPL, j);
		}

		@Override public void
		visitNeg(Join j) {
		    visitUnop(JCTree.NEG, j);
		}



		@Override public void
		visitSetSize(Join j) {
		    eval.unifyVar(j.getArg(1), b.methodapp(eval.loadVar(j.getArg(0)), "size"));
		}

		@Override public void
		visitMapSize(Join j) {
		    eval.unifyVar(j.getArg(1), b.methodapp(eval.loadVar(j.getArg(0)), "size"));
		}

		@Override public void
		visitArraySize(Join j) {
		    eval.unifyVar(j.getArg(1), b.field(eval.loadVar(j.getArg(0)), "length"));
		}



		@Override public void
		visitField(edu.umass.pql.il.Field fld) {
		    final JCFieldAccess fldref = (JCFieldAccess) fld.getField();
		    eval.unifyVar(fld.getArg(1), b.field(eval.loadVar(fld.getArg(0)), fldref.name.toString()));
		}

		@Override public void
		visitCoerceChar(Join j) {
		    eval.unifyVar(j.getArg(1), b.cast(b.syms.charType, eval.loadVar(j.getArg(0))));
		}

		@Override public void
		visitCoerceShort(Join j) {
		    eval.unifyVar(j.getArg(1), b.cast(b.syms.shortType, eval.loadVar(j.getArg(0))));
		}

		@Override public void
		visitCoerceByte(Join j) {
		    eval.unifyVar(j.getArg(1), b.cast(b.syms.byteType, eval.loadVar(j.getArg(0))));
		}

		@Override public void
		visitCoerceBoolean(Join j) {
		    final JCExpression body;

		    switch (b.typeOf(j.getArg(0)).tag) {
		    case BOOLEAN:
			body = eval.loadVar(j.getArg(0));
			break;
		    case INT:
		    case SHORT:
		    case BYTE:
		    case LONG:
		    case CHAR:
			// not sure if this can actually arise...
			body =  b.conditional(b.isEqual(b.intLiteral(0),
							eval.loadVar(j.getArg(0))),
					      b.booleanLiteral(false),
					      b.booleanLiteral(true)
					      );
			break;
		    default:
			body = eval.loadVar(j.getArg(0));
		    }
		    eval.unifyVar(j.getArg(1), body);
		}

		@Override public void
		visitCoerceFloat(Join j) {
		    eval.unifyVar(j.getArg(1), b.cast(b.syms.floatType, eval.loadVar(j.getArg(0))));
		}

		void
		visitRange(final int store, final ExpressionLoader lower, final ExpressionLoader upper)
		{
		    if (Env.isReadVar(store)) {
			JCExpression store_v = eval.loadIntoTemp(store);
			eval.assertTrue(b.binOp(JCTree.AND,
						b.binOp(JCTree.LE, lower.load(eval), b.copy(store_v)),
						b.binOp(JCTree.LE, store_v, upper.load(eval))));
		    } else {
			final JCExpression lower_expr = lower.load(init);
			final JCExpression upper_expr = upper.load(init);
			JoinTranslatorContext.ParallelAccess pa = tc.parallelAccessFields(upper.getType(b));
			final String counter_field = pa.count_var;//tc.freshField(b.typeOf(store), null);
			final String upper_field = pa.max_var;//tc.freshField(b.typeOf(upper), null);

			init.add(b.assign(counter_field, lower_expr));
			init.add(b.assign(upper_field, upper_expr));

			eval.ifThenElse(b.isEqual(b.id(upper_field), b.id(counter_field)),
					new Construction() { // true
					    public void construct (BuildContext bc) {
						bc.add(b.assign(upper_field, b.intLiteral(0))); // last entry
						bc.storeVar(store, b.id(counter_field));
						bc.add(b.assign(counter_field, b.intLiteral(1)));
					    }},
					new Construction() { // false
					    public void construct (BuildContext bc) {
						bc.ifThenElse(b.binOp(JCTree.LE, b.id(counter_field), b.id(upper_field)),
							      new Construction() { public void construct(BuildContext bc) {
								  bc.storeVar(store, b.unOp(JCTree.POSTINC, b.id(counter_field)));
							      }},
							      new Construction() { public void construct(BuildContext bc) {
								  bc.fail();
							      }}
							      );
					    }}
					);
		    }
		}

		@Override public void
		visitRangeContains(Join j) {
		    // PARALLELISABLE
		    final int store = j.getArg(2);
		    final int lower = j.getArg(0);
		    final int upper = j.getArg(1);

		    visitRange(store, eval.loadVarDeferred(lower), eval.loadVarDeferred(upper));
		}


		@Override public void
		visitSetContains(Join j) {
		    // CONDITIONALLY PARALLELISABLE
		    final int store = j.getArg(1);
		    final int set = j.getArg(0);

		    if (Env.isReadVar(store) && !Env.isWildcard(store)) {
			eval.assertTrue(b.methodapp(eval.loadVar(set), "contains", eval.loadVar(store)));
		    } else {
			makeExplicitlyStateful();
			final String PSET_TYPE = "edu.umass.pql.container.PSet";
			final String iterator_var = tc.freshField(b.id("java.util.Iterator"), null);
			final String pset_var = tc.freshField(b.arrayType(b.id("java.lang.Object")), null);
			JoinTranslatorContext.ParallelAccess pa = tc.parallelAccessFields(attr.syms.intType);
			final String pset_counter_var = pa.count_var;//tc.freshField(b.typeExpr(attr.syms.intType), null);
			final String pset_size_var = pa.max_var;//tc.freshField(b.typeExpr(attr.syms.intType), null);

			final BuildContext pset_ctx = tc.newEvaluationState();

			final Type cast_type = b.typeOf(store);

			String[] varname_ref = new String[1];

			final JCExpression basic_set = init.loadIntoTemp(set);
			init.ifThenElse(b.isInstanceOf(basic_set, b.id(PSET_TYPE)),
					new Construction() { public void construct(BuildContext bc) {
					    bc.add(b.assign(pset_var, b.methodapp(b.cast(b.id(PSET_TYPE), basic_set), "getRepresentation")));
					    bc.add(b.assign(pset_counter_var, b.intLiteral(0)));
					    bc.add(b.assign(pset_size_var, b.binOp(JCTree.MINUS,
										   b.id(pset_var + ".length"),
										   b.intLiteral(1))));
					    bc.setState(pset_ctx);
					}},
					new Construction() { public void construct(BuildContext bc) {
					    bc.add(b.assign(iterator_var, b.methodapp(bc.loadVar(set), "iterator")));
					    bc.setState(eval);
					}});

			{ // iterator-based
			    final JCExpression getter = b.methodapp(b.id(iterator_var), "next");

			    eval.ifThenElse(b.methodapp(b.id(iterator_var), "hasNext"),
					    new Construction() { public void construct(BuildContext bc) {
						bc.storeVar(store, b.castBoxed(cast_type, getter));
					    }},
					    new Construction() { public void construct(BuildContext bc) {
						bc.fail();
					    }});
			    eval.succeed();
			}

			{ // indexing-based
			    final String result_temp = pset_ctx.freshLocal(b.boxedType(cast_type), null);
			    // here is the PARALLELISABLE part
			    pset_ctx.whileDo(b.binOp(JCTree.LE, b.id(pset_counter_var), b.id(pset_size_var)),
					     new Construction() { public void construct(BuildContext bc) {
						 bc.add(b.assign(result_temp, b.castBoxed(cast_type, b.index(pset_var, b.unOp(JCTree.POSTINC, b.id(pset_counter_var))))));
						 bc.ifThen(b.isNotEqual(b.nullLiteral(), b.id(result_temp)),
							   new Construction() { public void construct(BuildContext bc) {
							       bc.storeVar(store, b.id(result_temp));
							       bc.succeed();
							   }});
					     }});
			    pset_ctx.fail();
			}
		    }
		}

		abstract class Expander
		{
		    abstract JCExpression expand(BuildContext bc, JCExpression expr);
		}

		abstract class TwoExpander
		{
		    abstract JCExpression expand(BuildContext bc, JCExpression expr, JCExpression expr2);
		}

		/* For a `map[key] = value' join, this handles two cases: (a) value is a wildcard, (b) `key' is read-only. */
		boolean
		handleSimpleContainerCases(final int container, final int key, final int value, boolean range_constraints,
					   final Expander compute_size, TwoExpander compute_index)
		{
		    if (Env.isWildcard(value)) {
			//tc.skip();
			// simple int range gen, re-use existing code
			ExpressionLoader upper_loader = new ExpressionLoader() {
				@Override public Type getType(Builder b) { return b.syms.intType; }
				@Override public JCExpression load(BuildContext ctx) { return b.binOp(JCTree.MINUS,
												      compute_size.expand(eval, ctx.loadVar(container)),
												      b.intLiteral(1)); }
			    };
			visitRange(key, new ExpressionLoader.ConstantLoader(b.intLiteral(0)), upper_loader);
			return true;
		    } else if (Env.isReadVar(key)) {
			// trivial lookup
			final JCExpression container_e = eval.loadIntoTemp(container);
			final JCExpression key_e = eval.loadIntoTemp(key);
			// eval.assertTrue(b.binOp(JCTree.NE, b.copy(key_e), b.nullLiteral()));
			// eval.assertTrue(b.binOp(JCTree.NE, b.copy(container_e), b.nullLiteral()));
			if (range_constraints) {
			    eval.assertTrue(b.binOp(JCTree.LE, b.intLiteral(0), key_e));
			    eval.assertTrue(b.binOp(JCTree.LT, b.copy(key_e), compute_size.expand(eval, container_e)));
			}
			eval.unifyVar(value, compute_index.expand(eval, b.copy(container_e), b.copy(key_e)));
			return true;
		    } else
			return false;
		}

		@Override
		public void
		visitArrayLookup(Join j)
		{
		    // MOSTLY PARALLELISABLE
		    final int array = j.getArg(0);
		    final int key = j.getArg(1);
		    final int value = j.getArg(2);

		    if (!(handleSimpleContainerCases(array, key, value, true, new Expander() {
			    public JCExpression expand(BuildContext bc, JCExpression expr) {
				return b.field(expr, "length"); }
			},
				new TwoExpander() {
				    public JCExpression expand(BuildContext bc, JCExpression base, JCExpression index) { return b.index(base, index); }
				}
				))) {
			// must iterate
			makeExplicitlyStateful();

			final String array_field = tc.freshField(b.typeOf(array), null);
			init.add(b.assign(array_field, init.loadVar(array)));

			final JCExpression lower_expr = b.intLiteral(0);
			final JCExpression upper_expr = b.binOp(JCTree.MINUS,
								b.field(b.id(array_field), "length"),
								b.intLiteral(1));
			JoinTranslatorContext.ParallelAccess pa = tc.parallelAccessFields(b.syms.intType);
			final String counter_field = pa.count_var;//tc.freshField(b.typeOf(store), null);
			final String upper_field = pa.max_var;//tc.freshField(b.typeOf(upper), null);

			init.add(b.assign(upper_field, upper_expr));
			init.add(b.assign(counter_field, lower_expr));

			JCExpression range_check = b.binOp(JCTree.LE, b.id(counter_field), b.id(upper_field));

			if (Env.isReadVar(value)) {
			    // slow search
			    final String expected_val = tc.freshField(b.typeOf(value), null);
			    init.add(b.assign(expected_val, init.loadVar(value)));
				
			    eval.whileDo(range_check,
					 new Construction() { public void construct(BuildContext bc) {
					     String val = bc.freshLocal(b.typeOf(value), b.index(b.id(array_field), b.unOp(JCTree.POSTINC, b.id(counter_field))));
					     bc.ifThen(b.isEqual(b.id(val), b.id(expected_val)),
						       new Construction() { public void construct(BuildContext bc) {
							   if (!Env.isWildcard(key))
							       bc.storeVar(key, b.binOp(JCTree.MINUS, b.id(counter_field), b.intLiteral(1)));
							   bc.succeed();
						       }});
					 }});
			    eval.fail();
			} else {
			    eval.assertTrue(range_check);
			    eval.unifyVar(key, b.id(counter_field));
			    eval.unifyVar(value, b.index(b.id(array_field), b.unOp(JCTree.POSTINC, b.id(counter_field))));
					     // eval.add(b.funapp("System.err.print", b.stringLiteral(counter_field)));
					     // eval.add(b.funapp("System.err.print", b.stringLiteral("=")));
					     // eval.add(b.funapp("System.err.println", b.id(counter_field)));
					     // eval.add(b.funapp("System.err.print", b.stringLiteral("state")));
					     // eval.add(b.funapp("System.err.print", b.stringLiteral("=")));
					     // eval.add(b.funapp("System.err.println", b.id("state")));

			}
		    }
		}

		@Override
		public void
		visitMapLookup(Join j)
		{
		    // MOSTLY CONDITIONALLY PARALLELISABLE
		    final int map = j.getArg(0);
		    final int key = j.getArg(1);
		    final int value = j.getArg(2);

		    if (!(handleSimpleContainerCases(map, key, value, false, new Expander() { public JCExpression expand(BuildContext bc, JCExpression expr) {
			return b.methodapp(expr, "size"); }},
				new TwoExpander() {
				    public JCExpression expand(BuildContext bc, JCExpression base, JCExpression index) {
					final Type type = b.boxedType(b.typeOf(value));
					final String tempvar_name = bc.freshLocal(type, b.castBoxed(type, b.methodapp(base, "get", index)));
					bc.assertFalse(b.isEqual(b.id(tempvar_name), b.nullLiteral()));
					return b.copy(b.id(tempvar_name));					}
				}
				))) {
			makeExplicitlyStateful();
		    	    // otherwise:
		    	    // `key' must be written to
		    	    // `value' is not a wildcard

			final Type map_type = b.typeOf(map);
			final Type key_type = b.typaramOf(map_type, 0, b.typeOf(key));
			final Type value_type = b.typaramOf(map_type, 1, b.typeOf(value));

			final String PMAP_TYPE = "edu.umass.pql.container.PMap";
			//tc.skip();
			final String iterator_field = tc.freshField(b.parameterisedType("java.util.Iterator", b.id("java.util.Map.Entry")), null);
			final String array_field = tc.freshField(b.arrayType(b.id("java.lang.Object")), null);
			final String pmap_counter_var = tc.freshField(b.typeExpr(attr.syms.intType), null);
			final String pmap_size_var = tc.freshField(b.typeExpr(attr.syms.intType), null);
			final String value_checker_var;

			if (Env.isReadVar(value))
			    value_checker_var = tc.freshField(b.typeExpr(value_type), null);
			else
			    value_checker_var = null;

			final BuildContext pmap_ctx = tc.newEvaluationState();

			final JCExpression basic_map = init.loadIntoTemp(map);
			init.ifThenElse(b.isInstanceOf(basic_map, b.id(PMAP_TYPE)),
					new Construction() { public void construct(BuildContext bc) {
					    bc.add(b.assign(array_field, b.methodapp(b.cast(b.id(PMAP_TYPE), basic_map), "getRepresentation")));
					    bc.add(b.assign(pmap_counter_var, b.intLiteral(0)));
					    bc.add(b.assign(pmap_size_var, b.id(array_field + ".length")));

					    if (Env.isReadVar(value)) // searching for particular value?
						bc.add(b.assign(b.id(value_checker_var), bc.loadVar(value)));

					    bc.setState(pmap_ctx);
					}},
					new Construction() { public void construct(BuildContext bc) {
					    bc.add(b.assign(iterator_field, b.methodapp(b.methodapp(init.loadVar(map), "entrySet"), "iterator")));
					    bc.setState(eval);
					}});
					    
			{ // eval: old-fashioned serial
			    eval.assertTrue(b.methodapp(b.id(iterator_field), "hasNext"));
			    final String kvp = eval.freshLocal(b.id("java.util.Map.Entry"), b.methodapp(b.id(iterator_field), "next"));

			    eval.storeVar(key, b.castBoxed(key_type, b.methodapp(b.id(kvp), "getKey")));
			    eval.unifyVar(value, b.castBoxed(value_type, b.methodapp(b.id(kvp), "getValue")));
			}

			{ // pmap_ctx: PMap access
			    final String result_key_temp = pmap_ctx.freshLocal(b.boxedType(key_type), null);
			    // here is the PARALLELISABLE part
			    pmap_ctx.whileDo(b.binOp(JCTree.LT, b.id(pmap_counter_var), b.id(pmap_size_var)),
					     new Construction() { public void construct(BuildContext bc) {
						 bc.add(b.assign(result_key_temp, b.castBoxed(key_type, b.index(array_field, b.id(pmap_counter_var)))));
						 bc.add(b.assignOp(JCTree.PLUS_ASG, b.id(pmap_counter_var), b.intLiteral(2)));

						 final Construction store_key_and_value_and_finish = 
						     new Construction() { public void construct(BuildContext bc) {
							 if (!Env.isWildcard(key))
							     bc.storeVar(key, b.id(result_key_temp));
							 bc.storeVar(value, b.castBoxed(value_type, b.index(array_field, b.binOp(JCTree.MINUS, b.id(pmap_counter_var), b.intLiteral(1)))));
							 bc.succeed();
						     }};
						     
						 bc.ifThen(b.isNotEqual(b.nullLiteral(), b.id(result_key_temp)),
							   new Construction() { public void construct(BuildContext bc) {
							       if (Env.isReadVar(value)) // searching for particular value?
								   // succeed conditionally
								   bc.ifThen(b.isEqual(b.id(value_checker_var), b.id(result_key_temp)),
									     store_key_and_value_and_finish);
							       else
								   // succeed unconditionally
								   store_key_and_value_and_finish.construct(bc);
							   }});
					     }});
			    pmap_ctx.fail();
			}
		    }
		}

		@Override
		public void
		visitJavaType(edu.umass.pql.il.Type.JAVA_TYPE jty)
		{
		    if (!Env.isWildcard(jty.getArg(0)))
			if (Env.isWriteVar(jty.getArg(0)))
			    throw new RuntimeException("Selecting all objects of a given type not currently supported: " + jty);
			else
			    eval.assertTrue(b.isInstanceOf(eval.loadVar(jty.getArg(0)), b.typeExpr((Type) jty.getType())));
		}

		@Override
		public void
		visitTypeChar(Join j)
		{
		    visitRange(j.getArg(0),
			       new ExpressionLoader.ConstantLoader(b.charLiteral(Character.MIN_VALUE)),
			       new ExpressionLoader.ConstantLoader(b.charLiteral(Character.MAX_VALUE)));
		}

		@Override
		public void visitTypeShort(Join j)
		{
		    visitRange(j.getArg(0),
			       new ExpressionLoader.ConstantLoader(b.intLiteral(Short.MIN_VALUE)),
			       new ExpressionLoader.ConstantLoader(b.intLiteral(Short.MAX_VALUE)));
		}

		@Override
		public void visitTypeByte(Join j)
		{
		    visitRange(j.getArg(0),
			       new ExpressionLoader.ConstantLoader(b.byteLiteral(Byte.MIN_VALUE)),
			       new ExpressionLoader.ConstantLoader(b.byteLiteral(Byte.MAX_VALUE)));
		}

		@Override
		public void visitTypeInt(Join j)
		{
		    visitRange(j.getArg(0),
			       new ExpressionLoader.ConstantLoader(b.intLiteral(Integer.MIN_VALUE)),
			       new ExpressionLoader.ConstantLoader(b.intLiteral(Integer.MAX_VALUE)));
		}

		@Override
		public void visitTypeBoolean(Join j)
		{
		    if (!Env.isWildcard(j.getArg(0)) && Env.isReadVar(j.getArg(0))) {
			eval.assertTrue(b.binOp(JCTree.AND,
						b.binOp(JCTree.GE, eval.loadVar(j.getArg(0)), b.intLiteral(0)),
						b.binOp(JCTree.LE, eval.loadVar(j.getArg(1)), b.intLiteral(1))));
		    } else {
			makeExplicitlyStateful();
			final BuildContext false_test_ctx = tc.newEvaluationState();
			final BuildContext final_ctx = tc.newEvaluationState();

			eval.storeVar(j.getArg(0), b.booleanLiteral(true));
			eval.setState(false_test_ctx);
			eval.succeed();

			false_test_ctx.storeVar(j.getArg(0), b.booleanLiteral(false));
			eval.setState(final_ctx);
			false_test_ctx.succeed();

			final_ctx.fail();
		    }
		}

		// ----------------------------------------
		// Control structures are no-ops, because they are handled through ctx magic (cf. `Attribute.close()', below
		@Override
		public void
		visitConjunctiveBlock(final AbstractBlock.Conjunctive pre_block)
		{
		    this.relinquishStates();
		    setup(tc_factory.newConjunctiveJoinTranslatorContext());

		    final JCExpression self_expr = tc.getJoinExpressionForSelf();

		    if (!(tc instanceof ConjunctiveJoinContext))
			throw new RuntimeException("ConjunctiveBlock should be translated with a ConjunctiveJoinContext (unless you added something new and cool?)");

		    for (int i = 0; i < pre_block.getComponentsNr(); i++) {
			Join j = pre_block.getComponent(i);
			tc.setJoinExpressionForSelf(b.methodapp(b.cast(b.id("edu.umass.pql.ControlStructure"),
								       b.copy(self_expr)),
								"getComponent",
								b.intLiteral(i)));
			tc.buildJoin(j);
		    }
		    tc.setJoinExpressionForSelf(self_expr);
		}


		// // FIXME: The `Not' implementation works (if I'm not mistaken) fo the linear body case, but not
		// // for nonlinear bodies.  Reason is that a nonlinear body, if it fails on the first try, should
		// // make `Not' succeed exactly once, not multiple times.
		//
		// @Override
		// public void
		// visitNot(final edu.umass.pql.il.Not not_block)
		// {
		//     relinquishStates();
		//     final ContinuationContextFactory ccf = tc_factory.getContinuationContextFactory();
		//     final JCExpression self_expr = tc_factory.getJoinExpressionForSelf();
		//     final Builder _builder = b;

		//     tc_factory.setJoinExpressionForSelf(b.methodapp(b.cast(b.id("edu.umass.pql.ControlStructure"),
		// 							   b.copy(self_expr)),
		// 						    "getComponent",
		// 						    b.intLiteral(0)));

		//     tc_factory.addContinuationContextOverride(new ContinuationContextOverride() {
		// 	    public ContinuationContext
		//     	    override(final ContinuationContext old_context)
		// 	    {
		// 		return new ContinuationContext() {
		// 		    public
		//     		    JCStatement
		//     		    failStmt()
		// 		    {
		// 			return old_context.succeedStmt();
		// 		    }

		// 		    public
		//     		    JCStatement
		//     		    succeedStmt()
		// 		    {
		// 			return old_context.failStmt();
		// 		    }

		// 		    public
		//     		    JCStatement
		//     		    returnStmt(JCExpression e)
		// 		    {
		// 			return old_context.returnStmt(_builder.negate(e));
		// 		    }

		// 		    public
		//     		    JCStatement
		//     		    assertTrueStmt(JCExpression e)
		// 		    {
		// 			return _builder.ifThen(_builder.negate(e),
		// 					       old_context.succeedStmt());
		// 		    }

		// 		    public
		//     		    JCStatement
		//     		    assertFalseStmt(JCExpression e)
		// 		    {
		// 			return _builder.ifThen(e,
		// 					       old_context.succeedStmt());
		// 		    }
		// 		};
		// 	    }
		// 	});
		//     ((Annotation) (not_block.getComponent(0).getDecoration())).translateJoin(tc_factory);
		// 	//tc_factory.buildJoin(not_block.getComponent(0));

		//     tc_factory.setJoinExpressionForSelf(self_expr);
		//     tc_factory.setContinuationContextFactory(ccf);
		// }

		// ============================================================
		// Reductions

		@Override
		public void
		visitReduction(Reduction reduction)
		{
		    try {
			this.relinquishStates();
			if (tc_factory.getParallelismSupported())
			    setup(tc_factory.newParallelReductionJoinContext(reduction.getReductors()));
			else
			    setup(tc_factory.newNonParallelReductionJoinContext(reduction.getReductors()));

			if (reduction.getComponentsNr() != 1)
			    throw new UnsupportedReductionException("# of components = " + reduction.getComponentsNr());

			final JCExpression self_expr = tc.getJoinExpressionForSelf();
			tc.setJoinExpressionForSelf(b.methodapp(b.cast(b.id("edu.umass.pql.ControlStructure"),
								       b.copy(self_expr)),
								"getComponent",
								b.intLiteral(0)));
			this.tc.buildJoin(reduction.getComponent(0));
			tc.setJoinExpressionForSelf(self_expr);
		    } catch (UnsupportedReductionException r) {
			System.err.println("Skipping reduction " + reduction + ": " + r.toString());
			// `un-relinquish' states
			setup(tc_factory.newImplicitlyStatefulJoinTranslatorContext());
			this.visitDefault(reduction);
		    }
		}

	    };
	    TJV tjv = new TJV();
	    join.accept(tjv);

	    // We might be recursing on tc_factory, e.g., via `Not'; in that case, tc will be null.
	    if (tjv.tc != null)
		tjv.tc.close(this);
	}

	// ----------------------------------------
	// phase 2b: join class generation

	public JCParens simple_compilation_plug = null; // pointer to the part into which we must plug the simple_compilation (traditional Joins) during completeCompilation
	public JCExpression opt_result = null; // optimised representation, if present (and all contained in here)
	public boolean result_is_parallel = false; // determine whether the result is tagged as parallel

	public boolean
	resultIsParallel()
	{
	    return result_is_parallel;
	}

	/**
	 * Set the simple_compilation_result and simple_compilation_plug, if appropriate.
	 *
	 */
	public void
	close()
	{
	    // final Builder _builder = builder;
	    // final Attr _attr = attr;
	    // final Annotation _annotation = this;

	    // final Join join = PQLFactory.flattenBlock(this.join);

	    VarSet inputs = new VarSet();
	    VarSet outputs = new VarSet();
	    this.join.addActualReads(inputs, null);
	    this.join.addWriteDependencies(outputs);
	    outputs.removeAll(inputs);

	    final OptFlags opt_flags = new OptFlags(inputs, outputs);

	    ToplevelJoinTranslatorContext tcc = new ToplevelJoinTranslatorContext(PQLOptimiser.this.builder, opt_flags);
	    PQLOptimiser.this.builder.setOptFlags(opt_flags, tcc);
	    this.translateJoin(tcc);
	    tcc.writeToAnnotation(attr, this);

	    if (!opt_flags.is_all_inlined) {
		// Oops, whatever we generated is broken.  Try again!
		opt_flags.minimise_env_access = false;
		tcc = new ToplevelJoinTranslatorContext(PQLOptimiser.this.builder, opt_flags);
		this.translateJoin(tcc);
		tcc.writeToAnnotation(attr, this);
	    }

	    // join.accept(new JoinVisitor(){

	    // 	    @Override public void
	    // 	    visitDefault(Join j)
	    // 	    {
	    // 		JoinTranslatorContext tc = new SingleJoinContext(_builder);
	    // 		tc.close(_attr, _annotation);
	    // 	    }

	    // 	    // --------------------------------------------------------------------------------
	    // 	    // Control structures
	    // 	    @Override
	    // 	    public void
	    // 	    visitConjunctiveBlock(final AbstractBlock.Conjunctive block)
	    // 	    {
	    // 		ConjunctiveJoinContext ctx = new ConjunctiveJoinContext(_builder);
	    // 		ctx.close(_attr, _annotation);
	    // 	    }
	    // 	});
	}

	// ----------------------------------------
	// phase 3: completing the compilation
	// This phase is always executed but may be trivial if phase 3 was skipped.

	public JCExpression
	completeCompilation(JCExpression simple_compilation_result)
	{
	    if (simple_compilation_plug != null)
		simple_compilation_plug.expr = simple_compilation_result;

	    if (this.opt_result != null)
		return this.opt_result;
	    else
		return simple_compilation_result;
	}
    }
}
