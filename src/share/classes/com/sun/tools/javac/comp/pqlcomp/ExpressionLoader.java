/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package com.sun.tools.javac.comp.pqlcomp;

import java.util.*;

import com.sun.tools.javac.code.*;
import com.sun.tools.javac.jvm.*;
import com.sun.tools.javac.tree.*;
import com.sun.tools.javac.tree.pql.*;
import com.sun.tools.javac.util.*;
import com.sun.tools.javac.util.JCDiagnostic.DiagnosticPosition;
import com.sun.tools.javac.util.List;

import com.sun.tools.javac.code.Symbol.*;
import com.sun.tools.javac.tree.JCTree.*;
import com.sun.tools.javac.code.Type.*;

import com.sun.tools.javac.jvm.Target;

import com.sun.tools.javac.comp.Attr;
import com.sun.tools.javac.comp.PQLOptimiser.Annotation;

import static com.sun.tools.javac.code.Flags.*;
import static com.sun.tools.javac.code.Kinds.*;
import static com.sun.tools.javac.code.TypeTags.*;
import static com.sun.tools.javac.jvm.ByteCodes.*;

import edu.umass.pql.*;
import edu.umass.pql.Env;

public abstract class ExpressionLoader
{
    public abstract JCExpression
    load(BuildContext ctx);

    public abstract Type
    getType(Builder b);

    public static class VarLoader extends ExpressionLoader
    {
	int var_id;

	public VarLoader(int var_id)
	{
	    this.var_id = var_id;
	}

	@Override
	public JCExpression
	load(BuildContext ctx)
	{
	    return ctx.loadVar(this.var_id);
	}

	@Override
	public Type
	getType(Builder b)
	{
	    return b.typeOf(this.var_id);
	}
    }

    public static class ConstantLoader extends ExpressionLoader
    {
	JCExpression expr;
	boolean used_once = false;

	public
	ConstantLoader(JCExpression expr)
	{
	    this.expr = expr;
	}

	@Override
	public JCExpression
	load(BuildContext ctx)
	{
	    if (this.used_once)
		return ctx.b.copy(this.expr);
	    else {
		this.used_once = true;
		return this.expr;
	    }
	}

	@Override
	public Type
	getType(Builder b)
	{
	    return this.expr.type;
	}

    }
}

