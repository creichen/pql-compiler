/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package com.sun.tools.javac.comp.pqlcomp;

import java.util.*;

import com.sun.tools.javac.code.*;
import com.sun.tools.javac.jvm.*;
import com.sun.tools.javac.tree.*;
import com.sun.tools.javac.tree.pql.*;
import com.sun.tools.javac.util.*;
import com.sun.tools.javac.util.JCDiagnostic.DiagnosticPosition;
import com.sun.tools.javac.util.List;

import com.sun.tools.javac.code.Symbol.*;
import com.sun.tools.javac.tree.JCTree.*;
import com.sun.tools.javac.code.Type.*;

import com.sun.tools.javac.jvm.Target;

import com.sun.tools.javac.comp.PQLOptimiser.Annotation;

import static com.sun.tools.javac.code.Flags.*;
import static com.sun.tools.javac.code.Kinds.*;
import static com.sun.tools.javac.code.TypeTags.*;
import static com.sun.tools.javac.jvm.ByteCodes.*;

import edu.umass.pql.*;
import edu.umass.pql.Env;


public abstract class BuildContext
{
    protected ListBuffer<JCTree> code = new ListBuffer<JCTree>();
    protected int state_id;

    public
    BuildContext(Builder b, int state_id)
    {
	this.b = b;
	this.state_id = state_id;
    }

    public StateBlock
    getStateBlock()
    {
	return new StateBlock(this.getBody(), state_id, false);
    }

    // ================================================================================
    // Public API

    public Builder b;

    public void
    setState(BuildContext ctx)
    {
	add(b.assign("this.state", b.intLiteral(ctx.getStateId())));
    }

    /**
     * Add a statement or expression to the build context
     */
    public void
    add(JCTree statement)
    {
	beforeAdd();
	this.code.add(statement);
    }

    abstract void
    prependStmt(JCTree t);

    public String
    freshLocal(Type t, JCExpression initialiser)
    {
	return freshLocal(b.typeExpr(t), initialiser);
    }

    public abstract String
    freshLocal(JCExpression t, JCExpression initialiser);

    public int
    getStateId()
    {
	return this.state_id;
    }

    public JCExpression
    loadVar(int var_id)
    {
	final String f = b.getVarFieldName(var_id);
	if (f == null)
	    return b.cast(b.typeOf(var_id), loadVarImprecise(var_id));
	else
	    return b.id(f);
    }

    public JCExpression
    loadVarBoxed(int var_id)
    {
	final String f = b.getVarFieldName(var_id);
	if (f == null)
	    return b.castBoxedCanonical(b.typeOf(var_id), loadVarImprecise(var_id));
	else
	    return b.castBoxedCanonical(b.typeOf(var_id), b.id(f));
    }

    public ExpressionLoader
    loadVarDeferred(int var)
    {
	return new ExpressionLoader.VarLoader(var);
    }

    public abstract JCExpression
    loadVarImprecise(int var_id);

    public abstract void
    storeVar(int var_id, JCExpression value);


    public JCExpression
    loadIntoTemp(int var)
    {
	final JCExpression expr = loadVar(var);
	if (expr instanceof JCIdent)
	    return expr;
	else {
	    String tempvar_name = freshLocal(b.varInfo(var).type, expr);
	    return b.id(tempvar_name);
	}
    }

    public void
    unifyVar(int var, JCExpression expr)
    {
	if (Env.isWildcard(var))
	    return;

	if (Env.isWriteVar(var))
	    storeVar(var, expr);
	else {
	    System.err.println("Asserting true for unification within " + this.getClass() + " on " + expr);
	    assertTrue(b.isEqual(loadVar(var),
				 expr));
	}
    }

    public abstract void
    assertTrue(JCExpression expr);

    public abstract void
    assertFalse(JCExpression expr);

    public abstract void
    fail();

    public abstract void
    succeed();

    /**
     * Close context and retrieve code
     */
    public abstract ListBuffer<JCTree>
    getBody();

    public abstract void
    ifThenElse(JCExpression cond,
	       Construction truebranch,
	       Construction falsebranch);

    public abstract void
    ifThen(JCExpression cond,
	   Construction truebranch);

    public abstract void
    whileDo(JCExpression cond,
	    Construction body);

    // ================================================================================
    // Implementer API

    abstract void
    setContinuationContext(ContinuationContext cctx);

    abstract ContinuationContext
    getContinuationContext();

    protected void
    beforeAdd() {}
}