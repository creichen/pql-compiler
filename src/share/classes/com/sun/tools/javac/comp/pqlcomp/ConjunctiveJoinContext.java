/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package com.sun.tools.javac.comp.pqlcomp;

import java.util.*;

import java.io.StringWriter;
import java.io.PrintWriter;

import com.sun.tools.javac.code.*;
import com.sun.tools.javac.jvm.*;
import com.sun.tools.javac.tree.*;
import com.sun.tools.javac.tree.pql.*;
import com.sun.tools.javac.util.*;
import com.sun.tools.javac.util.JCDiagnostic.DiagnosticPosition;
import com.sun.tools.javac.util.List;

import com.sun.tools.javac.code.Symbol.*;
import com.sun.tools.javac.tree.JCTree.*;
import com.sun.tools.javac.code.Type.*;

import com.sun.tools.javac.jvm.Target;

import com.sun.tools.javac.comp.Attr;
import com.sun.tools.javac.comp.PQLOptimiser.Annotation;

import static com.sun.tools.javac.code.Flags.*;
import static com.sun.tools.javac.code.Kinds.*;
import static com.sun.tools.javac.code.TypeTags.*;
import static com.sun.tools.javac.jvm.ByteCodes.*;

import edu.umass.pql.*;


// --------------------------------------------------------------------------------
// Translates a sequence of components
public class ConjunctiveJoinContext extends DefaultJoinTranslatorContext
{
    public static final boolean DEBUG_CONJUNCTIVELOOP_GEN = false;

    int component_counter = 0;

    @Override
    public ParallelAccess
    parallelAccessFields(Type counter_type)
    {
	if (component_counter == 1 && this.getParentTranslatorContext().getParallelismSupported()) { // Parallelism!
	    this.getParentTranslatorContext().setParallelismDesired();
	    return new ParallelAccess("range_pos",
				      "range_stop");
	} else
	    return super.parallelAccessFields(counter_type);
    }

    @Override
    public boolean
    getParallelismSupported()
    {
	return false;
    }

    // ================================================================================
    static class ReferenceBlockBuffers
    {
	final static boolean SUCCESS = true;
	final static boolean FAILURE = false;

	public ListBuffer<JCBlock> success_refs = new ListBuffer<JCBlock>();
	public ListBuffer<JCBlock> failure_refs = new ListBuffer<JCBlock>();

	ListBuffer<JCBlock>
	get(boolean success_or_failure)
	{
	    if (success_or_failure == SUCCESS)
		return this.success_refs;
	    else
		return this.failure_refs;
	}
    }

    // --------------------------------------------------------------------------------
    public static class ConjunctiveContinuationContextFactory extends ContinuationContextFactory
    {
	public ReferenceBlockBuffers refs_buffers = new ReferenceBlockBuffers();

	public ReferenceBlockBuffers
        getAndResetReferenceBlockBuffers()
	{
	    final ReferenceBlockBuffers retval = this.refs_buffers;
	    this.refs_buffers = new ReferenceBlockBuffers();
	    return retval;
	}

	public ContinuationContext
	createContinuationContext(Builder builder,
				  JoinTranslatorContext jctx,
				  BuildContext bctx)
	{
	    return new ConjunctiveContinuationContext(builder);
	}

	JCBlock
	allocateBlockIn(Builder b, ListBuffer<JCBlock> buffer)
	{
	    final JCBlock block = b.block();
	    buffer.add(block);
	    return block;
	}

	class ConjunctiveContinuationContext implements ContinuationContext
	{
	    Builder b;
	    public ConjunctiveContinuationContext(Builder builder)
	    {
		this.b = builder;
	    }

	    private JCStatement
	    maybeDebug(JCStatement stmt, String msg)
	    {
		if (DEBUG_CC)
		    return b.block(b.stmt(b.stringLiteral("cjc:" + msg)),
				   stmt);
		else
		    return stmt;
	    }


	    @Override
	    public
	    JCStatement
	    failStmt(String msg)
	    {
		return maybeDebug(allocateBlockIn(b, ConjunctiveContinuationContextFactory.this.refs_buffers.failure_refs),
				  "cjc-f:" + msg);
	    }

	    @Override
	    public
	    JCStatement
	    succeedStmt(String msg)
	    {
		// String trace = "";
		// try {
		//     throw new RuntimeException();
		// } catch (RuntimeException e) {
		//     StringWriter sw = new StringWriter();
		//     PrintWriter pw = new PrintWriter(sw);
		//     e.printStackTrace(pw);
		//     trace = sw.toString().replace('\n', ';');
		// }
		return maybeDebug(allocateBlockIn(b, ConjunctiveContinuationContextFactory.this.refs_buffers.success_refs),
				  "cjc-s:" /*+ trace + ":"*/ + msg);
	    }

	    @Override
	    public
	    JCStatement
	    assertTrueStmt(JCExpression e, String msg)
	    {
		return maybeDebug(b.ifThen(b.negate(e),
					   this.failStmt("#cjc-aT:" + msg)
					   ),
				  "cjc-aT:" + msg);
	    }

	    @Override
	    public
	    JCStatement
	    assertFalseStmt(JCExpression e, String msg)
	    {
		return maybeDebug(b.ifThen(e,
					   this.failStmt("#cjc-aF:" + msg)
					   ),
				  "cjc-aF:" + msg);
	    }


	    @Override
	    public JCStatement
            returnStmt(JCExpression e, String msg)
	    {
		return maybeDebug(b.ifThenElse(e, this.succeedStmt("cjc-r0:" + msg), this.failStmt("cjc-r1:" + msg)),
				  "cjc-r:" + msg);
	    }
	}
    }

    class ConjunctiveComponent
    {
	StateBlock init_sb;
	StateBlock eval_sb;
	String label = freshLabel("conjunctive_" + (component_counter++));
	boolean implicit_state_management;
	boolean interpreted = false; // Is this intepreted?
	Annotation annotation;

	ReferenceBlockBuffers refs_buffers = null;

	public void
    	setReferenceBlockBuffers(ReferenceBlockBuffers rb)
	{
	    this.refs_buffers = rb;
	}

	public StateBlock
    	getInitContext()
	{
	    return this.init_sb;
	}

	public StateBlock
    	getEvalContext()
	{
	    return this.eval_sb;
	}

	public boolean
	hasIndeterminateStateAfterInit()
	{
	    return (!this.implicit_state_management)
		&& eval_sb.getStateId() != eval_sb.getMaxStateId();
	}

	public int
    	getMaxState()
	{
	    return this.eval_sb.getMaxStateId();
	}

    	private void
    	subst(boolean success_or_failure, List<JCStatement> stmts)
	{
	    for (JCBlock bb : refs_buffers.get(success_or_failure)) {
		bb.stats = b.copyList(stmts);
	    }
	}

    	public void
    	substSuccesses(List<JCStatement> stmts)
	{
	    subst(ReferenceBlockBuffers.SUCCESS, stmts);
	}

    	public void
    	substFailures(List<JCStatement> stmts)
	{
	    subst(ReferenceBlockBuffers.FAILURE, stmts);
	}
    }

    ArrayList<ConjunctiveComponent> components = new ArrayList<ConjunctiveComponent>();
    ArrayList<BasicBuildContext> all_current_build_state_contexts = 
	new ArrayList<BasicBuildContext> ();


    protected final int ABORT_STATE = -1;
    protected int last_init_state = ABORT_STATE;
    protected int current_init_state = ABORT_STATE;

    protected ListBuffer<String> joins_that_we_need_to_clone = new ListBuffer<String>();
    protected HashMap<String, JCExpression> join_access_for_joins_to_clone = new HashMap<String, JCExpression>();

    private ConjunctiveComponent current_component = null;
    private ListBuffer<JCTree> pre_init_statements = new ListBuffer<JCTree>(); // statements executed before everything else

    private ConjunctiveContinuationContextFactory ccc_factory;

    public ConjunctiveJoinContext(ToClassTranslatorContext context, ContinuationContextFactory ccf)
    {
	super(context, ccf);
	this.ccc_factory = new ConjunctiveContinuationContextFactory();

	nextComponent();
    }

    @Override
    public BasicBuildContext
    createNewEvaluationState()
    {
	throw new RuntimeException("Mustn't be called.  Instead, create a new inferior context with (e.g.) newImplicitlyStatefulJoinTranslatorContext and create states on that.");
    }

    @Override
    void
    overrideAllCurrentBuildContextContinuations(ContinuationContextOverride override)
    {
	for (BasicBuildContext ctx : this.all_current_build_state_contexts)
	    ctx.overrideContinuationContext(override);
    }

    protected void
    closeCurrentComponent()
    {
	if (this.current_component != null) {
	    this.current_component.implicit_state_management = this.implicit_state_management;
	    this.current_component.setReferenceBlockBuffers(ccc_factory.getAndResetReferenceBlockBuffers());
	    this.components.add(this.current_component);
	}
    }

    private void
    nextComponent()
    {
	this.all_current_build_state_contexts.clear();

	final int old_last_init_state = this.last_init_state; // debugging
	this.last_init_state = this.current_init_state;

	this.current_component = new ConjunctiveComponent();
	// this.init = null;
	// this.eval = null;

	this.implicit_state_management = true;
    }

    boolean is_first_component = true;

    @Override
    public void
    doBuildJoin(Join j, Annotation annotation)
    {
	if (!is_first_component)
	    nextComponent();
	is_first_component = false;

	this.current_component.annotation = annotation;
	if (annotation == null)
	    throw new RuntimeException("Missing annotation in join");
	annotation.translateJoin(this);
    }

    boolean
    componentHasImplicitStateManagement(int id)
    {
	return this.components.get(id).implicit_state_management;
    }

    boolean
    componentIsLinear(int id)
    {
	return //componentHasImplicitStateManagement(id) &&
	    this.components.get(id).annotation.isLinear();
    }

    // --------------------

    boolean implicit_state_management = true;

    private JoinTranslatorContext
    setFactoryToCCC(JoinTranslatorContext ctx)
    {
	final ContinuationContext cctx_init = ccc_factory.createContinuationContext(this.b, this, ctx.init);
	ctx.init.setContinuationContext(cctx_init);
	final ContinuationContext cctx_eval = ccc_factory.createContinuationContext(this.b, this, ctx.eval);
	ctx.eval.setContinuationContext(cctx_eval);
	ctx.setContinuationContextFactory(ccc_factory);
	return ctx;
    }

    @Override
    public JoinTranslatorContext
    newImplicitlyStatefulJoinTranslatorContext()
    {
	this.implicit_state_management = false;//true;
	// We use the explicit context, since our handling of statefulness differs
	return new ImplicitlyStatefulSingleJoinContext(this, ccc_factory, false);
    }

    @Override
    public JoinTranslatorContext
    newExplicitlyStatefulJoinTranslatorContext()
    {
	this.implicit_state_management = false;
	return new ExplicitlyStatefulSingleJoinContext(this, ccc_factory);
    }

    @Override
    public JoinTranslatorContext
    newConjunctiveJoinTranslatorContext()
    {
	return this;
    }

    @Override
    public JoinTranslatorContext
    newNonParallelReductionJoinContext(Reductor[] reductors)
    {
	return new ReductionJoinContext(this, ccc_factory, reductors, false);
    }

    @Override
    public JoinTranslatorContext
    newParallelReductionJoinContext(Reductor[] reductors)
    {
	return new ReductionJoinContext(this, ccc_factory, reductors, true);
    }

    // --------------------------------------------------------------------------------
    // close
    protected void
    closeInternal(StateBlock init, StateBlock eval)
    {
	this.current_component.init_sb = init;
	this.current_component.eval_sb = eval;
	// if (!eval.isTerminated()) {
	//     try {throw new RuntimeException("not-terminated:  " + eval.code);}
	//     catch (RuntimeException e) {
	// 	e.printStackTrace();
	//     }
	// }
	this.closeCurrentComponent();
    }

    protected void
    closeInternalSkip()
    {
	// Child expression couldn't build opt code, so let's interpret it
	final JCExpression join_access = this.getJoinExpressionForSelf();

	String fname = this.freshField(b.id("edu.umass.pql.Join"), // null);
				       join_access);

	this.joins_that_we_need_to_clone.add(fname);
	this.join_access_for_joins_to_clone.put(fname, join_access);

	final ListBuffer<JCTree> init_detail = new ListBuffer<JCTree>();
	final SimpleBuildContext eval_detail_bctx = new SimpleBuildContext(this);
	final ListBuffer<JCTree> eval_detail = eval_detail_bctx.getCode();

	VarSet read_vars = new VarSet();
	VarSet write_vars = new VarSet();
	final Join join = this.current_component.annotation.join;
	join.addReadDependencies(read_vars);
	join.addWriteDependencies(write_vars);
	// first(1/2), copy all required read variables back into env (for the initialiser part)
	for (int i = 0; i < join.getArgsNr(); i++) {
	    int var = join.getArg(i);
	    if (Env.isReadVar(var) && !this.b.varInfo(var).isConstant()) {
		init_detail.add(this.b.assign(this.b.envVarAccess(var),
					      eval_detail_bctx.loadVar(var)));
	    }
	}
	// first(2/2), copy all required read variables back into env (for the iterative part)
	for (int var : read_vars) {
	    if (!this.b.varInfo(var).isConstant()) {
		eval_detail.add(this.b.assign(this.b.envVarAccess(var),
					      eval_detail_bctx.loadVar(var)));
	    }
	}
	// now add code to initialise:
	init_detail.add(b.methodapp(b.id(fname), "reset", b.id("env")));
	// now add code to iterate:
	eval_detail.add(ccc_factory.createContinuationContext
			(this.b, this, eval_detail_bctx).assertTrueStmt(b.methodapp(b.id(fname),
										    "next", b.id("env")),
									"cjc:closeInternalSkip"
									));
	// finally add code to read out any changed variables:
	for (int var : write_vars) {
	    if (!this.b.varInfo(var).isConstant()) {
		eval_detail.add(this.b.assign(eval_detail_bctx.loadVar(var),
					      this.b.envVarAccess(var)));
	    }
	}

	this.current_component.init_sb = new StateBlock(init_detail, this.allocateStateId(), false);
	this.current_component.eval_sb = new StateBlock(eval_detail, this.allocateStateId(), false);
	this.current_component.interpreted = true;

	this.closeCurrentComponent();
    }

    @Override
    public void
    close(Annotation a)
    {
	// refactor me...

	// make successes in the last statement point to final success state
	if (this.components.size() < 2)
	    throw new RuntimeException("Too few components: " + this.components.size());

	// Basic translation scheme:
	// For states { i0, s0, i1, s1, ... in, sn } we build the following [SIMPLIFIED]:
	//
	// while (state > 0) {
	//   while (state <= (2n + 1)) {
	//     ...
	//       while (state <= 3) {
	//         while (state <= 1) {
	//           if (state == 0) { i0; state = 1; }
	//           s0 [fail <- { state = -1; return false}; success <- {state = 3; i1 /* no longer true: this is in `init' now */; break;}];
	//         }
	//         s1 [fail <- { state = 1; continue}; success <- {state = 5; i2; break;}];
	//       }
	//     ...
	//   }
	//   sn [fail <- { state  = 2n-1; continue }; success <- { return true; }]
	// }
	// return false;

	/*
	 * More generally, the translation constructs loops from first join (innermost) to last
	 * join (outermost), passing on `success_handler' and `failure_handler' code (effectively,
	 * those are continuations).
	 * What exactly a success handler and a failure handler contain depends on several factors.
	 * FAILURE:
	 * - For the first join, failure is always `return false, set state to ABORT_STATE'.
	 * SUCCESS:
	 * - For the last join, success is always `revert to most recent non-linear state, return true'
	 * (The most recent non-linear state may be the ABORT_STATE.)
	 * Success and failure in all other cases is defined as follows:
	 * FAILURE:
	 * - Revert to most recent non-linear state.  If there is none, set to ABORT_STATE, return false.
	 * SUCCESS:
	 * - Initialise next state.  If next state is not linear and implicitly managed, set `state'
	 *   variable to next state ID.  If current state is not linear, break out of loop.
	 *
	 * Relevant variables:
	 *  body_in_loop :  boolean // current body in loop
	 *  most_recent_nonlinear_state : JCExpression
	 *  current_body : JCStatement
	 *  succes_handler : List<JCStatement>
	 *  failure_handler : List<JCStatement>
	 */

	List<? extends JCStatement> init_list = null; // initialiser code to be put into `init'

	boolean at_least_one_component_is_nonlinear = false;
	for (int c = 0; c < this.components.size(); c++)
	    if (!componentIsLinear(c)) {
		at_least_one_component_is_nonlinear = true;
		break;
	    }

	final String state_var = "state"; // FIXME: make local

	JCExpression most_recent_nonlinear_state = b.intLiteral(ABORT_STATE);
	boolean need_outer_loop = false;

	// // -- debug							\
	// System.err.println("implicit/linear/multistate:");
	// for (int component_nr = 0; component_nr < this.components.size(); component_nr++)
	// 	System.err.println("\t" + component_nr
	// 			   + ":" + componentHasImplicitStateManagement(component_nr)
	// 			   + " / " + this.components.get(component_nr).annotation.isLinear()
	// 			   + " / " + this.components.get(component_nr).hasIndeterminateStateAfterInit()
	// 			   + " in " + this.components.get(component_nr).annotation.join);
	// // -- debug /

    	JCStatement current_body;
	boolean current_body_in_loop = false;


	// initialise init_list
	{
	    // if anything, we should reset to the init state here (which used to be 0).  However, removing the below works fine:
	    // this.pre_init_statements.add(b.assign(state_var, b.intLiteral(0)));
	    ListBuffer<JCTree> body = this.pre_init_statements;
	    body.appendList(this.components.get(0).getInitContext().getCode().toList());
	    init_list = b.stmts(b.<JCTree>copyList(body.toList()));
	}

	// initialise current body
	{
	    current_body = b.block(b.flatStmtList(b.ifThen(b.isEqual(b.id(state_var), b.intLiteral(ABORT_STATE)),
							   b.block(b.flatStmtList(this.genFailStmts())))
						  // b.ifThen(b.isEqual(b.id(state_var), b.intLiteral(0)),
						  // 	       b.block(body.toList()))
						  ));
	}

	List<JCStatement> success_handler;

	// { state = -1; return false};
	List<JCStatement> failure_handler =
	    b.flatStmtList(b.assign(state_var, b.intLiteral(ABORT_STATE)),
			   this.genFailStmts());

	int return_state_nr = ABORT_STATE;

	for (int component_nr = 0; component_nr < this.components.size(); component_nr++) {
	    ConjunctiveComponent component = this.components.get(component_nr);

	    String next_label = null;
	    String state_backup_var =
		component.hasIndeterminateStateAfterInit() ? this.freshField(b.syms.intType, null) : null; // fixme: make local

	    // success_handler: start
	    final JCStatement backup_state_stmt = state_backup_var == null ? null : b.stmt(b.assign(state_backup_var, b.id(state_var)));
	    if (component_nr + 1 < this.components.size()) {
		// not the final join

		final ConjunctiveComponent next_component = this.components.get(component_nr + 1);
		final StateBlock next_init = next_component.getInitContext();

		if (componentIsLinear(component_nr)) {
		    success_handler =
			b.flatStmtList(next_init.getCode() // no break
				       );
		} else {
		    // {state = ...; i1; break;}
		    success_handler =
			b.flatStmtList(
				       backup_state_stmt,
				       next_init.getCode(),
				       (componentHasImplicitStateManagement(component_nr + 1) && !componentIsLinear(component_nr + 1))
				       ? b.assign(state_var, b.intLiteral(next_component.getEvalContext().getStateId()))
				       : null,
				       component.annotation.isLinear() ? null : b.breakStmt(component.label)
				       );
		    if (component.implicit_state_management && !component.annotation.isLinear())
			success_handler = b.flatStmtList(
							 b.assign(state_var, b.intLiteral(next_init.getStateId())),
							 success_handler);
		    next_label = next_component.label;
		}
	    } else {
		// final join
		JCTree adjust_state = null;
		if (component.annotation.isLinear())
		    adjust_state = b.assign(b.id(state_var), most_recent_nonlinear_state);
		else {
		    // if nonlinear, we still MUST back up our state

		    // multiple eval contexts => we have a backup state var AND state is already set, so we're fine.
		    if (!component.hasIndeterminateStateAfterInit()) // hence, otherwise we do:
			adjust_state = b.assign(b.id(state_var), b.intLiteral(component.getEvalContext().getStateId()));

		    // if (backup_state_stmt)
		    //     adjust_state = backup_state_stmt; // back up state into join-specific storage
		}
		success_handler = b.flatStmtList(//b.stmt(b.funapp("System.out.println", b.id("state"))),
						 backup_state_stmt,
						 adjust_state,
						 this.genSucceedStmts()
						 ); // { return true; } // (or equiv.)
	    }
	    // success_handler: end

	    // substitute as appropriate
	    component.substSuccesses(success_handler);
	    component.substFailures(failure_handler);
	    // construct loop and body

	    if (DEBUG_CONJUNCTIVELOOP_GEN) {
		System.err.println("================================================================================");
		System.err.println("Handling: " + component.annotation.join);
		System.err.println("--- SUCCESS: ----------------------------------------");
		System.err.println(success_handler);
		System.err.println("--- FAILURE: ----------------------------------------");
		System.err.println(failure_handler);
	    }
				   

	    // eval_body: start
	    Object eval_body;
	    eval_body = component.getEvalContext().getCode();

	    // most-recent-non-linear-state \
	    if (!component.annotation.isLinear()) {
		if (state_backup_var == null)
		    most_recent_nonlinear_state = b.intLiteral(component.getEvalContext().getStateId());
		else
		    most_recent_nonlinear_state = b.id(state_backup_var);
	    }
	    // most-recent-non-linear-state /

	    current_body =
		b.block(b.flatStmtList(/*b.funapp("System.out.print", b.intLiteral(component_nr)),
					 b.funapp("System.out.print", b.stringLiteral(":")),
					 b.funapp("System.out.print", b.id("state")),
					 b.funapp("System.out.print", b.stringLiteral(":")),
					 b.funapp("System.out.print", b.intLiteral(C)),
					 b.funapp("System.out.print", b.stringLiteral("-")),
					 b.funapp("System.out.print", b.stringLiteral(":")),
					 b.funapp("System.out.println", b.stringLiteral("")),*/
				       current_body,
				       eval_body,
				       //b.stringLiteral("stdsuccess:"),
				       component.getEvalContext().isTerminated() ? null : success_handler));
	    JCExpression body_entry_condition = b.binOp(JCTree.LE, b.id(state_var), b.intLiteral(component.getMaxState()));
	    if (componentIsLinear(component_nr)) {
		// one-shot component
		current_body =
		    b.ifThen(body_entry_condition,
			     b.block(b.flatStmtList(current_body/*,
								  success_handler*/)));

		current_body_in_loop = false;
					   
	    } else {
		return_state_nr = component.getEvalContext().getStateId();
		// loop
		current_body = b.labelled(component.label,
					  b.whileLoop(body_entry_condition,
						      current_body));
		current_body_in_loop = true;
	    }
	    if (DEBUG_CONJUNCTIVELOOP_GEN) {
		System.err.println("--- RESULT: ----------------------------------------");
		System.err.println(current_body);
		System.err.println("================================================================================");
	    }

	    // failure-handler update \
	    if (at_least_one_component_is_nonlinear // if all are linear, we can just stick to the default `return false'
		&& !componentIsLinear(component_nr)) {
		// { state  = 2n-1; continue };
		failure_handler =
		    b.flatStmtList(
				   b.assign(state_var, most_recent_nonlinear_state),
				   b.continueStmt()
				   );
	    }
	    // failure-handler update /
	}

	// If necessary, wrap loop in brak
	if (at_least_one_component_is_nonlinear && !current_body_in_loop) {
	    current_body = 
		b.whileLoop(b.booleanLiteral(true),
			    current_body);
	}

	final List<? extends JCStatement> eval_list =
	    b.flatStmtList(current_body,
			   this.genFailStmts()
			   );


	for (String field : joins_that_we_need_to_clone) {
	    this.addPostCloneOperation(b.ifThen(b.isNotEqual(b.id(field), b.nullLiteral()),
						b.stmt(b.assign(field,
								//b.methodapp(b.id(field), "copyRecursively",
								b.copy(join_access_for_joins_to_clone.get(field))
								))));
	}

	ListBuffer<JCTree> initlb = new ListBuffer<JCTree>();
	for (JCTree x : init_list)
	    initlb.add(x);
	ListBuffer<JCTree> evallb = new ListBuffer<JCTree>();
	for (JCTree x : eval_list)
	    evallb.add(x);

	StateBlock sb_init_result = new StateBlock(initlb, this.components.get(0).getInitContext().getStateId(), false);
	StateBlock sb_eval_result = new StateBlock(evallb, this.components.get(0).getEvalContext().getStateId(), false);
	sb_eval_result.addStateId(this.components.get(this.components.size() - 1).getEvalContext().getMaxStateId());

	this.getParentTranslatorContext().closeInternal(sb_init_result, sb_eval_result);
    }
}


