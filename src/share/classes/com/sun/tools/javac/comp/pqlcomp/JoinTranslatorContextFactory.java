/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package com.sun.tools.javac.comp.pqlcomp;

import com.sun.tools.javac.code.*;
import com.sun.tools.javac.jvm.*;
import com.sun.tools.javac.tree.*;
import com.sun.tools.javac.tree.pql.*;
import com.sun.tools.javac.util.*;
import com.sun.tools.javac.util.JCDiagnostic.DiagnosticPosition;
import com.sun.tools.javac.util.List;

import com.sun.tools.javac.code.Symbol.*;
import com.sun.tools.javac.tree.JCTree.*;
import com.sun.tools.javac.code.Type.*;

import com.sun.tools.javac.jvm.Target;

import com.sun.tools.javac.comp.Attr;
import com.sun.tools.javac.comp.PQLOptimiser.Annotation;

import static com.sun.tools.javac.code.Flags.*;
import static com.sun.tools.javac.code.Kinds.*;
import static com.sun.tools.javac.code.TypeTags.*;
import static com.sun.tools.javac.jvm.ByteCodes.*;

import edu.umass.pql.*;
import edu.umass.pql.Env;

public interface JoinTranslatorContextFactory
{
    /**
     *
     */
    public OptFlags
    getOptFlags();

    // --------------------
    // Continuation context updates

    /**
     * Adds a continuation context override to all generated build contexts
     * and updates the current continuation context factory with one that
     * reflects this build context.
     * (See getContinuationContextFactory() for a description of what this is for.)
     *
     * It is not generally possible to fully reverse this operation.  To restore
     * the ContinuationContextFactory to its previous state, extract it with
     * getContinuationContextFactory() BEFORE using this call and set it
     * again with setContinuationContextFactory() after you are done.
     * Any BuildContexts generated in between will of course still be
     * affected by the ContinuationContextOverride.
     */
    public abstract void
    addContinuationContextOverride(ContinuationContextOverride override);

    /**
     * Obtain the current ContinuationContext Factory.
     *
     * This factory generates continuation contexts (which determine how
     * the build context should handle success and failure).  It is used
     * to generate the initial init and eval contexts, as well as any
     * additional build contexts requested by the translation.
     *
     * @return The current ContinuationContextFactory for this context
     */
    public abstract ContinuationContextFactory
    getContinuationContextFactory();

    /**
     * Alter the current ContinuationContext Factory.
     *
     * (See getContinuationContextFactory() for a description of what
     * that factory does.)
     *
     * Generally, you only want to call this (a) on join contexts designed
     * to translate more than one join and (b) in order to restore a previous
     * factory.  If you want to update all continuation contexts for the
     * next join-to-be-translated, use addContinuationContextOverride()
     * instead.
     *
     * @param f The new ContinuationContext factory to set
     */
    public abstract void
    setContinuationContextFactory(ContinuationContextFactory f);


    // --------------------------------------------------------------------------------

    public abstract JCExpression
    getJoinExpressionForSelf();

    public abstract void
    setJoinExpressionForSelf(JCExpression s);

    // --------------------------------------------------------------------------------

    /**
     * Obtain a join context with explicit state handling.
     */
    public abstract JoinTranslatorContext
    newExplicitlyStatefulJoinTranslatorContext();

    /**
     *
     */
    public abstract JoinTranslatorContext
    newImplicitlyStatefulJoinTranslatorContext();

    /**
     *
     */
    public abstract JoinTranslatorContext
    newConjunctiveJoinTranslatorContext();

    /**
     *
     */
    public abstract JoinTranslatorContext
    newNonParallelReductionJoinContext(Reductor[] reductors);

    /**
     *
     */
    public abstract JoinTranslatorContext
    newParallelReductionJoinContext(Reductor[] reductors);

    /**
     *
     */
    public abstract boolean
    getParallelismSupported();
}