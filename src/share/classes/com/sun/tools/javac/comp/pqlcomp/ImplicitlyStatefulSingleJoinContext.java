/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package com.sun.tools.javac.comp.pqlcomp;

import java.util.*;

import com.sun.tools.javac.code.*;
import com.sun.tools.javac.jvm.*;
import com.sun.tools.javac.tree.*;
import com.sun.tools.javac.tree.pql.*;
import com.sun.tools.javac.util.*;
import com.sun.tools.javac.util.JCDiagnostic.DiagnosticPosition;
import com.sun.tools.javac.util.List;

import com.sun.tools.javac.code.Symbol.*;
import com.sun.tools.javac.tree.JCTree.*;
import com.sun.tools.javac.code.Type.*;

import com.sun.tools.javac.jvm.Target;

import com.sun.tools.javac.comp.Attr;
import com.sun.tools.javac.comp.PQLOptimiser.Annotation;

import static com.sun.tools.javac.code.Flags.*;
import static com.sun.tools.javac.code.Kinds.*;
import static com.sun.tools.javac.code.TypeTags.*;
import static com.sun.tools.javac.jvm.ByteCodes.*;

import edu.umass.pql.*;
import edu.umass.pql.Env;


public class ImplicitlyStatefulSingleJoinContext extends DefaultJoinTranslatorContext
{
    protected final BasicBuildContext init_detail;
    protected final BasicBuildContext eval_detail;
    private boolean ensure_no_multiple_calls;

    public ImplicitlyStatefulSingleJoinContext(ToClassTranslatorContext tctc,
					       ContinuationContextFactory ccf,
					       boolean ensure_no_multiple_calls)
    {
	super(tctc, ccf);

	this.ensure_no_multiple_calls = ensure_no_multiple_calls;
	this.init = this.init_detail = (BasicBuildContext) this.newEvaluationState();
	this.init_detail.setInit(this.init_detail, this, true);
	this.eval_detail = (BasicBuildContext) this.newEvaluationState();
	this.eval_detail.setInit(this.init_detail, this, false);
	this.init(this.init_detail, this.eval_detail);
    }

    @Override
    void
    overrideAllCurrentBuildContextContinuations(ContinuationContextOverride override)
    {
	init_detail.overrideContinuationContext(override);
	eval_detail.overrideContinuationContext(override);
    }

    @Override
    public void
    close(Annotation annotation)
    {
	// ordererd to skip this one?
	if (this.wasSkipped())
	    return;

	if (annotation.isLinear() && ensure_no_multiple_calls) {
	    BasicBuildContext bbc = (BasicBuildContext) this.eval;

	    // Update evaluator to only execute once
	    // prepend the following code:
	    //  	if (state == 1) return false;
	    //  	state = 1;

	    final int eval_state_id = this.eval.getStateId();

	    final JCTree return_on_fail =
		b.ifThen(b.isEqual(b.id("state"), b.intLiteral(eval_state_id)),
			 bbc.failStmt("issj-0"));
	    this.eval.prependStmt(b.assign("state", b.intLiteral(eval_state_id)));
	    this.eval.prependStmt(b.ifThen(b.isEqual(b.id("state"), b.intLiteral(eval_state_id)),
					   bbc.failStmt("issj-1")));
	}
	// Make sure to always init state to the init state
	if (!this.init.getBody().isEmpty() && ensure_no_multiple_calls)
	    this.init.prependStmt(b.assign("state", b.intLiteral(this.init.getStateId())));

	if (!this.eval_detail.hasUnconditionalReturn())
	    this.eval.succeed();

	this.getParentTranslatorContext().closeInternal(this.init.getStateBlock(), this.eval.getStateBlock());
    }

    @Override
    public BasicBuildContext
    createNewEvaluationState()
    {
	BasicBuildContext bc = new BasicBuildContext(b,
						     null,
						     this.getParentTranslatorContext().allocateStateId());
	bc.setContinuationContext(newContinuationContext(b, bc));
	bc.setInit(this.init, this, false);
	return bc;
    }
}
