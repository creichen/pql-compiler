/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package com.sun.tools.javac.comp.pqlcomp;

import java.util.*;

import com.sun.source.tree.Tree;
import com.sun.source.tree.*;
import com.sun.tools.javac.tree.JCTree.*;
import com.sun.tools.javac.util.List;
import com.sun.tools.javac.util.ListBuffer;
import com.sun.tools.javac.tree.*;

import com.sun.tools.javac.code.*;
import com.sun.tools.javac.jvm.*;
import com.sun.tools.javac.tree.*;
import com.sun.tools.javac.tree.pql.*;
import com.sun.tools.javac.util.*;
import com.sun.tools.javac.util.JCDiagnostic.DiagnosticPosition;
import com.sun.tools.javac.util.List;

import com.sun.tools.javac.code.Symbol.*;
import com.sun.tools.javac.tree.JCTree.*;
import com.sun.tools.javac.code.Type.*;

import com.sun.tools.javac.jvm.Target;

import static com.sun.tools.javac.code.Flags.*;
import static com.sun.tools.javac.code.Kinds.*;
import static com.sun.tools.javac.code.TypeTags.*;
import static com.sun.tools.javac.jvm.ByteCodes.*;

import edu.umass.pql.*;
import edu.umass.pql.Env;

public class Builder
{
    public final Name.Table names;
    public final Symtab syms;
    public final Types types;
    public final TreeMaker maker;
    public final PQLContext pql_context;

    public OptFlags opt_flags;
    VarSet temp_vars = new VarSet();
    private Map<Integer, String> var_field_map;
    final TreeCopier<Object> copier;
    final TreeCopier<Object> desync_copier; // copies and removes synchronized statements
    private ToplevelJoinTranslatorContext tc;

    public void
    setOptFlags(OptFlags f, ToplevelJoinTranslatorContext tc)
    {
	this.opt_flags = f;
	this.var_field_map = new HashMap<Integer, String>();
	this.had_output = false;
	this.tc = tc;

	System.err.println("-------------------------------");
	if (f.minimise_env_access) {
	    for (int var : this.opt_flags.inputs)
		addVarField(tc, var);
	    this.opt_flags.use_result_var = (this.opt_flags.outputs.size() == 1); // only use this with a single output

	    for (int var : this.opt_flags.outputs) {
		// only use this with a single output, so far
		Type vartype = varInfo(var).type;
		if (vartype == null || vartype.isPrimitive()) {
		    this.opt_flags.use_result_var = false;
		}
		System.err.print("OUTPUT VAR: ");
		addVarField(tc, var, this.opt_flags.use_result_var);
	    }
	}
    }

    // ----------------------------------------
    // helpers for codegen purposes
    public String
    getEnvArrayName(int var)
    {
	final int ty = (var >> Env.VAR_TABLE_SHIFT) & Env.VAR_TABLE_MASK;
	switch (ty) {
	case Env.TYPE_INT:
	    return "v_int";
	case Env.TYPE_LONG:
	    return "v_long";
	case Env.TYPE_DOUBLE:
	    return "v_double";
	case Env.TYPE_OBJECT:
	    return "v_object";
	case Env.TYPE_WILDCARD:
	    throw new RuntimeException("Wildcard!");
	default:
	    throw new RuntimeException("Unexpected type " + ty);
	}
    }

    public JCExpression
    envVarAccess(int var)
    {
	return index("env." + getEnvArrayName(var), intLiteral(Env.varIndex(var)));
    }

    /**
     * Write all outputs for the current context back into `env'
     */
    public JCStatement
    writeOutputsBefore(JCStatement stmt)
    {
	if (opt_flags.minimise_env_access
	    && !opt_flags.use_result_var) {
	    ListBuffer<JCStatement> lb = new ListBuffer<JCStatement>();
	    for (int var : this.opt_flags.outputs)
		if (!varInfo(var).isConstant()) {
		    lb.add(stmt(assign(envVarAccess(var), id(getVarFieldName(var)))));
		}
	    lb.add(stmt);
	    return block(lb.toList());
	} else
	    return stmt;
    }

    public JCExpression
    defaultInitValueForType(Type t)
    {
	switch (t.elimQTyVars().tag) {
	case INT:	return intLiteral(0);
	case SHORT:	return shortLiteral(0);
	case BYTE:	return byteLiteral(0);
	case CHAR:	return charLiteral('\00');
	case LONG:	return longLiteral(0);
	case FLOAT:	return floatLiteral(0.0f);
	case DOUBLE:	return doubleLiteral(0.0);
	case BOOLEAN:	return booleanLiteral(false);
	default:	return nullLiteral();
	}
    }

    /**
     * Produce variable declarations and initialisations from `env' for all inputs for the current context
     */
    public List<JCStatement>
    readInputs()
    {
	if (true || opt_flags.minimise_env_access) {
	    ListBuffer<JCStatement> lb = new ListBuffer<JCStatement>();

	    for (int var : this.opt_flags.inputs) {
		PQLVarInfo vi = varInfo(var);
		if (vi.isLiteral())
		    continue;

		lb.add(varWithInit(getVarFieldName(var), typeExpr(vi.type), cast(vi.type, envVarAccess(var))));
	    }

	    for (int var : this.temp_vars) {
		PQLVarInfo vi = varInfo(var);
		if (vi.isLiteral() || this.opt_flags.inputs.contains(var))
		    continue;

		lb.add(varWithInit(getVarFieldName(var), typeExpr(vi.type), defaultInitValueForType(vi.type)));
	    }

	    // If the result is in the result var, we don't need to do anything; otherwise, make sure to initialise temp vars
	    if (!opt_flags.use_result_var)
		for (int var : this.opt_flags.outputs) {
		    PQLVarInfo vi = varInfo(var);
		    if (vi.isLiteral())
			continue;

		    lb.add(varWithInit(getVarFieldName(var), typeExpr(vi.type), defaultInitValueForType(vi.type)));
		}
	    return lb.toList();
	} else {
	    throw new RuntimeException("Do Not Want");
	}
    }

    private void
    addVarField(ToplevelJoinTranslatorContext tc, int var)
    {
	addVarField(tc , var, false);
    }

    boolean had_output = false;

    private void
    addVarField(ToplevelJoinTranslatorContext tc, int var, boolean is_output)
    {
	final PQLVarInfo var_info = varInfo(var);
	if (var_info.isLiteral())
	    return;

	final String oname;
	if (is_output && !had_output) {
	    if (had_output)
		throw new RuntimeException("Multi-output not supported!");
	    had_output = true;
	    oname = "this.result";
	} else
	    oname = "cvar_" + Env.showVar(var).replace('[', '_').replace(']', '_').replace('!', '_').replace('?', '_') + "_" + tc.getFreshLocalVarNumber();

	System.err.println("  Var: " + Env.showVar(var) + " : " + var_info.type + " => " + oname);
	var_field_map.put(Env.readVar(var), oname);
    }

    public String
    getVarFieldName(int var)
    {
	if ((var_field_map.get(Env.readVar(var))) == null
	    && opt_flags.minimise_env_access
	    && !opt_flags.inputs.contains(var)) {
	    temp_vars.insert(var);
	    addVarField(this.tc, var);
	}
	return var_field_map.get(Env.readVar(var));
    }

    public PQLVarInfo
    varInfo(int var)
    {
	return this.pql_context.varInfo(var);
    }

    public Type
    typeOf(int var)
    {
	if (Env.isWildcard(var))
	    return this.syms.objectType;
	return this.varInfo(var).type.elimQTyVars();
    }

    public Builder(Name.Table names, Symtab syms, Types types, TreeMaker maker, PQLContext pql_context)
    {
	this.names = names;
	this.syms = syms;
	this.types = types;
	this.maker = maker;
	this.pql_context = pql_context;
	this.copier = new TreeCopier<Object>(maker);
	this.desync_copier = new TreeCopier<Object>(maker) {
	    @Override
	    public JCTree visitSynchronized(SynchronizedTree node, Object p) {
		JCSynchronized t = (JCSynchronized) node;
		JCBlock body = super.copy(t.body, p);
		return body;
	    }
	};
	this.boolean_type = maker.TypeIdent(TypeTags.BOOLEAN);
	this.int_type = maker.TypeIdent(TypeTags.INT);
	this.void_type = maker.TypeIdent(TypeTags.VOID);
    }

    public Name
    name(String s)
    {
	return names.fromString(s);
    }

    public JCExpression
    qualifiedID(String classname)
    {
	String[] names = classname.split("\\.");

	int index = 0;
	JCExpression result = maker.Ident(name(names[index++]));
	while (index < names.length)
	    result = maker.Select(result, name(names[index++]));
	return result;
    }

    public JCExpression
    id(String id)
    {
	return qualifiedID(id);
    }

    public JCParens
    parensDummy()
    {
	return maker.Parens(qualifiedID("edu.umass.pql.PQLFactory.TRUE"));
    }

    public <T extends JCTree> T
    copy(T tree)
    {
	if (tree == null)
	    return null;
	else
	    return copier.copy(tree);
    }

    public <T extends JCTree> List<T>
    copyList(List<T> list)
    {
	if (list.isEmpty())
	    return List.<T>nil();
	else {
	    final List<T> tl = this.copyList((List<T>) list.tail);
	    return tl.prepend(copy(list.head));
	}
    }

    public JCExpression
    negate(JCExpression expr)
    {
	return unOp(JCTree.NOT, expr);
    }

    public JCExpression
    negateIf(JCExpression expr, boolean cond)
    {
	if (cond)
	    return negate(expr);
	else
	    return expr;
    }

    public <T> List<T>
    list(T ... args)
    {
	ListBuffer<T> lb = new ListBuffer<T>();

	for (T a : args)
	    if (a != null)
		lb.add(a);

	return lb.toList();
    }

    public List<JCStatement>
    flatStmtList(Object ... args)
    {
	ListBuffer<JCStatement> lb = new ListBuffer<JCStatement>();

	addArrayToFlat(lb, args);

	return lb.toList();
    }

    private void
    addToFlat(ListBuffer<JCStatement> lb, Object o)
    {
	if (o == null)
	    return;
	else if (o instanceof Collection<?>)
	    addContainerToFlat(lb, (Collection<Object>)o);
	else if (o instanceof Object[])
	    addArrayToFlat(lb, (Object[])o);
	else if (o instanceof JCStatement)
	    lb.add((JCStatement) o);
	else if (o instanceof JCExpression)
	    lb.add(this.stmt((JCExpression) o));
	else
	    throw new RuntimeException("Unsupported type: " + o.getClass());
    }

    private void
    addContainerToFlat(ListBuffer<JCStatement> lb, Collection<Object> args)
    {
	for (Object a : args)
	    addToFlat(lb, a);
    }

    private void
    addArrayToFlat(ListBuffer<JCStatement> lb, Object[] args)
    {
	for (Object a : args)
	    addToFlat(lb, a);
    }

    public JCExpression
    funapp(String receiver, JCExpression ... args)
    {
	return maker.Apply(List.<JCExpression>nil(),
			   qualifiedID(receiver),
			   list(args));
    }

    public JCStatement
    throwException(JCExpression classname, JCExpression ... args)
    {
	return maker.Throw(newObjectStmt(classname, args));
    }

    public JCExpression
    funapp(Symbol.MethodSymbol receiver, JCExpression ... args)
    {
	final String name = (receiver.owner == null
			     ? ""
			     : receiver.owner.getQualifiedName() + ".")
	    + receiver.getQualifiedName().toString();

	return funapp(name, args);
    }

    public JCExpression
    field(JCExpression obj, String message)
    {
	return maker.Select(obj, name(message));
    }

    public JCExpression
    methodapp(JCExpression receiver, String message, JCExpression ... args)
    {
	return maker.Apply(List.<JCExpression>nil(),
			   maker.Select(receiver, name(message)),
			   list(args));
    }

    public JCReturn
    ret(JCExpression expr)
    {
	return maker.Return(expr);
    }

    public JCExpression boolean_type;
    public JCExpression int_type;
    public JCExpression void_type;

    public JCBlock
    block(JCTree ... body)
    {
	return this.block(list(body));
    }

    public JCContinue
    continueStmt()
    {
	return maker.Continue(null);
    }

    public JCContinue
    continueStmt(String label)
    {
	return maker.Continue(names.fromString(label));
    }

    public JCBreak
    breakStmt(String label)
    {
	return maker.Break(names.fromString(label));
    }

    public JCBreak
    breakStmt()
    {
	return maker.Break(null);
    }

    public JCBlock
    block(List<? extends JCTree> body)
    {
	return maker.Block(0l, stmts(body));
    }

    JCVariableDecl
    var(String nm, JCExpression type)
    {
	return maker.VarDef(maker.Modifiers(0l, List.<JCAnnotation>nil()),
			    name(nm), type, (JCExpression)null);
	
    }

    JCVariableDecl
    varWithInit(String nm, JCExpression type, JCExpression init)
    {
	return maker.VarDef(maker.Modifiers(0l, List.<JCAnnotation>nil()),
			    name(nm), type, init);
	
    }

    public JCMethodDecl
    methodDecl(String method_name,
	       JCExpression return_type,
	       JCBlock body,
	       JCVariableDecl ... args)
    {
	return maker.MethodDef(maker.Modifiers(Flags.PUBLIC, List.<JCAnnotation>nil()),
			       name(method_name),
			       return_type,
			       List.<JCTypeParameter>nil(),
			       list(args),
			       List.<JCExpression>nil(),
			       body, null);
    }

    public JCLabeledStatement
    label(String label, JCStatement stmt)
    {
	return maker.Labelled(name(label), stmt);
    }

    public JCSwitch
    switchAnalysis(JCExpression selector, List<JCCase> cases)
    {
	return maker.Switch(selector, cases);
    }

    public List<JCStatement>
    stmts(List<? extends JCTree> args)
    {
	ListBuffer<JCStatement> statements = new ListBuffer<JCStatement>();
	for (JCTree t : args) {
	    if (t instanceof JCStatement)
		statements.add((JCStatement) t);
	    else if (t instanceof JCExpression)
		statements.add(stmt((JCExpression) t));
	    else
		throw new RuntimeException("Neither expr nor stmt: " + t + " : " + t.getClass());
	}

	return statements.toList();
    }

    public JCCase
    switchCase(int number, List<? extends JCTree> args)
    {
	return maker.Case(intLiteral(number), stmts(args));
    }

    public JCCase
    switchCase(int number, JCTree ... args)
    {
	return switchCase(number, list(args));
    }

    public JCExpression
    stringLiteral(String s)
    {
	return maker.Literal(TypeTags.CLASS, s);
    }

    public JCExpression
    nullLiteral()
    {
	return maker.Literal(TypeTags.BOT, null);
    }

    public JCExpression
    intLiteral(int nr)
    {
	JCExpression lit = maker.Literal(TypeTags.INT, nr);
	lit.type = syms.intType;
	return lit;
    }

    public JCExpression
    longLiteral(long nr)
    {
	JCExpression lit = maker.Literal(TypeTags.LONG, nr);
	lit.type = syms.longType;
	return lit;
    }

    public JCExpression
    shortLiteral(int nr)
    {
	JCExpression lit = this.intLiteral(nr);
	lit.type = syms.shortType;
	return lit;
    }

    public JCExpression
    byteLiteral(int nr)
    {
	JCExpression lit = this.intLiteral(nr);
	lit.type = syms.byteType;
	return lit;
    }

    public JCExpression
    charLiteral(int nr)
    {
	JCExpression lit = this.intLiteral(nr);
	lit.type = syms.charType;
	return lit;
    }

    public JCExpression
    doubleLiteral(double nr)
    {
	return maker.Literal(TypeTags.DOUBLE, nr);
    }

    public JCExpression
    floatLiteral(float nr)
    {
	return maker.Literal(TypeTags.FLOAT, nr);
    }

    public JCExpression
    literalZeroForVar(int nr)
    {
	switch (varInfo(nr).type.tag) {
	case BOOLEAN:
	    return booleanLiteral(false);
	case FLOAT:
	    return floatLiteral(0.0f);
	case DOUBLE:
	    return doubleLiteral(0.0f);
	default:
	    return intLiteral(0);
	}
    }

    public JCExpression
    booleanLiteral(boolean val)
    {
	return maker.Literal(TypeTags.BOOLEAN, val ? 1 : 0);
    }

    public JCArrayAccess
    index(String expr, JCExpression index)
    {
	return index(id(expr), index);
    }

    public JCArrayAccess
    index(JCExpression expr, JCExpression index)
    {
	return maker.Indexed(expr, index);
    }

    public JCExpression
    conditional(JCExpression cond, JCExpression thenpart, JCExpression elsepart)
    {
	return maker.Conditional(cond, thenpart, elsepart);
    }


    public JCIf
    ifThenElse(JCExpression cond, JCStatement thenpart, JCStatement elsepart)
    {
	return maker.If(cond, thenpart, elsepart);
    }

    public JCIf
    ifThen(JCExpression cond, JCStatement thenpart)
    {
	return this.ifThenElse(cond, thenpart, null);
    }

    public JCAssign
    assign(String lhs, JCExpression body)
    {
	return maker.Assign(id(lhs), body);
    }

    public JCAssign
    assign(JCExpression lhs, JCExpression body)
    {
	return maker.Assign(lhs, body);
    }

    public JCExpression
    isEqual(JCExpression lhs, JCExpression rhs)
    {
	return this.binOp(JCTree.EQ, lhs, rhs);
    }

    public JCExpression
    isNotEqual(JCExpression lhs, JCExpression rhs)
    {
	return this.binOp(JCTree.NE, lhs, rhs);
    }

    public JCExpression
    binOp(int op, JCExpression lhs, JCExpression rhs)
    {
	return maker.Binary(op, lhs, rhs);
    }

    public JCExpression
    assignOp(int op, JCExpression lhs, JCExpression rhs)
    {
	return maker.Assignop(op, lhs, rhs);
    }

    public JCExpression
    unOp(int op, JCExpression expr)
    {
	return maker.Unary(op, expr);
    }

    public JCExpressionStatement
    stmt(JCExpression expr)
    {
	return maker.Exec(expr);
    }

    public JCExpression
    isInstanceOf(JCExpression expr, JCExpression type)
    {
	return maker.TypeTest(expr, type);
    }

    public JCExpression
    parameterisedType(String ty, JCExpression ... args)
    {
	return maker.TypeApply(id(ty), this.<JCExpression>list(args));
    }

    /**
     * Create JCExpression tree that represents the specified type
     */
    public JCExpression
    typeExpr(Type t)
    {
	class TypeTranslationContext
	{
	}

	class ErrorTypeException extends RuntimeException {};

	Type.Visitor<JCExpression, TypeTranslationContext> visitor =
	    new Type.Visitor<JCExpression, TypeTranslationContext>() {
	    @Override
	    public JCExpression
	    visitClassType(ClassType t, TypeTranslationContext ctx)
	    {
		String name = t.toString();
		if (name.indexOf("<") > 0)
		    name = name.substring(0, name.indexOf("<"));
		return id(name);
	    }

	    @Override
	    public JCExpression
	    visitWildcardType(WildcardType t, TypeTranslationContext ctx)
	    {
		throw new RuntimeException("Unsupported subexpr type: " + t + "  from " + t.getClass());
	    }

	    @Override
	    public JCExpression
	    visitArrayType(ArrayType t, TypeTranslationContext ctx)
	    {
		return maker.TypeArray(t.elemtype.accept(this, ctx));
	    }

	    @Override
	    public JCExpression
	    visitMethodType(MethodType t, TypeTranslationContext ctx)
	    {
		throw new RuntimeException("Unsupported subexpr type: " + t + "  from " + t.getClass());
	    }

	    @Override
	    public JCExpression
	    visitPackageType(PackageType t, TypeTranslationContext ctx)
	    {
		throw new RuntimeException("Unsupported subexpr type: " + t + "  from " + t.getClass());
	    }

	    @Override
	    public JCExpression
	    visitTypeVar(TypeVar t, TypeTranslationContext ctx)
	    {
		throw new RuntimeException("Unsupported subexpr type: " + t + "  from " + t.getClass());
	    }

	    @Override
	    public JCExpression
	    visitCapturedType(CapturedType t, TypeTranslationContext ctx)
	    {
		throw new RuntimeException("Unsupported subexpr type: " + t + "  from " + t.getClass());
	    }

	    @Override
	    public JCExpression
	    visitForAll(ForAll t, TypeTranslationContext ctx)
	    {
		throw new RuntimeException("Unsupported subexpr type: " + t + "  from " + t.getClass());
	    }

	    @Override
	    public JCExpression
	    visitUndetVar(UndetVar t, TypeTranslationContext ctx)
	    {
		throw new RuntimeException("Unsupported subexpr type: " + t + "  from " + t.getClass());
	    }

	    @Override
	    public JCExpression
	    visitErrorType(ErrorType t, TypeTranslationContext ctx)
	    {
		throw new ErrorTypeException();
	    }

	    @Override
	    public JCExpression
	    visitMagicMapType(MagicMapType t, TypeTranslationContext ctx)
	    {
		return Builder.this.id("java.lang.Object");
	    }

	    @Override
	    public JCExpression
	    visitMagicSizeType(MagicSizeType t, TypeTranslationContext ctx)
	    {
		return Builder.this.id("java.lang.Object");
	    }

	    @Override
	    public JCExpression
	    visitType(Type t, TypeTranslationContext ctx)
	    {
		// primitive type
		return maker.TypeIdent(t.tag);
	    }
	};

	try {
	    return t.elimQTyVars().accept(visitor, new TypeTranslationContext());
	} catch (ErrorTypeException _) {
	    return id("Object");
	}
    }

    public JCExpression
    arrayType(JCExpression type)
    {
	return maker.TypeArray(type);
    }

    public JCStatement
    whileLoop(JCExpression cond, JCStatement body)
    {
	return maker.WhileLoop(cond, body);
    }

    public JCStatement
    doLoop(JCExpression cond, JCStatement body)
    {
	return maker.DoLoop(body, cond);
    }

    public JCStatement
    labelled(String label, JCStatement stmt)
    {
	return maker.Labelled(names.fromString(label), stmt);
    }

    public JCContinue
    doContinue(String label)
    {
	return maker.Continue(names.fromString(label));
    }

    public JCExpression
    cast(Type t, JCExpression expr)
    {
	return maker.TypeCast(typeExpr(t), expr);
    }

    public Type
    boxedType(Type t)
    {
	final ClassSymbol boxed_cast_symbol = this.types.boxedClass(t);
	if (boxed_cast_symbol != null)
	    t = boxed_cast_symbol.type;
	return t;
    }

    public JCExpression
    castBoxed(Type t, JCExpression expr)
    {
	return this.cast(this.boxedType(t), expr);
    }

    public JCExpression
    castBoxedCanonical(Type t, JCExpression expr)
    {
	if (t.isPrimitive()) {
	    if (t.tag == INT) {
		return this.funapp("edu.umass.pql.Env.canonicalInteger", expr);
	    } else if (t.tag == LONG) {
		return this.funapp("edu.umass.pql.Env.canonicalLong", expr);
	    }
	}
	return this.cast(this.boxedType(t), expr);
    }

    public JCExpression
    cast(JCExpression t, JCExpression expr)
    {
	return maker.TypeCast(t, expr);
    }

    public Type
    typaramOf(Type t, int arg_nr, Type alternative)
    {
	if (t instanceof ClassType) {
	    ClassType ct = (ClassType) t;
	    if (ct.getTypeArguments() == null)
		return alternative;
	    else
		return ct.typarams_field.get(arg_nr);
	}
	return alternative;
    }

    public JCExpression
    newObjectStmt(JCExpression type, JCExpression ... args)
    {
	return maker.NewClass(null, List.<JCExpression>nil(),
			      type, list(args),
			      null);
    }

    // synchronisation support

    public <T extends JCTree> T
    copyDesynchronized(T tree)
    {
	if (tree == null)
	    return null;
	else
	    return this.desync_copier.copy(tree);
    }

    public JCStatement
    syncStatement(JCStatement stmt)
    {
	JCBlock block;
	if (stmt instanceof JCBlock)
	    block = (JCBlock) stmt;
	else
	    block = block(stmt);

	final JCSynchronized sync = maker.Synchronized(id("edu.umass.pql.Join.class"), block);
	final JCBlock retval = block(sync);
	return retval;
    }
}
