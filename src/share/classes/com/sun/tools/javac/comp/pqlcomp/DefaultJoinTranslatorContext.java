/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package com.sun.tools.javac.comp.pqlcomp;

import java.util.*;

import com.sun.tools.javac.code.*;
import com.sun.tools.javac.jvm.*;
import com.sun.tools.javac.tree.*;
import com.sun.tools.javac.tree.pql.*;
import com.sun.tools.javac.util.*;
import com.sun.tools.javac.util.JCDiagnostic.DiagnosticPosition;
import com.sun.tools.javac.util.List;

import com.sun.tools.javac.code.Symbol.*;
import com.sun.tools.javac.tree.JCTree.*;
import com.sun.tools.javac.code.Type.*;

import com.sun.tools.javac.jvm.Target;

import com.sun.tools.javac.comp.Attr;
import com.sun.tools.javac.comp.PQLOptimiser.Annotation;

import static com.sun.tools.javac.code.Flags.*;
import static com.sun.tools.javac.code.Kinds.*;
import static com.sun.tools.javac.code.TypeTags.*;
import static com.sun.tools.javac.jvm.ByteCodes.*;

import edu.umass.pql.*;
import edu.umass.pql.Env;

public abstract class DefaultJoinTranslatorContext extends JoinTranslatorContext
{
    void
    overrideAllCurrentBuildContextContinuations(ContinuationContextOverride override)
    {
    }

    @Override
    public final void
    addContinuationContextOverride(ContinuationContextOverride override)
    {
	super.addContinuationContextOverride(override);
	this.overrideAllCurrentBuildContextContinuations(override);
    }

    public
    DefaultJoinTranslatorContext(ToClassTranslatorContext parent, ContinuationContextFactory cctx_factory)
    {
	super(parent);
	this.setContinuationContextFactory(cctx_factory);
	setJoinExpressionForSelf(parent.getJoinExpressionForSelf());
    }

    public final ContinuationContext
    newContinuationContext(Builder b, BuildContext bctx)
    {
	return continuation_context_factory.createContinuationContext(b, this, bctx);
    }

    @Override
    public void
    close(Annotation _)
    {
	// ordererd to skip this one?
	if (!this.wasSkipped())
	    this.getParentTranslatorContext().closeInternal(this.init.getStateBlock(), this.eval.getStateBlock());
    }

    protected List<JCStatement>
    genSucceedStmts()
    {
	final SimpleBuildContext accumulator = new SimpleBuildContext(this);
	final ContinuationContext cc = this.getContinuationContextFactory().createContinuationContext(this.b, this, accumulator);
	JCStatement stmt = cc.succeedStmt("djt-genSucceedStmts");
	return b.flatStmtList(accumulator.getCode().toList(),
			      stmt);
    }

    protected List<JCStatement>
    genFailStmts()
    {
	final SimpleBuildContext accumulator = new SimpleBuildContext(this);
	final ContinuationContext cc = this.getContinuationContextFactory().createContinuationContext(this.b, this, accumulator);
	JCStatement stmt = cc.failStmt("djt-genFailStmts");
	return b.flatStmtList(accumulator.getCode().toList(),
			      stmt);
    }

    @Override
    public boolean
    getParallelismSupported()
    {
	return this.getParentTranslatorContext().getParallelismSupported();
    }

    public JoinTranslatorContext
    newExplicitlyStatefulJoinTranslatorContext()
    {
	throw new UnsupportedOperationException();
    }

    public JoinTranslatorContext
    newImplicitlyStatefulJoinTranslatorContext()
    {
	throw new UnsupportedOperationException();
    }

    public JoinTranslatorContext
    newConjunctiveJoinTranslatorContext()
    {
	throw new UnsupportedOperationException();
    }

    @Override
    public JoinTranslatorContext
    newNonParallelReductionJoinContext(Reductor[] reductors)
    {
	return new ReductionJoinContext(this, this.getContinuationContextFactory(), reductors, false);
    }

    @Override
    public JoinTranslatorContext
    newParallelReductionJoinContext(Reductor[] reductors)
    {
	return new ReductionJoinContext(this, this.getContinuationContextFactory(), reductors, true);
    }
}
