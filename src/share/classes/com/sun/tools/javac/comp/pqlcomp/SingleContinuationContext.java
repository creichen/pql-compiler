/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package com.sun.tools.javac.comp.pqlcomp;

import java.util.*;

import com.sun.tools.javac.code.*;
import com.sun.tools.javac.jvm.*;
import com.sun.tools.javac.tree.*;
import com.sun.tools.javac.tree.pql.*;
import com.sun.tools.javac.util.*;
import com.sun.tools.javac.util.JCDiagnostic.DiagnosticPosition;
import com.sun.tools.javac.util.List;

import com.sun.tools.javac.code.Symbol.*;
import com.sun.tools.javac.tree.JCTree.*;
import com.sun.tools.javac.code.Type.*;

import com.sun.tools.javac.jvm.Target;

import com.sun.tools.javac.comp.Attr;
import com.sun.tools.javac.comp.PQLOptimiser.Annotation;

import static com.sun.tools.javac.code.Flags.*;
import static com.sun.tools.javac.code.Kinds.*;
import static com.sun.tools.javac.code.TypeTags.*;
import static com.sun.tools.javac.jvm.ByteCodes.*;

import edu.umass.pql.*;
import edu.umass.pql.Env;

// Basic continuation context with `return'
public class SingleContinuationContext implements ContinuationContext
{
    Builder b;

    public SingleContinuationContext(Builder builder)
    {
	this.b = builder;
    }

    private JCStatement
    maybeDebug(JCStatement stmt, String msg)
    {
	if (DEBUG_CC)
	    return b.block(b.stmt(b.stringLiteral("scc:" + msg)),
			   stmt);
	else
	    return stmt;
    }

    @Override
    public JCStatement
    failStmt(String msg)
    {
	return maybeDebug(b.ret(b.booleanLiteral(false)),
			  msg);
    }

    @Override
    public JCStatement
    succeedStmt(String msg)
    {
	return maybeDebug(b.writeOutputsBefore(b.ret(b.booleanLiteral(true))),
			  msg);
    }

    @Override
    public JCStatement
    returnStmt(JCExpression e, String msg)
    {
	return maybeDebug((JCStatement) b.ret(e),
			  msg);
    }

    @Override
    public JCStatement
    assertTrueStmt(JCExpression e, String msg)
    {
	return maybeDebug((JCStatement) b.ret(e),
			  msg);
    }

    @Override
    public JCStatement
    assertFalseStmt(JCExpression e, String msg)
    {
	return maybeDebug(b.ret(b.negate(e)),
			  msg);
    }

    public static class Factory extends ContinuationContextFactory
    {
	@Override
	public ContinuationContext
	createContinuationContext(Builder builder,
				  JoinTranslatorContext jctx,
				  BuildContext bctx)
	{
	    return new SingleContinuationContext(builder);
	}
    }

}
