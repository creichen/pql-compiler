/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package com.sun.tools.javac.comp.pqlcomp;

import java.util.*;

import com.sun.tools.javac.code.*;
import com.sun.tools.javac.jvm.*;
import com.sun.tools.javac.tree.*;
import com.sun.tools.javac.tree.pql.*;
import com.sun.tools.javac.util.*;
import com.sun.tools.javac.util.JCDiagnostic.DiagnosticPosition;
import com.sun.tools.javac.util.List;

import com.sun.tools.javac.code.Symbol.*;
import com.sun.tools.javac.tree.JCTree.*;
import com.sun.tools.javac.code.Type.*;

import com.sun.tools.javac.jvm.Target;

import com.sun.tools.javac.comp.PQLOptimiser.Annotation;

import static com.sun.tools.javac.code.Flags.*;
import static com.sun.tools.javac.code.Kinds.*;
import static com.sun.tools.javac.code.TypeTags.*;
import static com.sun.tools.javac.jvm.ByteCodes.*;

import edu.umass.pql.*;
import edu.umass.pql.Env;


public class StateBlock
{
    public int first_state_id, last_state_id;
    public ListBuffer<JCTree> code;
    public boolean is_terminated;

    public
    StateBlock(ListBuffer<JCTree> code, int id, boolean terminated)
    {
	this.first_state_id = this.last_state_id = id;
	this.is_terminated = terminated;
	this.code = code;
    }

    public ListBuffer<JCTree>
    getCode()
    {
	return this.code;
    }

    public boolean
    isTerminated()
    {
	return is_terminated;
    }

    public int
    getStateId()
    {
	return first_state_id;
    }

    public int
    getMaxStateId()
    {
	return this.last_state_id;
    }

    public void
    addStateId(int i)
    {
	if (i > last_state_id)
	    this.last_state_id = i;
    }
}