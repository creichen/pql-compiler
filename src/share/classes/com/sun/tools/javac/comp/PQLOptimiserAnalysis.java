/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package com.sun.tools.javac.comp;

import java.util.*;

// import com.sun.tools.javac.code.*;
// import com.sun.tools.javac.jvm.*;
// import com.sun.tools.javac.tree.*;
// import com.sun.tools.javac.tree.pql.*;
// import com.sun.tools.javac.util.*;
// import com.sun.tools.javac.util.JCDiagnostic.DiagnosticPosition;
// import com.sun.tools.javac.util.List;

// import com.sun.tools.javac.code.Symbol.*;
// import com.sun.tools.javac.tree.JCTree.*;
// import com.sun.tools.javac.code.Type.*;

// import com.sun.tools.javac.jvm.Target;

// import static com.sun.tools.javac.code.Flags.*;
// import static com.sun.tools.javac.code.Kinds.*;
// import static com.sun.tools.javac.code.TypeTags.*;
// import static com.sun.tools.javac.jvm.ByteCodes.*;

import edu.umass.pql.*;
import edu.umass.pql.il.*;
import edu.umass.pql.il.meta.*;
import edu.umass.pql.Env;

/**
 * Visitor for binding iterators
 */
public class PQLOptimiserAnalysis
{
    public static final int IS_LINEAR_0		= 0;
    public static final int IS_LINEAR_1		= 1;
    public static final int IS_LINEAR_MAX_1	= 2;
    public static final int IS_NONLINEAR	= 3;

    public static class IsLinearChecker extends JoinVisitor
    {
	public int linearity;

	public boolean isRead(Join j, int i) { int reg = j.getArg(i); return !Env.isWildcard(reg) && Env.isReadVar(reg); }
	public int ifRead(Join j, int i, int truecase, int falsecase) { if (isRead(j, i)) return truecase; else return falsecase; }

	public void visitDefault(Join it) { throw new RuntimeException("Missing analysis for " + it.getClass()); }

	public void visitField(Field fld) { linearity = ifRead(fld, 0, IS_LINEAR_MAX_1, IS_LINEAR_1); }

	public void visitJavaType(Type.JAVA_TYPE jty) { linearity = ifRead(jty, 0, IS_LINEAR_MAX_1, IS_LINEAR_1); }

	public void visitInstantiation(Instantiation ins) { linearity = ifRead(ins, 0, IS_LINEAR_0, IS_LINEAR_1); }

	public void visitControlStructure(ControlStructure sp) { throw new RuntimeException("Missing analysis for control structure " + sp.getClass()); }

	public void visitReduction(Reduction red) { linearity = IS_NONLINEAR; }

	public void visitAnyBlock(AbstractBlock b) { throw new RuntimeException("Missing analysis for block " + b.getClass()); }

	public void visitConjunctiveBlock(AbstractBlock.Conjunctive b)
	{
	    // int aggregate_linearity = IS_LINEAR_1;
	    // for (int i = 0; i < b.getComponentsNr(); i++) {
	    // 	b.getComponent(i).accept(this);
	    // 	final int local_linearity = this.linearity;
	    // 	if (local_linearity * aggregate_linearity < 2)
	    // 	    aggregate_linearity = local_linearity * aggregate_linearity;
	    // 	else if (local_linearity == IS_NONLINEAR || aggregate_linearity == IS_NONLINEAR)
	    // 	    aggregate_linearity = IS_NONLINEAR;
	    // 	else
	    // 	    aggregate_linearity = IS_LINEAR_MAX_1;
	    // }
	    // this.linearity = aggregate_linearity;
	    this.linearity = IS_NONLINEAR;
	}

	public void visitDisjunctiveBlock(AbstractBlock.Disjunctive b) { linearity = IS_NONLINEAR; }

	public void visitNot(Not n)
	{
	    // if (linearity == IS_LINEAR_1)
	    // 	linearity = IS_LINEAR_0;
	    // else if (linearity == IS_LINEAR_0)
	    // 	linearity = IS_LINEAR_1;
	    // else linearity = IS_LINEAR_MAX_1;
	    //this.linearity = IS_NONLINEAR;
	    this.linearity = IS_LINEAR_MAX_1;
	}

	public void visitNoFail(NoFail b) { linearity = IS_NONLINEAR; } //LINEAR_1

	public void visitBool(NoFail.Bool b) { linearity = IS_NONLINEAR; } // linearity = ifRead(b, 0, IS_LINEAR_MAX_1, IS_LINEAR_1); }

	public void visitSelectPath(SelectPath sp) { visitControlStructure(sp); } // not supported yet

	public void visitSchedule(Schedule sp) { visitControlStructure(sp); } // not supported yet

	public void visitCustomDecorator(CustomDecoratorJoin j) { super.visitCustomDecorator(j); } // no support needed, probably
	// ----------------------------------------

	public void visitTrue(Join j) { linearity = IS_LINEAR_1; }
	public void visitFalse(Join j) { linearity = IS_LINEAR_0; }
	public void visitAdd(Join j) { linearity = ifRead(j, 2, IS_LINEAR_MAX_1, IS_LINEAR_1); }
	public void visitSub(Join j) { linearity = ifRead(j, 2, IS_LINEAR_MAX_1, IS_LINEAR_1); }
	public void visitMul(Join j) { linearity = ifRead(j, 2, IS_LINEAR_MAX_1, IS_LINEAR_1); }
	public void visitDiv(Join j) { linearity = ifRead(j, 2, IS_LINEAR_MAX_1, IS_LINEAR_1); }
	public void visitMod(Join j) { linearity = ifRead(j, 2, IS_LINEAR_MAX_1, IS_LINEAR_1); }
	public void visitNeg(Join j) { linearity = ifRead(j, 1, IS_LINEAR_MAX_1, IS_LINEAR_1); }
	public void visitBitInv(Join j) { linearity = ifRead(j, 1, IS_LINEAR_MAX_1, IS_LINEAR_1); }
	public void visitBitOr(Join j) { linearity = ifRead(j, 2, IS_LINEAR_MAX_1, IS_LINEAR_1); }
	public void visitBitXor(Join j) { linearity = ifRead(j, 2, IS_LINEAR_MAX_1, IS_LINEAR_1); }
	public void visitBitAnd(Join j) { linearity = ifRead(j, 2, IS_LINEAR_MAX_1, IS_LINEAR_1); }
	public void visitBitShl(Join j) { linearity = ifRead(j, 2, IS_LINEAR_MAX_1, IS_LINEAR_1); }
	public void visitBitShr(Join j) { linearity = ifRead(j, 2, IS_LINEAR_MAX_1, IS_LINEAR_1); }
	public void visitBitSshr(Join j) { linearity = ifRead(j, 2, IS_LINEAR_MAX_1, IS_LINEAR_1); }

	public void visitEq(Join j) { linearity = ifRead(j, 1, IS_LINEAR_MAX_1, IS_LINEAR_1); }
	public void visitEqEquals(Join j) { linearity = ifRead(j, 1, IS_LINEAR_MAX_1, IS_LINEAR_1); }
	public void visitNeq(Join j) { linearity = IS_LINEAR_MAX_1; }
	public void visitNeqEquals(Join j) { linearity = IS_LINEAR_MAX_1; }
	public void visitLt(Join j) { linearity = IS_LINEAR_MAX_1; }
	public void visitLte(Join j) { linearity = IS_LINEAR_MAX_1; }

	public void visitArrayLookup(Join j) { visitPolyLookup(j); }
	public void visitMapLookup(Join j) { visitPolyLookup(j); }
	public void visitPolyLookup(Join j) { linearity = ifRead(j, 1, IS_LINEAR_MAX_1, IS_NONLINEAR); }

	public void visitSetContains(Join j) { linearity = ifRead(j, 1, IS_LINEAR_MAX_1, IS_NONLINEAR); }
	public void visitRangeContains(Join j) { linearity = ifRead(j, 2, IS_LINEAR_MAX_1, IS_NONLINEAR); }
	public void visitPolySize(Join j) { linearity = ifRead(j, 1, IS_LINEAR_MAX_1, IS_LINEAR_1); }
	public void visitArraySize(Join j) { linearity = ifRead(j, 1, IS_LINEAR_MAX_1, IS_LINEAR_1); }
	public void visitSetSize(Join j) { linearity = ifRead(j, 1, IS_LINEAR_MAX_1, IS_LINEAR_1); }
	public void visitMapSize(Join j) { linearity = ifRead(j, 1, IS_LINEAR_MAX_1, IS_LINEAR_1); }

	public void visitTypeChar(Join j) { linearity = ifRead(j, 0, IS_LINEAR_MAX_1, IS_NONLINEAR); }
	public void visitCoerceChar(Join j) { linearity = ifRead(j, 1, IS_LINEAR_MAX_1, IS_LINEAR_1); }
	public void visitTypeShort(Join j) { linearity = ifRead(j, 0, IS_LINEAR_MAX_1, IS_NONLINEAR); }
	public void visitCoerceShort(Join j) { linearity = ifRead(j, 1, IS_LINEAR_MAX_1, IS_LINEAR_1); }
	public void visitTypeByte(Join j) { linearity = ifRead(j, 0, IS_LINEAR_MAX_1, IS_NONLINEAR); }
	public void visitCoerceByte(Join j) { linearity = ifRead(j, 1, IS_LINEAR_MAX_1, IS_LINEAR_1); }
	public void visitTypeBoolean(Join j) { linearity = ifRead(j, 0, IS_LINEAR_MAX_1, IS_NONLINEAR); }
	public void visitCoerceBoolean(Join j) { linearity = ifRead(j, 1, IS_LINEAR_MAX_1, IS_LINEAR_1); }
	public void visitTypeFloat(Join j) { linearity = ifRead(j, 0, IS_LINEAR_MAX_1, IS_NONLINEAR); }
	public void visitCoerceFloat(Join j) { linearity = ifRead(j, 1, IS_LINEAR_MAX_1, IS_LINEAR_1); }
	public void visitTypeInt(Join j) { linearity = ifRead(j, 0, IS_LINEAR_MAX_1, IS_NONLINEAR); }
	public void visitTypeLong(Join j) { linearity = ifRead(j, 0, IS_LINEAR_MAX_1, IS_NONLINEAR); }
    }
}
