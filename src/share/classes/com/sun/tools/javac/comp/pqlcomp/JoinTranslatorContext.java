/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package com.sun.tools.javac.comp.pqlcomp;

import java.util.*;

import com.sun.tools.javac.code.*;
import com.sun.tools.javac.jvm.*;
import com.sun.tools.javac.tree.*;
import com.sun.tools.javac.tree.pql.*;
import com.sun.tools.javac.util.*;
import com.sun.tools.javac.util.JCDiagnostic.DiagnosticPosition;
import com.sun.tools.javac.util.List;

import com.sun.tools.javac.code.Symbol.*;
import com.sun.tools.javac.tree.JCTree.*;
import com.sun.tools.javac.code.Type.*;

import com.sun.tools.javac.jvm.Target;

import com.sun.tools.javac.comp.Attr;
import com.sun.tools.javac.comp.PQLOptimiser.Annotation;

import static com.sun.tools.javac.code.Flags.*;
import static com.sun.tools.javac.code.Kinds.*;
import static com.sun.tools.javac.code.TypeTags.*;
import static com.sun.tools.javac.jvm.ByteCodes.*;

import edu.umass.pql.*;
import edu.umass.pql.Env;

public abstract class JoinTranslatorContext extends ToClassTranslatorContext
{
    private ToClassTranslatorContext parent_context = null;

    protected ToClassTranslatorContext
    getParentTranslatorContext()
    {
	return this.parent_context;
    }

    public JoinTranslatorContext(ToClassTranslatorContext parent_context)
    {
	super(parent_context.b);
	this.parent_context = parent_context;
    }

    public BuildContext init;
    public BuildContext eval;

    protected void
    init(BuildContext init, BuildContext eval)
    {
	this.init = init;
	this.eval = eval;
    }

    public String
    freshField(JCExpression type_expr, JCExpression initialiser)
    {
	return this.parent_context.freshField(type_expr, initialiser);
    }

    private boolean was_skipped;

    protected boolean
    wasSkipped()
    {
       return this.was_skipped;
    }

    @Override
    public OptFlags
    getOptFlags()
    {
	return this.parent_context.getOptFlags();
    }

    /**
     * Don't optimise this one
     */
    public final void
    skip()
    {
	this.getOptFlags().is_all_inlined = false;
	this.was_skipped = true;
	this.closeInternalSkip();
    }

    @Override
    public boolean
    getParallelismSupported()
    {
	return this.parent_context.getParallelismSupported();
    }

    @Override
    public ParallelAccess
    parallelAccessFields(Type counter_type)
    {
	return this.parent_context.parallelAccessFields(counter_type);
    }

    @Override
    final int
    getFreshLocalVarNumber()
    {
	return this.parent_context.getFreshLocalVarNumber();
    }

    /**
     * Translate a single join into this context
     */
    public final void
    buildJoin(Join j)
    {
	doBuildJoin(j, (Annotation)j.getDecoration());
    }

    /**
     * Write a join to the translator context
     *
     * This method invokes the join annotation's build method with a suitably configured
     * translator context.  It may to pre- and post-processing for each translation.
     *
     * The number of times that this method may/must be called varies by translation context.
     *
     * The default implementation enforces nonrecursive calls.
     */
    public void
    doBuildJoin(Join j, Annotation annotation)
    {
	if (recursive_do_build_join)
	    throw new RuntimeException("Oops!  You called tc.buildJoin(j) on a Join j whose PQLOptimiser translateJoin invokes tc.buildJoin(j) again!");
	recursive_do_build_join = true;
	annotation.translateJoin(this);
	recursive_do_build_join = false;
    }
    private boolean recursive_do_build_join = false;

    @Override
    public final void
    writeToAnnotation(Attr attr, Annotation a)
    {
	throw new UnsupportedOperationException();
    }

     /**
     * Allocate a fresh state ID; may be used by implementations that have
     * more than one internal state (one state comes `for free', cf. getEvaluationState).
     * This disables the implicit state management.
     */
    public final BuildContext
    newEvaluationState()
    {
       BuildContext retval = createNewEvaluationState();
       return retval;
    }

    /**
     * Not supported by default; only special contexts for control structures will need to support this.
     */
    @Override
    protected void
    closeInternal(StateBlock _, StateBlock __)
    {
	throw new UnsupportedOperationException();
    }

    @Override
    protected void
    closeInternalSkip()
    {
	this.getParentTranslatorContext().closeInternalSkip();
    }

    public abstract BasicBuildContext
    createNewEvaluationState();

    public final void
    relinquishStates(int ... state_nrs)
    {
	this.getParentTranslatorContext().relinquishStates(state_nrs);
    }

    @Override
    public final int
    allocateStateId()
    {
	return this.getParentTranslatorContext().allocateStateId();
    }

    public final void
    addPostCloneOperation(JCTree post_clone_op)
    {
	this.getParentTranslatorContext().addPostCloneOperation(post_clone_op);
    }

    @Override
    public final String
    freshLabel(String arbitrary_annotation_name)
    {
	return this.getParentTranslatorContext().freshLabel(arbitrary_annotation_name);
    }
}
