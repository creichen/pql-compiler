/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package com.sun.tools.javac.comp.pqlcomp;

/**
 * A context to represent the handling of success/failure continuations
 */
public abstract class ContinuationContextFactory
{
    public abstract ContinuationContext
    createContinuationContext(Builder builder,
			      JoinTranslatorContext jctx,
			      BuildContext bctx);

    public final ContinuationContextFactory
    applyOverride(final ContinuationContextOverride override)
    {
	final ContinuationContextFactory old_factory = this;

	return new ContinuationContextFactory() {
	    public ContinuationContext
		createContinuationContext(Builder builder,
					  JoinTranslatorContext jctx,
					  BuildContext bctx)
	    {
		return override.override(old_factory.createContinuationContext(builder, jctx, bctx));
	    }
	};
    }
}
