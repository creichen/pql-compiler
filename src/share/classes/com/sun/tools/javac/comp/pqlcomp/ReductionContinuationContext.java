/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package com.sun.tools.javac.comp.pqlcomp;

import java.util.*;

import com.sun.tools.javac.code.*;
import com.sun.tools.javac.jvm.*;
import com.sun.tools.javac.tree.*;
import com.sun.tools.javac.tree.pql.*;
import com.sun.tools.javac.util.*;
import com.sun.tools.javac.util.JCDiagnostic.DiagnosticPosition;
import com.sun.tools.javac.util.List;

import com.sun.tools.javac.code.Symbol.*;
import com.sun.tools.javac.tree.JCTree.*;
import com.sun.tools.javac.code.Type.*;

import com.sun.tools.javac.jvm.Target;

import com.sun.tools.javac.comp.Attr;
import com.sun.tools.javac.comp.PQLOptimiser.Annotation;

import static com.sun.tools.javac.code.Flags.*;
import static com.sun.tools.javac.code.Kinds.*;
import static com.sun.tools.javac.code.TypeTags.*;
import static com.sun.tools.javac.jvm.ByteCodes.*;

import edu.umass.pql.*;
import edu.umass.pql.Env;

import java.io.StringWriter;
import java.io.PrintWriter;

class ReductionContinuationContext implements ContinuationContext
{
    private Builder b;
    private String outer_label;
    private Reductor reductor;
    private BuildContext eval;
    private SuccessHandler success_handler;

    public BuildContext
    getEval()
    {
	return this.eval;
    }

    static class Factory extends ContinuationContextFactory
    {
	Reductor[] reductors;
	String outer_label;
	ReductionJoinContext.InitCommitter init_state;

	Factory(Reductor [] reductors,
		String outer_label,
		ReductionJoinContext.InitCommitter init_state)
	{
	    this.reductors = reductors;
	    this.outer_label = outer_label;
	    this.init_state = init_state;
	}

	public ContinuationContext
	createContinuationContext(Builder b, JoinTranslatorContext jctx, BuildContext bctx)
	{
	    return new ReductionContinuationContext(b, reductors,
						    outer_label,
						    bctx,
						    init_state);
	}
    }

    public ReductionContinuationContext(Builder builder,
					Reductor[] reductors,
					String outer_label,
					BuildContext bctx,
					ReductionJoinContext.InitCommitter init_state)
    {
	this.b = builder;
	this.outer_label = outer_label;
	if (bctx == null)
	    throw new RuntimeException("Must have `bctx'");
	this.eval = bctx;

	if (reductors.length != 1)
	    throw new UnsupportedReductionException("Number of reductors = " + reductors.length);

	this.reductor = reductors[0];

	this.success_handler = createSuccessHandler(init_state, this.reductor);
    }

    private SuccessHandler
    createSuccessHandler(ReductionJoinContext.InitCommitter init_state,
			 Reductor r)
    {
	final SuccessHandler sh = new SuccessHandler(init_state);
	if (r.getInnerReductor() != null) {
	    sh.inner_reductor_handler = createSuccessHandler(init_state.inner_reductor, r.getInnerReductor());
	    // Any inner reductor handling will be boxed:
	    sh.inner_reductor_handler.container_is_boxed = true;
	}
	return sh;
    }

    private JCStatement
    maybeDebug(JCStatement stmt, String msg)
    {
	if (DEBUG_CC) {
	    String trace = "";
	    // try {
	    // 	throw new RuntimeException();
	    // } catch (RuntimeException e) {
	    // 	StringWriter sw = new StringWriter();
	    // 	PrintWriter pw = new PrintWriter(sw);
	    // 	e.printStackTrace(pw);
	    // 	trace = sw.toString().replace('\n', ';');
	    // }

	    return b.block(b.stmt(b.stringLiteral("rcc["+trace+"]:" + msg)),
			   stmt);
	} else
	    return stmt;
    }

    @Override
    public JCStatement
    failStmt(String msg)
    {
	return maybeDebug(b.breakStmt(outer_label),
			  msg);
    }

    @Override
    public JCStatement
    succeedStmt(String msg)
    {
	reductor.accept(success_handler);
	JCStatement success_stmt = success_handler.result;
	success_stmt = b.syncStatement(success_stmt);
	return maybeDebug(b.block(b.copy(success_stmt),
				  b.continueStmt(outer_label)),
			  msg);
    }

    @Override
    public JCStatement
    returnStmt(JCExpression e, String msg)
    {
	return maybeDebug(b.breakStmt(outer_label),
			  msg);
    }

    @Override
    public JCStatement
    assertTrueStmt(JCExpression e, String msg)
    {
	return maybeDebug(b.ifThen(b.negate(e),
				   failStmt("rcc-aT")),
			  msg);
    }

    @Override
    public JCStatement
    assertFalseStmt(JCExpression e, String msg)
    {
	return maybeDebug(b.ifThen(e,
				   failStmt("rcc-aF")),
			  msg);
    }

    // FIXME
    private static int private_local_counter = 0;

    class SuccessHandler implements edu.umass.pql.il.reductor.ReductorVisitor {
	public
	SuccessHandler(ReductionJoinContext.InitCommitter state)
	{
	    this.state = state;
	}

	SuccessHandler inner_reductor_handler;
	ReductionJoinContext.InitCommitter state; // used to pass state from init to here
	boolean container_is_boxed = false;
	JCStatement result;

	// ----------------------------------------

	private String
	freshLocal()
	{
	    return "ll_" + private_local_counter++;
	}

	// ----------------------------------------

	private JCStatement
	abortOnMismatch(JCExpression var_old, JCExpression var_new)
	{
	    return b.ifThen(b.isNotEqual(b.copy(var_old), b.nullLiteral()),
			    b.ifThen(b.isNotEqual(b.copy(var_old), b.copy(var_new)),
				     // FIXME: reporting value instead of key
				     b.throwException(b.id("edu.umass.pql.AmbiguousMapKeyException"), b.copy(var_new))));
	}

	// ============================================================

	@Override
	public void
	visitForall(edu.umass.pql.il.reductor.Forall r)
	{
	    throw new UnsupportedOperationException();
	}

	@Override
	public void
	visitExists(edu.umass.pql.il.reductor.Exists r)
	{
	    throw new UnsupportedOperationException();
	}

	@Override
	public void
	visitEquality(edu.umass.pql.il.reductor.EqualityReductor r)
	{
	    // Does this actually get used?
	    this.result = b.block(abortOnMismatch(state.container, getEval().loadVarImprecise(r.getArg(0))),
				  b.assign(b.copy(state.container), getEval().loadVarImprecise(r.getArg(0)))
				  );
	}

	@Override
	public void
	visitArray(edu.umass.pql.il.reductor.ArrayReductor r)
	{
	    throw new UnsupportedOperationException();
	}


	@Override
	public void
	visitDefaultMap(edu.umass.pql.il.reductor.DefaultMapReductor r)
	{
	    visitAnyMap(r, 1, 2);
	}

	@Override
	public void visitMap(edu.umass.pql.il.reductor.MapReductor r)
	{
	    visitAnyMap(r, 0, 1);
	}

	public void
	visitAnyMap(Reductor r, int key_index, int value_index)
	{
	    final String value_ty = "java.lang.Object";
	    final Type key_ty = b.varInfo(r.getArg(key_index)).type;
	    final JCExpression value_var;

	    if (r.getInnerReductor() != null)
		value_var = state.inner_reductor.container;
	    else
		value_var = b.id(freshLocal());

	    if (r.getInnerReductor() != null) {
		// FIXME: we can do this more efficiently?  We need to traverse the hash map
		// twice here!

		final String was_null_read = freshLocal();

		r.getInnerReductor().accept(this.inner_reductor_handler);
		final JCStatement update_value_var = this.inner_reductor_handler.result;

		final ComputesReferenceVisitor rv = new ComputesReferenceVisitor();

		r.getInnerReductor().accept(rv);

		final JCStatement put_stmt = b.stmt(b.assign(b.copy(value_var), b.methodapp(b.copy(state.container), "put",
											    b.castBoxedCanonical(key_ty, getEval().loadVar(r.getArg(key_index))),
											    b.copy(value_var))));

		final JCStatement write_back_stmt;
		if (rv.computes_references)
		    write_back_stmt = b.ifThen(b.id(was_null_read), put_stmt);
		else
		    write_back_stmt = put_stmt; // must always write back

		this.result = b.block(// load into value_var
				      b.varWithInit(value_var.toString(), /* <- FIXME: hack */
						    b.id(value_ty),
						    b.methodapp(b.copy(state.container), "get",
								b.cast(key_ty, getEval().loadVar(r.getArg(key_index))))),
				      b.varWithInit(was_null_read, b.boolean_type, b.isEqual(b.copy(value_var), b.nullLiteral())),
				      // initialise value if needed
				      b.ifThen(b.id(was_null_read), b.stmt(b.assign(b.copy(state.inner_reductor.container),
										    state.inner_reductor.getDefaultValue()))),
				      update_value_var,
				      write_back_stmt);
	    } else {
		final JCStatement put_stmt = b.varWithInit(value_var.toString(), /* <- FIXME: hack */
							   b.id(value_ty),
							   b.methodapp(b.copy(state.container), "put",
								       getEval().loadVar(r.getArg(key_index)),
								       getEval().loadVar(r.getArg(value_index))));
		this.result = b.block(put_stmt,
				      // b.funapp(("System.err.print"), b.stringLiteral("Add-to-set: ")),
				      // b.funapp(("System.err.print"), getEval().loadVar(r.getArg(0))),
				      // b.funapp(("System.err.print"), b.stringLiteral(" -> ")),
				      // b.funapp(("System.err.print"), getEval().loadVar(r.getArg(1))),
				      // b.funapp(("System.err.println"), b.stringLiteral("")),

				      abortOnMismatch(b.copy(value_var), getEval().loadVarBoxed(r.getArg(value_index))));
	    }
	}

	@Override
	public void
	visitSet(edu.umass.pql.il.reductor.SetReductor r)
	{
	    this.result = b.stmt(b.methodapp(b.copy(state.container), "add", getEval().loadVar(r.getArg(0))));
	}

	@Override
	public void
	visitMethodAdapter(edu.umass.pql.il.reductor.MethodAdapterReductor r)
	{
	    final Symbol.MethodSymbol method = (Symbol.MethodSymbol)r.getReductorObject();
	    final Type ty = method.type.asMethodType().restype;
	    JCExpression container = b.copy(state.container);
	    if (this.container_is_boxed)
		container = b.castBoxed(ty, container);
		
	    this.result = b.stmt(b.assign(b.copy(state.container),
					  b.funapp(method, container, getEval().loadVar(r.getArg(0)))));
	    // this.result = b.block(this.result,
	    // 			  b.funapp(("System.err.print"), b.stringLiteral("Accum: ")),
	    // 			  b.funapp(("System.err.print"), b.copy(state.container)),
	    // 			  b.funapp(("System.err.print"), b.stringLiteral("+")),
	    // 			  b.funapp(("System.err.print"), getEval().loadVar(r.getArg(0))),
	    // 			  b.funapp(("System.err.println"), b.stringLiteral(""))
	    // 			  );
	}
    }


    class ComputesReferenceVisitor implements edu.umass.pql.il.reductor.ReductorVisitor
    {
	boolean computes_references = true;

	@Override
	public void
	visitForall(edu.umass.pql.il.reductor.Forall r)
	{
	    this.computes_references = false;
	}

	@Override
	public void
	visitExists(edu.umass.pql.il.reductor.Exists r)
	{
	    this.computes_references = false;
	}

	@Override
	public void
	visitEquality(edu.umass.pql.il.reductor.EqualityReductor r)
	{
	    this.computes_references = false;
	}

	@Override
	public void
	visitDefaultMap(edu.umass.pql.il.reductor.DefaultMapReductor r)
	{
	}

	@Override
	public void
	visitMap(edu.umass.pql.il.reductor.MapReductor r)
	{
	}

	@Override
	public void
	visitArray(edu.umass.pql.il.reductor.ArrayReductor r)
	{
	}

	@Override
	public void
	visitSet(edu.umass.pql.il.reductor.SetReductor r)
	{
	}

	@Override
	public void
	visitMethodAdapter(edu.umass.pql.il.reductor.MethodAdapterReductor r)
	{
	    final Symbol.MethodSymbol method = (Symbol.MethodSymbol)r.getReductorObject();
	    final Type ty = method.type.asMethodType().restype;
	    this.computes_references = !ty.isPrimitive();
	}
    }
}
