/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package com.sun.tools.javac.comp.pqlcomp;

import edu.umass.pql.*;

public class OptFlags
{
    public VarSet inputs;
    public VarSet outputs;

    public OptFlags(VarSet inputs, VarSet outputs)
    {
	this.inputs = inputs;
	this.outputs = outputs;
    }

    // this flag is altered during cmpilation if any inferior object disagrees.  In that case, we abort and try again:
    public boolean is_all_inlined = true;

    // whether to use properly typed local variables instead of `env' accesses as much as possible (only works if all is inlined):
    public boolean minimise_env_access = true;

    // whether the output is unique (one variable) and written to the Join's `output' field of type Object
    public boolean use_result_var = false;
}
