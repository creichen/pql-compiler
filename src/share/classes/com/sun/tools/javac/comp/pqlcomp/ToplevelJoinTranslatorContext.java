/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package com.sun.tools.javac.comp.pqlcomp;

import java.util.*;

import com.sun.tools.javac.code.*;
import com.sun.tools.javac.jvm.*;
import com.sun.tools.javac.tree.*;
import com.sun.tools.javac.tree.pql.*;
import com.sun.tools.javac.util.*;
import com.sun.tools.javac.util.JCDiagnostic.DiagnosticPosition;
import com.sun.tools.javac.util.List;

import com.sun.tools.javac.code.Symbol.*;
import com.sun.tools.javac.tree.JCTree.*;
import com.sun.tools.javac.code.Type.*;

import com.sun.tools.javac.jvm.Target;

import com.sun.tools.javac.comp.Attr;
import com.sun.tools.javac.comp.PQLOptimiser.Annotation;

import static com.sun.tools.javac.code.Flags.*;
import static com.sun.tools.javac.code.Kinds.*;
import static com.sun.tools.javac.code.TypeTags.*;
import static com.sun.tools.javac.jvm.ByteCodes.*;

import edu.umass.pql.*;
import edu.umass.pql.Env;


public class ToplevelJoinTranslatorContext extends ToClassTranslatorContext
{
    protected ListBuffer<JCTree> fields = new ListBuffer<JCTree>();
    private int field_counter = 0;
    private int label_counter = 0;

    private ListBuffer<JCTree> post_clone_operations;

    @Override
    public final String
    freshLabel(String arbitrary_annotation_name)
    {
	return "L_" + arbitrary_annotation_name + "_" + (this.label_counter++);
    }

    @Override
    protected void
    addPostCloneOperation(JCTree post_clone_op)
    {
	if (this.post_clone_operations == null)
	    this.post_clone_operations = new ListBuffer<JCTree>();
	this.post_clone_operations.add(post_clone_op);
    }

    public
    ToplevelJoinTranslatorContext(Builder b, OptFlags opt_flags)
    {
	super(b);
	this.opt_flags = opt_flags;
    }

    OptFlags opt_flags;

    @Override
    public OptFlags
    getOptFlags()
    {
	return this.opt_flags;
    }

    public String
    freshField(JCExpression type_expr, JCExpression initialiser)
    {
	final String varname = "field_" + (field_counter++);
	final JCVariableDecl field = b.maker.VarDef(b.maker.Modifiers(Flags.PUBLIC),
						    b.name(varname),
						    type_expr,
						    initialiser);
	this.fields.add(field);
	return varname;
    }

    private List<JCTree>
    genPostCopyOperation(List<JCTree> list)
    {
	final JCMethodDecl copy_method =
	    b.methodDecl("postCopy", b.void_type,
			 b.block(this.post_clone_operations.toList()));
	return list.prepend(copy_method);
    }

    private List<JCTree>
    genToStringOperation(List<JCTree> list, String string)
    {
	// --- to-string string
	final JCLiteral stringlit = b.maker.Literal(TypeTags.CLASS,
						    string);
	stringlit.type = b.syms.stringType;
	final List<JCStatement> tostring_statements = List.of((JCStatement) b.maker.Return(stringlit));
	final JCIdent str_ty_ident = b.maker.Ident(b.names.fromString("java.lang.String"));
	str_ty_ident.sym = b.syms.stringType.tsym;

	final JCMethodDecl to_string_method =
	    b.methodDecl("toString", //b.maker.TypeIdent(stringlit.type),
			 str_ty_ident,
			 //b.syms.stringType,
			 b.block(tostring_statements));
	return list.prepend(to_string_method);
    }

    public JCClassDecl
    makeClass(List<? extends JCTree> initialiser_stmts, List<? extends JCTree> evaluator_stmts, List<JCTree> other_methods)
    {
	final JCBlock initialiser = (initialiser_stmts == null) ? null : b.block(initialiser_stmts);
	final JCBlock synchronized_evaluator = b.block(evaluator_stmts);

	final JCMethodDecl sync_next_method =
	    b.methodDecl("synchronizedNext", b.boolean_type,
			 synchronized_evaluator,
			 b.var("env", b.id("edu.umass.pql.Env")));

	final JCMethodDecl next_method =
	    b.methodDecl("next", b.boolean_type,
			 b.copyDesynchronized(synchronized_evaluator),
			 b.var("env", b.id("edu.umass.pql.Env")));

	List<JCTree> definitions = List.<JCTree>of(next_method,
						   sync_next_method
						   );
	if (other_methods != null)
	    definitions = definitions.prependList(other_methods);

	if (initialiser != null) {
	    final JCMethodDecl reset_method =
		b.methodDecl("reset", b.void_type,
			     initialiser,
			     b.var("env", b.id("edu.umass.pql.Env")));
	    definitions = definitions.prepend(reset_method);
	}

	definitions = definitions.prependList(this.fields.toList());

	final JCClassDecl result = b.maker.AnonymousClassDef(b.maker.Modifiers(Flags.FINAL | Flags.STATIC, List.<JCAnnotation>nil()),
							     definitions);
	System.err.println("||> " + result);
	return result;
    }

    boolean superclass_is_parallel = false;

    public void
    setParallelismDesired()
    {
	superclass_is_parallel = true;
    }

    /**
     * Write the instantiation of `decl' as the result to `a'
     */
    public void
    defaultClose(Annotation a, JCClassDecl decl)
    {
	final JCParens parens = b.parensDummy();

	final JCExpression expr = b.maker.NewClass(null,
						   List.<JCExpression>nil(),
						   b.id(this.superclass_is_parallel ?
							"edu.umass.pql.CustomParallelDecoratorJoin"
							: "edu.umass.pql.CustomDecoratorJoin"
							),
						   List.<JCExpression>of(parens),
						   decl);

	a.result_is_parallel = this.superclass_is_parallel;
	a.opt_result = expr;
	a.simple_compilation_plug = parens;
    }

    ListBuffer<JCTree> init_stmt_buffer;
    ListBuffer<JCTree> eval_stmt_buffer;

    protected void
    closeInternal(StateBlock initb, StateBlock evalb)
    {
	this.init_stmt_buffer = new ListBuffer<JCTree>();
	for (JCStatement t : b.readInputs())
	    this.init_stmt_buffer.add(b.copy(t));
	for (JCTree t : initb.getCode().toList())
	    this.init_stmt_buffer.add(t);

	// If necessary, prepend initialisation for input variables
	this.eval_stmt_buffer = new ListBuffer<JCTree>();
	for (JCStatement t : b.readInputs())
	    this.eval_stmt_buffer.add(t);

	for (JCTree t : evalb.getCode().toList())
	    this.eval_stmt_buffer.add(t);
    }

    @Override
    protected void
    closeInternalSkip()
    {
    }

    public void
    close(Annotation _)
    {
	throw new RuntimeException("Don't call plain `close()' on the toplevel.  Either call closeInternal() to pass your state here, or call writeToaAnnotation() if you're done with everything and just want the result.");
    }

    @Override
    public void
    writeToAnnotation(Attr attr, Annotation a)
    {
	if (init_stmt_buffer == null)
	    return; // Did not translate this one

	List<JCTree> init_list = init_stmt_buffer.toList();
	List<JCTree> eval_list = eval_stmt_buffer.toList();

	if (init_list.isEmpty())
	    init_list = null;


	List<JCTree> aux_methods = List.<JCTree>nil();
	aux_methods = genToStringOperation(aux_methods, "OPT[" + a.join.toString() + "]");
	if (this.post_clone_operations != null) {
	    aux_methods = genPostCopyOperation(aux_methods);
	}

	defaultClose(a, this.makeClass(init_list,
				       eval_list,
				       aux_methods));

	if (a.opt_result != null)
	    a.opt_result.accept(attr);
    }

    // --------------------

    public JoinTranslatorContext
    newImplicitlyStatefulJoinTranslatorContext()
    {
	return new ImplicitlyStatefulSingleJoinContext(this, this.continuation_context_factory, true);
    }

    public JoinTranslatorContext
    newExplicitlyStatefulJoinTranslatorContext()
    {
	return new ExplicitlyStatefulSingleJoinContext(this, this.continuation_context_factory);
    }

    public JoinTranslatorContext
    newConjunctiveJoinTranslatorContext()
    {
	return new ConjunctiveJoinContext(this, this.continuation_context_factory);
    }

    @Override
    public JoinTranslatorContext
    newParallelReductionJoinContext(Reductor[] reductors)
    {
	return new ReductionJoinContext(this, this.getContinuationContextFactory(), reductors, true);
    }

    @Override
    public JoinTranslatorContext
    newNonParallelReductionJoinContext(Reductor[] reductors)
    {
	return new ReductionJoinContext(this, this.getContinuationContextFactory(), reductors, false);
    }
}
