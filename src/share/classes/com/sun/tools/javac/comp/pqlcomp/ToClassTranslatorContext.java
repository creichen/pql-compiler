/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package com.sun.tools.javac.comp.pqlcomp;

import java.util.*;

import com.sun.tools.javac.code.*;
import com.sun.tools.javac.jvm.*;
import com.sun.tools.javac.tree.*;
import com.sun.tools.javac.tree.pql.*;
import com.sun.tools.javac.util.*;
import com.sun.tools.javac.util.JCDiagnostic.DiagnosticPosition;
import com.sun.tools.javac.util.List;

import com.sun.tools.javac.code.Symbol.*;
import com.sun.tools.javac.tree.JCTree.*;
import com.sun.tools.javac.code.Type.*;

import com.sun.tools.javac.jvm.Target;

import com.sun.tools.javac.comp.Attr;
import com.sun.tools.javac.comp.PQLOptimiser.Annotation;

import static com.sun.tools.javac.code.Flags.*;
import static com.sun.tools.javac.code.Kinds.*;
import static com.sun.tools.javac.code.TypeTags.*;
import static com.sun.tools.javac.jvm.ByteCodes.*;

import edu.umass.pql.*;
import edu.umass.pql.Env;

/**
 * Toplevel translation context interface
 *
 * This interface represents the class that all generated code goes into.
 * Implemented in ToplevelJoinTranslatorContext.
 * The basic translation idea is as follows:
 *
 * Assume you're translating join `j' with annotation `a'.  Follow these steps:
 * <ul>
 *   <li> Generate a fresh ToplevelJoinTranslatorContext. Use this as JoinTranslationContextFactory for a.translateJoin().  </li>
 *   <li> In a.translateJoin(), use the factory `new...' methods of the Factory to generate a suitable join translation context for j. </li>
 *   <li> Translate j into that context.  If j is a control structure, recurse on j's subjoins by calling `translateJoin(subjoin)' on each subjoin. </li>
 *   <li> Call close() on the join translation context, after you have processed j's last element.
 * </ul>
 *
 * Assume you're trying to build a new join translation context.  Follow these steps:
 * <ul>
 *   <li> Extend JoinTranslationContextFactory and all implementing classes with a suitable factory method.  It's okay to have that method fail in contexts it's not intended for, and it's also okay to have it return `this'. </li>
 *   <li> For each factory-generated join translation context jct, implement JoinTranslatorContext.  In particular:
 *   <li> Override freshField() if you can translate fields into locals.
 *   <li> override doBuildJoin(j, a) if your context is for multiple joins (i.e., for a control structure) and you need preprocessing/postprocessing after handling each join.
 *   <li> If you contain subjoins, implement closeInternal().
 *   <li> Implement close().  Call this.getParentTranslatorContext().closeInternal() to pass down the result of your context translation.
 * </ul>
 */
public abstract class ToClassTranslatorContext implements JoinTranslatorContextFactory
{
    public Builder b;

    protected ToClassTranslatorContext
    getParentTranslatorContext()
    {
	return null;
    }

    private int localvar_counter = 0;

    public ToClassTranslatorContext(Builder builder)
    {
	this.b = builder;
	this.self_expression = builder.id("body");
    }

    private int local_var_counter;

    int
    getFreshLocalVarNumber()
    {
	return local_var_counter++;
    }

    public boolean
    getParallelismSupported()
    {
	return true;
    }

    public void
    setParallelismDesired()
    {
	// default no-op; override if support desired.
    }

    int state_nr = 0;

    public int
    allocateStateId()
    {
	return state_nr++;
    }

    /**
     * Release state numbers allocated by allocateStateNr(), if applicable
     *
     * Will lower the state number counter if possible.
     */
    public void
    relinquishStates(int ... state_nrs)
    {
	Arrays.sort(state_nrs);
	int i = state_nrs.length - 1;
	while (i >= 0 && state_nrs[i--] + 1 == this.state_nr)
	    --this.state_nr;
    }

    // ----------------------------------------
    // ccf

    protected ContinuationContextFactory continuation_context_factory = new SingleContinuationContext.Factory();

    @Override
    public ContinuationContextFactory
    getContinuationContextFactory()
    {
	return this.continuation_context_factory;
    }

    @Override
    public void
    setContinuationContextFactory(ContinuationContextFactory f)
    {
	this.continuation_context_factory = f;
    }

    @Override
    public void
    addContinuationContextOverride(ContinuationContextOverride override)
    {
	setContinuationContextFactory(this.getContinuationContextFactory().applyOverride(override));
    }

    public abstract String
    freshLabel(String arbitrary_annotation_name);

    // ----------------------------------------
    // Fields

    public abstract String
    freshField(JCExpression type_expr, JCExpression initialiser);

    public final String
    freshField(Type t, JCExpression initialiser)
    {
	return freshField(b.typeExpr(t), initialiser);
    }

    public static class ParallelAccess
    {
	public String count_var; // variable that stores the counter.  After init(), this is set to the least valid value.
	public String max_var;  // variable that stores the max-plus-one.  After init(), this value is initialised properly.

	public ParallelAccess(String count_var, String max_var)
	{
	    this.count_var = count_var;
	    this.max_var = max_var;
	}
    }

    /**
     * Allocates two fields indicating current-counter (initially, minimum) and max-counter+1.  These fields may be remembered internally for parallelisation.
     *
     * counter_type must be integral
     */
    public ParallelAccess
    parallelAccessFields(Type counter_type)
    {
	return new ParallelAccess(this.freshField(counter_type, null),
				  this.freshField(counter_type, null));
    }

    /**
     * Complete processing of this annotation translator
     *
     * @param attr The attr to use for post-translation static analysis of the generated AST
     * @param a The annotation to write the result to, if there is a result
     */
    public abstract void
    writeToAnnotation(Attr attr, Annotation a);

    public abstract void
    close(Annotation a);

    /**
     * Internal code generator method
     */
    protected abstract void
    closeInternal(StateBlock init_stmts, StateBlock eval_stmts);

    /**
     * Internal non-code generator method: signal to parent that code generation for this context is not possible and interpretation must be used
     */
    protected abstract void
    closeInternalSkip();

    /**
     * Add an operation to be executed after any copyRecursively() operation is executed on the generated CustomJoin
     *
     */
    protected abstract void
    addPostCloneOperation(JCTree post_clone_op);

    private JCExpression self_expression;

    /**
     * Retrieve a JCExpression that will compute a join expression that represents the object currently being translated
     */
    public JCExpression
    getJoinExpressionForSelf()
    {
	return this.self_expression;
    }

    public void
    setJoinExpressionForSelf(JCExpression s)
    {
	this.self_expression = s;
    }

    // ----------------------------------------
    // Automatically nesting Join factory

    /**
     * Obtain a join context with explicit state handling.
     */
    public abstract JoinTranslatorContext
    newExplicitlyStatefulJoinTranslatorContext();

    /**
     *
     */
    public abstract JoinTranslatorContext
    newImplicitlyStatefulJoinTranslatorContext();

    /**
     *
     */
    public abstract JoinTranslatorContext
    newConjunctiveJoinTranslatorContext();
}
