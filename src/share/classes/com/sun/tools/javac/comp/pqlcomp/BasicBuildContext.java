/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package com.sun.tools.javac.comp.pqlcomp;

import java.util.*;

import com.sun.tools.javac.code.*;
import com.sun.tools.javac.jvm.*;
import com.sun.tools.javac.tree.*;
import com.sun.tools.javac.tree.pql.*;
import com.sun.tools.javac.util.*;
import com.sun.tools.javac.util.JCDiagnostic.DiagnosticPosition;
import com.sun.tools.javac.util.List;

import com.sun.tools.javac.code.Symbol.*;
import com.sun.tools.javac.tree.JCTree.*;
import com.sun.tools.javac.code.Type.*;

import com.sun.tools.javac.jvm.Target;

import com.sun.tools.javac.comp.Attr;
import com.sun.tools.javac.comp.PQLOptimiser.Annotation;

import static com.sun.tools.javac.code.Flags.*;
import static com.sun.tools.javac.code.Kinds.*;
import static com.sun.tools.javac.code.TypeTags.*;
import static com.sun.tools.javac.jvm.ByteCodes.*;

import edu.umass.pql.*;
import edu.umass.pql.Env;

import java.io.StringWriter;
import java.io.PrintWriter;

public class BasicBuildContext extends BuildContext
{
    BuildContext init;
    JoinTranslatorContext tc;
    ContinuationContext cctx;

    private JCExpression queued_assertion = null;
    private boolean abort_if_assertion_true;
    protected boolean has_unconditional_return = false;
    protected boolean is_initialiser;

    public void
    setContinuationContext(ContinuationContext cctx)
    {
	this.cctx = cctx;
    }

    public boolean
    hasUnconditionalReturn()
    {
	return this.has_unconditional_return;
    }

    public ContinuationContext
    getContinuationContext()
    {
	return this.cctx;
    }

    /**
     * Just a convenience function
     */
    public final void
    overrideContinuationContext(ContinuationContextOverride override)
    {
	this.setContinuationContext(override.override(this.getContinuationContext()));
    }

    public BasicBuildContext(Builder b,
			     ContinuationContext cctx,
			     int state_nr)
    {
	super(b, state_nr);
	this.init = init;
	this.setContinuationContext(cctx);
    }

    public void
    add(JCTree stmt)
    {
	if (tc == null) {
	    System.err.println("Uninitialised " + this.getClass().toString());
	    throw new Error();
	}

	    // String trace = "";
	    // try {
	    // 	throw new RuntimeException();
	    // } catch (RuntimeException e) {
	    // 	StringWriter sw = new StringWriter();
	    // 	PrintWriter pw = new PrintWriter(sw);
	    // 	e.printStackTrace(pw);
	    // 	trace = sw.toString().replace('\n', ';');
	    // }
	    // super.add(b.funapp("System.err.println", b.stringLiteral(trace)));

	super.add(stmt);
    }

    public void
    setInit(BuildContext init, JoinTranslatorContext tc, boolean is_initialiser)
    {
	if (tc == null)
	    throw new Error();
	this.init = init;
	this.tc = tc;
	this.is_initialiser = is_initialiser;
    }

    // ----------------------------------------

    @Override
    public String
    freshLocal(JCExpression type_expr, JCExpression initialiser)
    {
	if (tc == null)
	    throw new RuntimeException("noninited w/ state = " + this.state_id + " in " + this.getClass());
	final String varname = "t_" + (tc.getFreshLocalVarNumber());
	final JCVariableDecl field = b.maker.VarDef(b.maker.Modifiers(Flags.FINAL),
						    b.name(varname),
						    type_expr,
						    initialiser);
	this.add(field);
	return varname;
    }

    @Override
    public JCExpression
    loadVarImprecise(int var)
    {
	final PQLVarInfo vi = b.varInfo(var);
	if (vi.isConstant()) {
	    JCExpression retval;
	    if (vi.isLiteral())
		retval = b.copy(vi.expr);
	    else {
		final String varname = tc.freshField(vi.type, null);
		init.add(b.assign(varname, b.cast(vi.type, b.envVarAccess(var))));
		retval = b.id(varname);
	    }
	    return retval;
	} else
		return //b.envVarAccess(var);
			loadVar(var);
    }

    @Override
    public void
    storeVar(int var, JCExpression value)
    {
	if (Env.isWildcard(var))
	    add(b.stmt(value));
	else {
	    final String f = b.getVarFieldName(var);

	    if (f == null)
		add(b.stmt(b.assign(b.envVarAccess(var),
				    value)));
	    else
		add(b.stmt(b.assign(b.id(f), value)));
	}
    }

    public
    JCStatement
    failStmt(String msg)
    {
	return cctx.failStmt("bbc:" + msg);
    }

    public
    JCStatement
    succeedStmt(String msg)
    {
	return cctx.succeedStmt("bbc:" + msg);
    }

    public
    JCStatement
    assertTrueStmt(JCExpression e, String msg)
    {
	return cctx.assertTrueStmt(e, "bbc:" + msg);
    }

    public
    JCStatement
    assertFalseStmt(JCExpression e, String msg)
    {
	return cctx.assertFalseStmt(e, "bbc: " + msg);
    }

    void
    finishAssertion()
    {
	if (queued_assertion != null) {
	    this.code.add(b.ifThen(b.negateIf(this.queued_assertion, !this.abort_if_assertion_true),
				   this.failStmt("bbc-finishAssertion")
				   ));
	    this.queued_assertion = null;
	}
    }


    @Override
    public void
    assertTrue(JCExpression expr)
    {
	this.finishAssertion();
	this.queued_assertion = expr;
	this.abort_if_assertion_true = false;
    }

    @Override
    public void
    assertFalse(JCExpression expr)
    {
	this.finishAssertion();
	this.queued_assertion = expr;
	this.abort_if_assertion_true = true;
    }

    protected void
    beforeAddCallback() {}

    @Override
    protected final void
    beforeAdd()
    {
	this.finishAssertion();
	beforeAddCallback();
	if (this.has_unconditional_return)
	    throw new RuntimeException("Attempt to add to terminated block");
    }

    @Override
    public void
    fail()
    {
	if (queued_assertion != null) {
	    final JCExpression assertion = queued_assertion;
	    this.queued_assertion = null;
	    this.add(this.cctx.returnStmt(b.negateIf(assertion, !this.abort_if_assertion_true), "bbc-fail0"));
	} else
	    this.add(this.failStmt("bbc-fail1"));
	this.has_unconditional_return = true;
    }

    @Override
    public void
    succeed()
    {
	if (queued_assertion != null) {
	    final JCExpression assertion = queued_assertion;
	    this.queued_assertion = null;
	    this.add(this.cctx.returnStmt(b.negateIf(assertion, this.abort_if_assertion_true), "bbc-succeed0"));
	} else
	    this.add(this.succeedStmt("bbc-succeed1"));
	this.has_unconditional_return = true;
    }

    public StateBlock
    getStateBlock()
    {
	return new StateBlock(this.getBody(), state_id, true);
    }

    public ListBuffer<JCTree>
    getBody()
    {
	if (!this.is_initialiser) {
	    // if (this.queued_assertion != null) {
	    // 	System.err.println("pre-code: " + code);
	    // 	this.code.add(this.assertTrueStmt(b.negateIf(this.queued_assertion, this.abort_if_assertion_true), "bbc-getBody"));
	    // 	System.err.println("post-code: " + code);
	    // 	this.queued_assertion = null;
	    // } else
	    if (!this.has_unconditional_return)
	      	this.succeed();
	}

	return code;
    }

    @Override
    public void
    prependStmt(JCTree t)
    {
	this.code = this.code.prepend(t);
    }

    protected ListBuffer<JCTree>
    getCode()
    {
	return this.getBody();
    }

    // ================================================================================
    // control structures
    @Override
    public void
    ifThenElse(JCExpression cond,
	       Construction truebranch,
	       Construction falsebranch)
    {
	BuildBlockContext true_ctx = new BuildBlockContext(this);
	BuildBlockContext false_ctx = new BuildBlockContext(this);

	JCStatement true_stmt;
	JCStatement false_stmt = null;

	truebranch.construct(true_ctx);
	true_stmt = true_ctx.genBlock();

	if (falsebranch != null) {
	    falsebranch.construct(false_ctx);
	    false_stmt = false_ctx.genBlock();
	}
	add(b.ifThenElse(cond, true_stmt, false_stmt));
    }

    @Override
    public void
    ifThen(JCExpression cond,
	   Construction truebranch)
    {
	ifThenElse(cond, truebranch, null);
    }

    @Override
    public void
    whileDo(JCExpression cond,
	    Construction body)
    {
	BuildBlockContext body_ctx = new BuildBlockContext(this);

	body.construct(body_ctx);
	final JCStatement body_stmt = body_ctx.genBlock();

	add(b.whileLoop(cond, body_stmt));
    }


    // --

    public static class BuildBlockContext extends BasicBuildContext
    {
	BasicBuildContext owner;
	public BuildBlockContext(BasicBuildContext owner)
	{
	    super(owner.b, owner.getContinuationContext(), owner.getStateId());
	    this.setInit(owner.tc.init, owner.tc, false);
	    this.owner = owner;
	}

	public JCStatement
	genBlock()
	{
	    if (this.code.length() == 1) {
		JCTree body = this.code.elems.head;
		if (body instanceof JCStatement)
		    return (JCStatement) body;
		else
		    return b.stmt((JCExpression) body);
	    }
	    return b.block(this.code.toList());
	}
    }
}
