/* Copyright (C) 2011, Christoph Reichenbach
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */
package com.sun.tools.javac.tree.pql;

import java.util.*;

import com.sun.tools.javac.tree.JCTree.*;
import com.sun.tools.javac.tree.*;
import com.sun.tools.javac.util.Log;
import com.sun.tools.javac.code.Type;
import com.sun.tools.javac.code.Types;
import com.sun.tools.javac.code.TypeTags;
import com.sun.tools.javac.code.Symtab;
import com.sun.source.tree.Tree.Kind;

import edu.umass.pql.*;
import edu.umass.pql.opt.Optimizer;
import com.sun.tools.javac.code.Symbol;
import javax.lang.model.type.TypeKind;

/**
 * PQL query representation
 */
public class PQLBody
{
    public final static int NO_RESULT_VAR = -1;

    public PQLContext context;
    public Join join;
    public int result_var = NO_RESULT_VAR;

    class PQLOptTypeSystem implements edu.umass.pql.opt.TypeSystem<Type>
    {
	public Type getType(int var)
	{
	    return context.varType(var);
	}

	public Type getIntType() { return context.syms.intType; }
	public Type getCharType() { return context.syms.charType; }
	public Type getBooleanType() { return context.syms.booleanType; }
	public Type getByteType() { return context.syms.byteType; }
	public Type getShortType() { return context.syms.shortType; }
	public Type getLongType() { return context.syms.longType; }
	public Type getTypeForJAVA_TYPE(Object t) { return (Type) t; };

	/**
	 * Check for subtypedness property
	 */
	public boolean
	isSubtypeOf(Type sub, Type sup)
	{
	    return context.types.isAssignable(sub, sup);
	}
    }

    public PQLOptTypeSystem pql_type_system = new PQLOptTypeSystem();

public Join
sillySplit(Join j)
{
	class JS extends JoinVisitor {
		public Join result;

		public void
		visitDefault(Join j)
		{
			result = j;
		}

		public void
		visitControlStructure(ControlStructure cs)
		{
			for (int i = 0; i < cs.getComponentsNr(); i++)
				cs.setComponent(i, sillySplit(cs.getComponent(i)));
			result = cs;
		}

		public void
		visitConjunctiveBlock(AbstractBlock.Conjunctive b)
		{
			for (int i = 0; i < b.getComponentsNr(); i++)
				b.setComponent(i, sillySplit(b.getComponent(i)));
			if (b.getComponentsNr() > 2) {
				final int cnr = b.getComponentsNr();
				final int firstpart = cnr >> 1;
				Join[] first = new Join[firstpart + 1];
				Join[] second = new Join[cnr - firstpart];
				for (int i = 0; i < firstpart; i++)
					first[i] = b.getComponent(i);
				for (int i = 0; i < cnr - firstpart; i++)
					second[i] = b.getComponent(i + firstpart);
				Join second_half = PQLFactory.ConjunctiveBlock(second);
				first[firstpart] = second_half;
				result = PQLFactory.ConjunctiveBlock(first);
			}
			else
				result = b;
		}
	}

	JS js = new JS();
	j.accept(js);
	return js.result;
}

    public PQLBody(PQLGenerator initialiser)
    {
	final PQLGenerator.QVar result = initialiser.result;
	this.result_var = result.asVar();
	this.join = initialiser.getResultJoin();
	this.context = initialiser.context;
	final VarSet input_vars = this.context.inputVarSet();
	final VarSet output_vars = new VarSet();
	output_vars.insert(this.result_var);

	System.err.println("<> Preparing Read-flag assignment with vars " + input_vars);
	if (!initialiser.hasError())
	    try {
		//PQLFactory.Analysis.setReadFlags(this.join, input_vars.copy());
		System.err.println("<> base      : " + this.join);
		this.join = Optimizer.selectAccessPathRecursively(Env.EMPTY,
								  input_vars,
								  this.join,
								  true);
		System.err.println("<> accesspath: " + this.join);
		this.join.accept(new edu.umass.pql.il.ReadFlagSetterVisitor(input_vars));
		System.err.println("<> readflags : " + this.join);
		PQLFactory.Analysis.markUnusedWrites(this.join, output_vars);
		System.err.println("<> wildcarded: " + this.join);
		this.join = Optimizer.removeUnnecessaryTypeChecks(this.join,
								  this.pql_type_system);
		System.err.println("<> type-elim : " + this.join);
		this.join = Optimizer.nestReductionRecursively(this.join);
		System.err.println("<> reduc-nest: " + this.join);
		this.join = Optimizer.selectAccessPathRecursively(Env.EMPTY,
								  input_vars,
								  this.join,
								  true);
		System.err.println("<> accesspat2: " + this.join);
// this.join = sillySplit(join);
// 		System.err.println("********************************************************************************\n<> silly-split: " + this.join);
	    } catch (Exception e) {
		System.err.println(this);
		e.printStackTrace();
	    }
    }

    public static PQLBody
    generate(Log log, Symtab symtab, Types types,
	     LinkedList<JCExpression> constants, JCTree tree)
    {
	PQLGenerator v = new PQLGenerator(log, symtab, types, constants);
	v.doVisitRoot(tree);

	return new PQLBody(v);
    }

    public static Type
    normalisedType(Symtab syms, Types types, Type ty)
    {
	switch (PQLContext.pqltypeStatic(types, ty)) {
	case Env.TYPE_INT:
	    return syms.intType;
	case Env.TYPE_LONG:
	    return syms.longType;
	case Env.TYPE_DOUBLE:
	    return syms.doubleType;
	default:
	    return syms.objectType;
	}
    }

    @Override
    public String
    toString()
    {
	StringBuffer context = new StringBuffer();
	String body = ">> " + join + "\n";
	String result = "";

	final Iterator<PQLVarInfo> context_variables = this.context.contextVariableIterator();
	while (context_variables.hasNext()) {
	    PQLVarInfo vi = context_variables.next();
	    if (vi != null)
		context.append(vi.toString() + "\n");
	}
	final Iterator<PQLVarInfo> context_exprs = this.context.contextExpressionIterator();
	while (context_exprs.hasNext()) {
	    PQLVarInfo vi = context_exprs.next();
	    if (vi != null)
		context.append(vi.toString() + "\n");
	}

	if (this.result_var != NO_RESULT_VAR)
	    result =  "result = " + Env.showVar(this.result_var) + " : " +  this.context.varType(this.result_var) + "\n";

	return context.toString() + body + result;
    }

    // compute the access flags needed for a call to pql.Query.query() with the given set of flags
    public int
    pqlQueryAccessFlags()
    {
	int retval = 0;
	for (int i = PQLContext.MIN_TYPE; i <= PQLContext.MAX_TYPE; i++)
	    if (!this.context.variable_table[i].isEmpty())
		retval |= 1 << i;
	return retval;
    }

    public PQLVarTable[]
    variableTable()
    {
	return this.context.variable_table;
    }

    public int
    pqlQueryTablesNr()
    {
	int retval = 0;
	for (int i = PQLContext.MIN_TYPE; i <= PQLContext.MAX_TYPE; i++)
	    if (!this.context.variable_table[i].isEmpty())
		retval++;
	return retval;
    }
}
