/***************************************************************************
 Copyright (C) 2011 Christoph Reichenbach <creichen@gmail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public Licence as
 published by the Free Software Foundaton; either version 2 of the
 Licence, or (at your option) any later version.

 It is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 merchantability or fitness for a particular purpose. See the
 GNU General Public Licence for more details.

 You should have received a copy of the GNU General Public Licence
 along with this program; see the file COPYING. If not, write to
 the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 Boston, MA 02111-1307, USA.

***************************************************************************/

package com.sun.tools.javac.tree.pql;
import edu.umass.pql.*;
import edu.umass.pql.il.*;
import edu.umass.pql.il.meta.*;
import edu.umass.pql.opt.Optimizer;
import java.util.*;

public class PQLRangeInference
{
    VarSet vars_of_interest;
    VarSet range_inferred_vars; // the variables we do range inference for
    JoinPredicate join_checker;
    Map<Integer, VarSet> dep_map;

    final VarSet EMPTY_VARSET = new VarSet();

    Join
    analyse(Join j)
    {
	RIContext context = new RIContext();
	j.accept(context);
	return context.getResult();
    }

    VarSet
    deps(int i)
    {
	VarSet v = dep_map.get(i);
	if (v == null)
	    return EMPTY_VARSET;
	else
	    return v;
    }

    PQLRangeInference(VarSet range_inferred_vars, JoinPredicate joinChecker, Map<Integer, VarSet> dependencyMap)
    {
	this.dep_map = dependencyMap;
	this.vars_of_interest = new VarSet();
	this.range_inferred_vars = range_inferred_vars;
	this.join_checker = joinChecker;

	// deps are already transitive, so this suffic
	//		System.err.println("regs = " + range_inferred_vars);
	//		System.err.println("deps = " + dependencyMap);
	for (int var : range_inferred_vars) {
	    var = Env.readVar(var);
	    //			System.err.println("reg : " + var);
	    this.vars_of_interest.addAll(deps(Env.readVar(var)));
	}
	//		System.err.println("voi = " + this.vars_of_interest);
    }

    public static Join
    inferRange(Join join, VarSet range_inferred_vars)
    {
	PQLRangeInference slicer = new PQLRangeInference(range_inferred_vars,
							 JoinPredicate.rangeInference(range_inferred_vars),
							 Optimizer.bindingDependencyMap(join));
	return slicer.analyse(join);
    }

    boolean
    bindsVarOfInterest(PQLParameterized j)
    {
	for (int i = j.getReadArgsNr(); i < j.getArgsNr(); i++)
	    if (j.isValidArg(i)
		&& this.vars_of_interest.contains(j.getArg(i)))
		return true;
	return false;
    }

    boolean
    reductorBindsVarOfInterest(Reductor r)
    {
	if (bindsVarOfInterest(r))
	    return true;
	if (r.getInnerReductor() != null)
	    return this.reductorBindsVarOfInterest(r.getInnerReductor());
	return false;
    }

    // ----------------------------------------
    // Let's say we have `forall z: T', and the translation of T is U.
    // Then we obtain `z' via

    // Reduce[SET(?z'): !s]: V
    // CONTAINS(?s, !z)

    // for fresh `s' and `z'', where V is derived from U as follows:
    // - both conjunctive and disjunctive blocks treated the same
    // - extract conjunctive block starting with everything that computes deps of z
    // - end conjunctive block with disjunction that contains all bindings of z found anywhere
    // - for performance reasons, conjunctive within disjunctive should be kept in there, so we could wind up with
    //   { a; b; c; [ d | { e ; f; [ g | h ]; } | i ] }
    //   in the end.
    // - As for Bool(y):  extract inner body and use the entire thing again if (y) is a dep.
    // - finally rename all locally bound variables x to x'
    //
    // Maintain the disjunctive choices separately for all range-inferred-vars

    class RIContext extends JoinVisitor
    {
	Join result = null;
	public Map<Integer, ArrayList<Join>> disjunctive_choices_map = new HashMap<Integer, ArrayList<Join>>();

	void
	addDisjunctiveChoice(int var, Join j)
	{
	    ArrayList<Join> al = disjunctive_choices_map.get(var);
	    if (al == null) {
		al = new ArrayList<Join>();
		disjunctive_choices_map.put(var, al);
	    }
	    al.add(j.copyRecursively());
	}

	public Join
	getResult()
	{
	    // Complicated control flow.  Could simplify but would require more expensive computations (more flattening etc.)

	    if (this.disjunctive_choices_map.size() == 0) {
		if (this.result == null)
		    return PQLFactory.TRUE;
		else
		    return this.result;
	    } else {
		ArrayList<Join> disjunction_sequence = new ArrayList<Join>();
		for (Map.Entry<Integer, ArrayList<Join>> kvb : this.disjunctive_choices_map.entrySet()) {
		    final ArrayList<Join> disjunctive_choices = kvb.getValue();
		    final Join disjunction = PQLFactory.Disjunction(disjunctive_choices);
		    disjunction_sequence.add(disjunction);
		}

		final Join all_disjunctions_in_conjunction = PQLFactory.Conjunction(disjunction_sequence);

		if (this.result == null)
		    return all_disjunctions_in_conjunction;
		else
		    return PQLFactory.flattenBlock(PQLFactory.ConjunctiveBlock(this.result, all_disjunctions_in_conjunction));
	    }
	}

	/**
	 * Merge this RIContext's resuls with a parent from a disjunction context
	 *
	 * When this context represents a disjunction, we try to merge its results into the
	 * set of disjunctive choices of the parent context economically.
	 */
	public void
	addResultToDisjunctiveBlock(RIContext master_block)
	{
	    if (this.result == null) {
		for (Map.Entry<Integer, ArrayList<Join>> kvb : this.disjunctive_choices_map.entrySet()) {
		    for (Join j: kvb.getValue())
			master_block.addDisjunctiveChoice(kvb.getKey(), j);
		}
		return;
	    }
	    else master_block.visitDefault(this.getResult());
	}

	public void
	addBinding(Join j)
	{
	    j = j.copyRecursively();
	    // Eliminate bindings for uninteresting vars (those might be undesired checks)
	    for (int i = j.getReadArgsNr(); i < j.getArgsNr(); i++)
		if (j.isValidArg(i) && !range_inferred_vars.contains(j.getArg(i)))
		    j.setArg(i, Env.WILDCARD);

	    // Remember this, for each of the valid choices
	    for (int i = j.getReadArgsNr(); i < j.getArgsNr(); i++) {
		int v = j.getArg(i);
		if (v != Env.WILDCARD)
		    addDisjunctiveChoice(Env.writeVar(v), j);
	    }
	    //    this.disjunctive_choices.add(j);
	}

	// --------------------------------------------------------------------------------

	@Override
	public void visitDefault(Join it)
	{
	    if (PQLRangeInference.this.join_checker.contains(it))
		this.addBinding(it);

	    if (bindsVarOfInterest(it))
		this.result = it.copyRecursively();
	    else
		this.result = null;
	}

	@Override
	public void visitControlStructure(ControlStructure sp) { throw new RuntimeException("Shouldn't be called"); }
	@Override
	public void visitAnyBlock(AbstractBlock b) { throw new RuntimeException("Shouldn't be called"); }

	@Override
	public void
	visitConjunctiveBlock(AbstractBlock.Conjunctive b)
	{
	    ArrayList<Join> results = new ArrayList<Join>();

	    for (int i = 0; i < b.getComponentsNr(); i++) {
		final Join j = b.getComponent(i);
		j.accept(this);
		if (this.result != null)
		    results.add(this.result);
	    }

	    if (results.size() == 0)
		this.result = null;
	    if (results.size() == 1)
		this.result = results.get(0);
	    else
		this.result = PQLFactory.ConjunctiveBlock(results);
	}

	@Override
	public void visitDisjunctiveBlock(AbstractBlock.Disjunctive b)
	{
	    for (int i = 0; i < b.getComponentsNr(); i++) {
		final Join j = b.getComponent(i);
		RIContext other = new RIContext();
		j.accept(other);
		other.addResultToDisjunctiveBlock(this);
	    }
	}

	@Override
	public void visitSelectPath(SelectPath sp)
	{
	    sp.getComponent(0).accept(this);
	}

	@Override
	public void visitSchedule(Schedule sp)
	{
	    sp.getComponent(0).accept(this);
	}

	@Override
	public void visitBool(NoFail.Bool b)
	{
	    b.getComponent(0).accept(this);
	    this.visitDefault(b); // set this.result appropriately
	}

	@Override
	public void visitNot(Not n)
	{
	    n.getComponent(0).accept(this);
	    this.result = null;
	}

	@Override
	public void visitNoFail(NoFail b)
	{
	    b.getComponent(0).accept(this);
	    this.result = null;
	}

	@Override
	public void visitReduction(Reduction red)
	{
	    for (Reductor r: red.getReductors()) {
		if (PQLRangeInference.this.reductorBindsVarOfInterest(r)) {
		    this.result = red.copyRecursively();
		    return;
		}
	    }

	    this.result = null;
	}
    }
}
