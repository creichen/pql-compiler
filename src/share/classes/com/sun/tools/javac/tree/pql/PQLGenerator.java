/* Copyright (C) 2011, Christoph Reichenbach
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */
package com.sun.tools.javac.tree.pql;

import java.util.*;

import com.sun.tools.javac.tree.*;
import com.sun.tools.javac.tree.JCTree.*;
import com.sun.tools.javac.code.Type;
import com.sun.tools.javac.code.Types;
import com.sun.tools.javac.code.TypeTags;
import com.sun.tools.javac.code.Symtab;
import com.sun.source.tree.Tree.Kind;

import edu.umass.pql.*;
import edu.umass.pql.opt.Optimizer;
import com.sun.tools.javac.code.Symbol;
import com.sun.tools.javac.util.Log;
import com.sun.tools.javac.util.JCDiagnostic.DiagnosticPosition;
import javax.lang.model.type.TypeKind;


/**
 * JCTree to PQL Join translation code
 *
 * The translation algorithm uses the JCTree visitor to traverse the source AST.
 * It stops at nodes that are conetant subexpressions (constant_subexprs, initialised by the
 * constructor).  Local results are stored in the `current_aggregator' and `result' variables.
 */
public final class PQLGenerator extends JCTree.Visitor
{
    static final VarSet empty_var_set = new VarSet();

    public final static int NO_RESULT_VAR = PQLBody.NO_RESULT_VAR;
    final static int TYPE_INT = VarConstants.TYPE_INT;
    final static int TYPE_LONG = VarConstants.TYPE_LONG;
    final static int TYPE_DOUBLE = VarConstants.TYPE_DOUBLE;
    final static int TYPE_OBJECT = VarConstants.TYPE_OBJECT;

    final PQLContext context; // variable information
    final Log log;

    // identify variables, both query and external
    final HashMap<Symbol, Integer> variables = new HashMap<Symbol, Integer>();

    /**
     * constant subexpressions.  Most of these subexpressions are identified before PQLGenerator is run.
     * Some special expressions may break up 
     */
    final HashMap<JCTree, Integer> constant_subexprs = new HashMap<JCTree, Integer>();

    // Result storage.
    // current_aggretator: stores the joins and equalities accumulated so far
    // result: stores the variable (and any joins needed to compute that variable) for an arithmetic subexpression
    private ConjunctiveJoinAggregator current_aggregator = new ConjunctiveJoinAggregator();
    public QVar result = new ContextQVar(current_aggregator);

    private static int mc_idc = 0;
    private static int c_idc = 0;

    boolean has_error = false;

    public boolean
    hasError()
    {
	return this.has_error;
    }

    void
    error(DiagnosticPosition pos, String msg, Object ... args)
    {
	log.error(pos, msg, args);
	this.has_error = true;
    }

    void
    clearResult()
    {
	this.result = new ContextQVar(current_aggregator);
    }

    private int constant_one_var = NO_RESULT_VAR;

    public int
    getConstantOne()
    {
	if (constant_one_var == NO_RESULT_VAR) {
	    JCLiteral lit = TreeMaker.FakeLiteral(TypeTags.BOOLEAN, 1);
	    lit.type = context.syms.booleanType;
	    setConstantOne(lit);
	}
	return constant_one_var;
    }

    private void
    setConstantOne(JCExpression lit)
    {
	final int var = this.context.nextVarForType(this.context.syms.intType);
	final PQLVarInfo vi = new PQLVarInfo(this.context.syms.intType, var, lit);
	constant_one_var = allocateVariable(vi);
    }

    public Join
    getResultJoin()
    {
	final Join result = this.current_aggregator.force();
	return result;
    }

    public static class ForcedJoinPair
    {
	Join binding_joins, nonbinding_joins;
    }


    /**
     * Collect joins and equalities
     *
     * This aggregator collects a set of joins and equalities.  When it is `forced', it manifests the equalities
     * by variable substitution (where possible) and introduction of equality checks (where necessary).
     */
    class ConjunctiveJoinAggregator
    {
	/**
	 * Represent equal variables and provide functionality for merging them
	 */
	class MergeContext
	{
	    VarSet vars = new VarSet(); // all variables stored herein are considered equal

	    void merge()
	    {
		int one_temp_var = -1;
		VarSet temp_vars = new VarSet();
		int one_value_var = -1;
		VarSet value_vars = new VarSet();
		int one_query_var = -1;
		VarSet query_vars = new VarSet();

		boolean string_comparison = true; // whether to compare everything using `.equals'

		for (int var : this.vars) {
		    final PQLVarInfo vi = context.varInfo(var);
		    final Type ty = vi.type;

		    if (!ty.equals(context.syms.stringType))
			string_comparison = false;

		    if (vi.isTempVar()) {
			one_temp_var = var;
			temp_vars.insert(var);
		    } else {
			if (vi.isQueryVar()/* && !vi.type_quantified */) { // <- comment fixes Container.singleton
			    one_query_var = var;
			    query_vars.insert(var);
			    if (one_value_var == -1)
				one_value_var = var; // only pick query vars as default value vars as last resort
			} else
			    one_value_var = var;
			value_vars.insert(var);
		    }
		}
		// query vars are a subset of value vars.
//System.err.println("Adding EQ:  value vars = " + value_vars + ", temp_vars = " + temp_vars + ", query_vars = " + query_vars);

		assert (one_temp_var != -1) || (one_value_var != -1) : "Equality between all query variables, and none of them bound anywhere?  Do we have to track variables that are existentially quantified differently here?";

		if (string_comparison) {
		    // generate slower code:  compare everything with EQ_String
//System.err.println("!! String comparison!!");
		    final int one_var;
		    final VarSet all_set = temp_vars;
		    temp_vars.addAll(value_vars);
		    if (one_temp_var > -1)
			one_var = one_temp_var;
		    else
			one_var = one_value_var;

		    all_set.remove(one_var);

		    for (int rhs : all_set)
			PQLFactory.EQ_String(one_var, rhs);

		    return;
		} else {
		    // not string_comparison
		    if (value_vars.size() > 1 && one_value_var == -1) {
			// All of our value vars are quantified variables:  we can neither back-substitute them nor read from them.
			// Thus, let's retain temp variable.
			one_value_var = one_temp_var;
		    }

		    // Part 1: eliminate temp vars
		    if (temp_vars.size() > 0) {
			int substitute = one_value_var == -1 ? one_temp_var : one_value_var; // substitute with real var iff possible
			temp_vars.remove(substitute);
//System.err.println("!! Backward subst: " + temp_vars + " <- " + Env.showVar(substitute));
			backwardSubstAndElimVariables(temp_vars, substitute);
		    }
		    // Part 2: intro equality tests as necessary
		    if (value_vars.size() > 1) {
//System.err.println("!! eq tests on value vars: " + value_vars);
			assert one_value_var != -1;
			value_vars.remove(one_value_var);
			// note that one_value_var will not normally be a query var, so we use it on the lhs in all of the below.

			int type = context.pqltype(context.varType(one_value_var));

			for (int rhs : value_vars) {
			    Join join;
			    switch (type) {
			    case TYPE_INT:	join = PQLFactory.EQ_Int(one_value_var, rhs); break;
			    case TYPE_LONG:	join = PQLFactory.EQ_Long(one_value_var, rhs); break;
			    case TYPE_DOUBLE:	join = PQLFactory.EQ_Double(one_value_var, rhs); break;
			    case TYPE_OBJECT:	join = PQLFactory.EQ_Object(one_value_var, rhs); break;
			    
			    default:
				throw new RuntimeException("Unexpected type in EQ: " + type);
			    }
			    joins.add(join);
			}
		    }
		}
	    }

	    void
	    add(int v)
	    {
		vars.insert(v);
	    }

	    void
	    absorb(MergeContext other)
	    {
		for (int v : other.vars) {
		    this.add(v);
		    unifications.put(v, this);
		}
	    }

	    private int mc_id = mc_idc++;

	    @Override
	    public String
	    toString()
	    {
		return mc_id + ":" + vars;
	    }
	}

	public
	ConjunctiveJoinAggregator()
	{
	    reset();
	}

	public void
	reset()
	{
	    joins = new LinkedList<Join>();
	    merge_contexts = new HashSet<MergeContext>();
	    unifications = new HashMap<Integer, MergeContext>();
	    current_result = NO_RESULT_VAR;
	}

	public List<Join> joins;
	public Set<MergeContext> merge_contexts;
	public Map<Integer, MergeContext> unifications;
	public int current_result; // result variable, if any; NO_RESULT_VAR if implicit boolean body

	private int c_id = c_idc++;

	@Override
	public String
	toString()
	{
	    return "#" + c_id + ":" + joins + ", " + merge_contexts + "=>" + Env.showVar(current_result);
	}

	void
	backwardSubstAndElimVariables(final VarSet temp_vars_to_elim, final int replacement_var)
	{
	    // final Symbol old_sym = this.context.varInfo(temp_var_to_elim).sym;
	    // if (old_sym != null)
	    //     this.variables.put(old_sym, replacement_var);
	    //context.elimVariable(temp_var_to_elim);
	    JoinVisitor v = new BackwardsSubstitutionVisitor() {
		    public int subst(int var) {
			if (temp_vars_to_elim.contains(var))
			    return replacement_var;
			else
			    return var;
		    }
		};
	    for (Join j : this.joins)
		j.accept(v);
	}

	protected Join
	conjunctiveBlock(ArrayList<Join> joins)
	{
	    if (joins.size() == 1)
		return joins.get(0);
	    else
		return PQLFactory.ConjunctiveBlock(joins);
	}


	/**
	 * Extract pair of joins join to represent everything stored in this aggregator
	 *
	 * Split into a part that binds the specified vars and into a part that does not.
	 */
	ForcedJoinPair
	force(VarSet separators)
	{

	    if (this.joins == null)
		throw new RuntimeException("Context already forced");

	    //System.err.println("[?" + this + "] forcing: " + this.merge_contexts.size());

	    for (MergeContext ctx : this.merge_contexts)
		ctx.merge();

	    ForcedJoinPair result = new ForcedJoinPair();
	    ArrayList<Join> binding_joins = new ArrayList<Join>();
	    ArrayList<Join> nonbinding_joins = new ArrayList<Join>();

	    for (Join j: this.joins) {
		if (PQLBinderChecker.isBinder(j, separators))
		    binding_joins.add(j);
		else
		    nonbinding_joins.add(j);
	    }

	    result.binding_joins = conjunctiveBlock(binding_joins);
	    result.nonbinding_joins = conjunctiveBlock(nonbinding_joins);

	    reset();
	    return result;
	}

	/**
	 * Extract a single join to represent everything stored in this aggregator
	 */
	public Join
	force()
	{
	    ForcedJoinPair joins = force(empty_var_set);
	    return joins.nonbinding_joins;
	}

	private void
	doAdd(Join j)
	{
	    if (j.isBlock() && j instanceof AbstractBlock.Conjunctive) {
		AbstractBlock block = (AbstractBlock) j;
		for (int i = 0; i < block.getComponentsNr(); i++)
		    doAdd(block.getComponent(i));
	    } else {
		this.joins.add(j);
	    }
	}

	/**
	 * Add a join and generate a QVar representing this join and a known arithmetic result variable
	 */
	public QVar
	addJoin(Join j, int result_var)
	{
	    doAdd(j);
	    this.current_result = result_var;
	    return new ContextQVar(this, result_var);
	}

	/**
	 * As addJoin(), but for a boolean query that binds a variable to its result
	 *
	 * If a boolean query that binds a variable is used as an `implicit boolean' (i.e., as a join without arithmetic result),
	 * we need to make sure that it only succeeds if it produces a `one'.  The ContextQVarInBooleanReduction() enforces this.
	 *
	 * @param j The join producing the boolean query
	 * @param result_var the variable produced by the boolean query
	 */
	public QVar
	addJoinBooleanReduction(Join j, int result_var)
	{
	    doAdd(j);
	    this.current_result = result_var;
	    return new ContextQVarInBooleanReduction(this, result_var, j);
	}

	public QVar
	addBooleanJoin(Join j)
	{
	    doAdd(j);
	    this.current_result = NO_RESULT_VAR;
	    return new ContextQVar(this);
	}

	public void
	addEquality(QVar v1, QVar v2)
	{
	    if (this.joins == null)
		throw new RuntimeException("Context already forced");
	    int var1 = Env.readVar(v1.asVariable(this));
	    int var2 = Env.readVar(v2.asVariable(this));

	    System.err.println("[##:" + this + "]  " +
	    		   Env.showVar(var1)
	    		   + " == " +
	    		   Env.showVar(var2)
	    		   );

	    MergeContext ctx1 = unifications.get(var1);
	    MergeContext ctx2 = unifications.get(var2);

	    if (ctx1 == null) {
		if (ctx2 == null) {
		    ctx2 = new MergeContext();
		    ctx2.add(var2);
		    unifications.put(var2, ctx2);
		    this.merge_contexts.add(ctx2);
		}
		ctx2.add(var1);
		unifications.put(var1, ctx2);
	    } else if (ctx2 == null) {
		ctx1.add(var2);
		unifications.put(var2, ctx1);
	    } else {
		ctx1.absorb(ctx2);
		this.merge_contexts.remove(ctx2);
	    }
	}

    }

    /**
     * A QVar represents both joins and arithmetic results.
     *
     * The subexpressions we support are either boolean or arithmetic in nature, and can be used in either context.
     * QVar provides an interface for these needs.
     */
    public abstract class QVar
    {
	/**
	 * Interpret QVar as `implicit boolean' (i.e., as Join).
	 */
	public abstract Join
	asJoin();

	/**
	 * Interpret QVar as arithmetic variable.
	 *
	 * The joins needed to compute the relevant variable are appended to `target'.
	 * Only one result is expected, usually.
	 *
	 * @return The variable representing this join
	 */
	public int
	asVariable(ConjunctiveJoinAggregator target)
	{
	    return asFunction(target, empty_var_set);
	}

	/**
	 * Interpret QVar as a function
	 *
	 * The joins needed to compute the relevant variable are appended to `target'.
	 * The QVar is interpreted as a variable that binds to all possible valuess that can be produced by `free_variable'.
	 * This is significant if the result is boolean:  in that case, all possible bindings for the `free_variable' entities are
	 * produced first (through a join) and the truth check is produced afterwards.
	 */
	public abstract int
	asFunction(ConjunctiveJoinAggregator target, VarSet free_variable);

	public final int
	asVar()
	{
	    return this.asVariable(current_aggregator);
	}
    }

    /**
     * qvar representing a join.  Succeeds iff true.
     */
    class ContextQVar extends QVar
    {
	ConjunctiveJoinAggregator source_context;
	Join result_join = null;
	int result_var = NO_RESULT_VAR;

	public
	ContextQVar(ConjunctiveJoinAggregator source_context)
	{
	    if (source_context == null)
		throw new NullPointerException();
	    this.source_context = source_context;
	}

	public
	ContextQVar(ConjunctiveJoinAggregator source_context, int var)
	{
	    if (source_context == null)
		throw new NullPointerException();
	    this.source_context = source_context;
	    this.result_var = var;
	}

	public Join
	asJoin()
	{
	    if (this.result_join == null)
		result_join = source_context.force();
	    return
		result_join;
	}

	public int
	asFunction(ConjunctiveJoinAggregator target, VarSet args)
	{
	    if (target == source_context
		&& this.result_var != NO_RESULT_VAR)
		// easiest case: continue appending
		return this.result_var;

	    ForcedJoinPair join_pair = this.source_context.force(args);

	    if (result_var == NO_RESULT_VAR) {
		result_var = freshTempVariableBoolean();
		join_pair.nonbinding_joins = PQLFactory.Bool(join_pair.nonbinding_joins, result_var);
	    }

	    Join join = PQLFactory.flattenBlock(PQLFactory.ConjunctiveBlock(join_pair.binding_joins, join_pair.nonbinding_joins));

	    target.addBooleanJoin(join);
	    return result_var;
	}
    }

    /**
     * See addJoinbooleanReduction and ContextQVar.
     */
    class ContextQVarInBooleanReduction extends ContextQVar
    {
	Join join;
	int var_to_subst_in_join;

	public ContextQVarInBooleanReduction(ConjunctiveJoinAggregator source_context, int var, Join join)
	{
	    super(source_context, var);
	    this.join = join;
	    this.var_to_subst_in_join = var;
	}

	public Join
	asJoin()
	{
	    final Join result = super.asJoin();
	    final int var_to_replace = this.var_to_subst_in_join;
	    final int constant_one = getConstantOne();

	    join.accept(new BackwardsSubstitutionVisitor() {
		    public int subst(int var)
		    {
			if (var == var_to_replace)
			    return constant_one;
			else
			    return var;
		    }
		});
	    return result;
	}
    }

    /**
     * QVar representing a concrete variable as result
     */
    class VariableQVar extends QVar
    {
	int var_id;

	public VariableQVar(int var)
	{
	    this.var_id = var;
	}

	public Join
	asJoin()
	{
	    return PQLFactory.EQ_Int(this.var_id, getConstantOne());
	}

	public int
	asFunction(ConjunctiveJoinAggregator target, VarSet args)
	{
	    return this.var_id;
	}
    }


    void
    doVisitRoot(JCTree tree)
    {
	this.result = this.doVisit(tree);
    }

    public PQLGenerator(Log log, Symtab symtab, Types types, LinkedList<JCExpression> constants)
    {
	this.log = log;
	this.context = new PQLContext(symtab, types);

	for (JCExpression constant : constants) {
	    if (constant.getKind() == Kind.IDENTIFIER)
		continue; // we handle these separately to avoid duplication

	    // FIXME: generalise this special-purpose hack to do CSE; right now it only works for `1'
	    if (constant instanceof JCLiteral) {
		JCLiteral lit = (JCLiteral) constant;

		if (lit.typetag == TypeTags.BOOLEAN
		    && lit.value.equals(1)) {
		    if (constant_one_var == NO_RESULT_VAR)
			setConstantOne(constant);
		    this.constant_subexprs.put(constant, getConstantOne());
		    continue;
		}
	    }
	    // no match?

	    final int var = this.context.nextVarForType(constant.type);
	    final PQLVarInfo vi = new PQLVarInfo(constant.type, var, constant);
	    this.allocateVariable(vi);
	    this.constant_subexprs.put(constant, var);
	}

    }

    boolean
    isConstant(JCExpression constant)
    {
	return this.constant_subexprs.containsKey(constant);
    }

    void
    addConstant(JCExpression constant)
    {
	final int var = this.context.nextVarForType(constant.type);
	final PQLVarInfo vi = new PQLVarInfo(constant.type, var, constant);
	this.allocateVariable(vi);
	this.constant_subexprs.put(constant, var);
    }

    void
    removeConstant(JCExpression constant)
    {
	final int var = this.constant_subexprs.get(constant);
	final PQLVarInfo vi = context.varInfo(var);
	context.elimVariable(var);
	this.constant_subexprs.remove(constant);
    }

    PQLVarInfo
    varInfo(int var)
    {
	return this.context.varInfo(var);
    }

    private int
    allocateVariable(PQLVarInfo vi)
    {
	this.context.allocateVariable(vi);
	if (vi.sym != null)
	    this.variables.put(vi.sym, vi.id);

	return vi.id;
    }

    int
    freshVariable(Type ty, Symbol sym)
    {
	final int var = this.context.nextVarForType(ty);
	final PQLVarInfo vi = new PQLVarInfo(ty, var, sym);
	return allocateVariable(vi);
    }

    // --------------------------------------------------------------------------------
    // support API

    int
    freshTempVariable(Type ty)
    {
	final int var = this.context.nextVarForType(ty);
	final PQLVarInfo vi = new PQLVarInfo(ty, var);
	return allocateVariable(vi);
    }

    int
    freshTempVariableInt()
    {
	return freshTempVariable(this.context.syms.intType);
    }

    int
    freshTempVariableBoolean()
    {
	return freshTempVariable(this.context.syms.booleanType);
    }

    public int
    getVar(Symbol sym)
    {
	if (this.variables.containsKey(sym))
	    return this.variables.get(sym);
	else
	    return freshVariable(sym.type, sym);
    }

    public void
    setResult(Join join, int result_var)
    {
	result = this.current_aggregator.addJoin(join, result_var);
    }

    public void
    setResultBooleanReduction(Join join, int result_var)
    {
	result = this.current_aggregator.addJoinBooleanReduction(join, result_var);
    }

    public void
    setResultVar(int result_var)
    {
	result = new VariableQVar(result_var);
    }

    public void
    setBooleanResult(Join join)
    {
	result = this.current_aggregator.addBooleanJoin(join);
    }

    QVar
    doVisit(JCTree tree)
    {
	if (this.constant_subexprs.containsKey(tree)) {
	    int var = this.constant_subexprs.get(tree);

	    if (tree.type.tag == TypeTags.BOOLEAN) {
		setBooleanResult(PQLFactory.EQ_Int(var, getConstantOne()));
	System.err.println(">> setting: var = " + Env.showVar(var)  + " = one as " + Env.showVar(getConstantOne()));
		return this.result;
	    } else
		return new VariableQVar(var);
	} else {
	    tree.accept(this);
	    return this.result;
	}
    }

    // --------------------------------------------------------------------------------

    @Override
    public void
    visitIdent(JCIdent that)
    {
	setResultVar(getVar(that.sym));
    }

    @Override
    public void
    visitAssign(JCAssign that)
    {
	this.current_aggregator.addEquality(this.doVisit(that.lhs), this.doVisit(that.rhs));
	this.result = new ContextQVar(this.current_aggregator);
    }

    public boolean
    isString(Type t)
    {
	t = t.elimQTyVars();
	return t.tsym != null
	    && t.tsym.equals(context.syms.stringType.tsym);
    }

    public void visitUnary(JCUnary that)
    {
	switch (that.getTag()) {
	case JCTree.POS:
	    return;
	case JCTree.NOT:
	    setBooleanResult(NEGATE(this.doVisit(that.arg).asJoin()));
	    return;
	}

	final int body_var = this.doVisit(that.arg).asVar();
	final int type = context.pqltype(that.arg.type);
	final int ret_var = this.freshTempVariable(that.type);
	Join join;

	switch (that.getTag()) {
	case JCTree.NEG:
	    switch (type) {
	    case TYPE_INT:	join = PQLFactory.NEG_Int(body_var, ret_var); break;
	    case TYPE_LONG:	join = PQLFactory.NEG_Long(body_var, ret_var); break;
	    case TYPE_DOUBLE:	join = PQLFactory.NEG_Double(body_var, ret_var); break;
	    default:
		throw new RuntimeException("Unexpected type in NEG: " + type);
	    }
	    setResult(join, ret_var);
	    return;

	case JCTree.COMPL:
	    switch (type) {
	    case TYPE_INT:	join = PQLFactory.BITINV_Int(body_var, ret_var); break;
	    case TYPE_LONG:	join = PQLFactory.BITINV_Long(body_var, ret_var); break;
	    default:
		throw new RuntimeException("Unexpected type in BITINV: " + type);
	    }
	    setResult(join, ret_var);
	    return;

	case JCTree.NOT:
	    //result = attr.syms.booleanType;
	    //break;
	default:
	    throw new RuntimeException("Unsupported unary expression: " + that);
	}
    }

    @Override
    public void
    visitBinary(JCBinary that)
    {
	switch (that.getTag()) {
	case JCTree.EQ: {
	    if (that.lhs.type.getKind() == TypeKind.BOOLEAN
		|| that.rhs.type.getKind() == TypeKind.BOOLEAN) {
		final Join lhs = this.doVisit(that.lhs).asJoin();
		final int rhs_var = this.doVisit(that.rhs).asVar();
		// System.err.println(">> Agg = " + this.current_aggregator);
		// System.err.println(">> var = " + Env.showVar(rhs_var));
		// System.err.println(">> lhs = " + lhs);
		setBooleanResult(PQLFactory.Bool(lhs, rhs_var));
	    } else {
System.err.println("EQ-adding: " + that.lhs + " = " + that.rhs);
		this.current_aggregator.addEquality(this.doVisit(that.lhs), this.doVisit(that.rhs));
		this.result = new ContextQVar(this.current_aggregator);
	    }
	    return;
	}
	case JCTree.AND:
	    // implicit constraint addition
	    this.doVisit(that.lhs);
	    this.doVisit(that.rhs);
	    clearResult();
	    return;
	case JCTree.IMPLIES: {
	    final Join lhs = this.doVisit(that.lhs).asJoin();
	    final Join rhs = this.doVisit(that.rhs).asJoin();
	    //setBooleanResult(PQLFactory.IfThenElse(lhs, rhs, PQLFactory.TRUE));
	    setBooleanResult(NEGATE(PQLFactory.ConjunctiveBlock(lhs, NEGATE(rhs))));
	    return;
	}
	case JCTree.OR:
	    setBooleanResult(PQLFactory.flattenBlock(PQLFactory.DisjunctiveBlock(this.doVisit(that.lhs).asJoin(),
										 this.doVisit(that.rhs).asJoin()
										 )));
	    return;
	default:
	}


	int lhs = this.doVisit(that.lhs).asVar();
	int rhs = this.doVisit(that.rhs).asVar();

	int arg_ty = context.pqltypeJoin(context.pqltype(that.lhs.type), context.pqltype(that.rhs.type));
	int ret_var = NO_RESULT_VAR;
	//int ret_var = this.freshTempVariable(that.type);

	Join join;

	switch (that.getTag()) {
	case JCTree.GT: {
	    int swap = lhs;
	    lhs = rhs;
	    rhs = swap;
	}
	case JCTree.LT:
	    switch (arg_ty) {
	    case TYPE_INT:	join = PQLFactory.LT_Int(lhs, rhs); break;
	    case TYPE_LONG:	join = PQLFactory.LT_Long(lhs, rhs); break;
	    case TYPE_DOUBLE:	join = PQLFactory.LT_Double(lhs, rhs); break;
	    default:
		throw new RuntimeException("Unexpected type in LT: " + arg_ty);
	    }
	    setBooleanResult(join);
	    return;

	case JCTree.GE: {
	    int swap = lhs;
	    lhs = rhs;
	    rhs = swap;
	}
	case JCTree.LE:
	    switch (arg_ty) {
	    case TYPE_INT:	join = PQLFactory.LTE_Int(lhs, rhs); break;
	    case TYPE_LONG:	join = PQLFactory.LTE_Long(lhs, rhs); break;
	    case TYPE_DOUBLE:	join = PQLFactory.LTE_Double(lhs, rhs); break;
	    default:
		throw new RuntimeException("Unexpected type in LT: " + arg_ty);
	    }
	    setBooleanResult(join);
	    return;

	case JCTree.NE:
	    if (isString(that.lhs.type) || isString(that.rhs.type))
		join = PQLFactory.NEQ_String(lhs, rhs);
	    else switch (arg_ty) {
	    case TYPE_INT:	join = PQLFactory.NEQ_Int(lhs, rhs); break;
	    case TYPE_LONG:	join = PQLFactory.NEQ_Long(lhs, rhs); break;
	    case TYPE_DOUBLE:	join = PQLFactory.NEQ_Double(lhs, rhs); break;
	    case TYPE_OBJECT:	join = PQLFactory.NEQ_Object(lhs, rhs); break;
	    default:
		throw new RuntimeException("Unexpected type in NE: " + arg_ty);
	    }
	    setBooleanResult(join);
	    return;

	case JCTree.MUL: {
	    int var = this.freshTempVariable(that.type);
	    switch (arg_ty) {
	    case TYPE_INT:	join = PQLFactory.MUL_Int(lhs, rhs, var); break;
	    case TYPE_LONG:	join = PQLFactory.MUL_Long(lhs, rhs, var); break;
	    case TYPE_DOUBLE:	join = PQLFactory.MUL_Double(lhs, rhs, var); break;
	    default:
		throw new RuntimeException("Unexpected type in MUL: " + arg_ty);
	    }
	    setResult(join, var);
	    return;
	}

	case JCTree.PLUS: {
	    int var = this.freshTempVariable(that.type);
	    switch (arg_ty) {
	    case TYPE_INT:	join = PQLFactory.ADD_Int(lhs, rhs, var); break;
	    case TYPE_LONG:	join = PQLFactory.ADD_Long(lhs, rhs, var); break;
	    case TYPE_DOUBLE:	join = PQLFactory.ADD_Double(lhs, rhs, var); break;
	    default:
		throw new RuntimeException("Unexpected type in ADD: " + arg_ty);
	    }
	    setResult(join, var);
	    return;
	}
	case JCTree.MINUS: {
	    int var = this.freshTempVariable(that.type);
	    switch (arg_ty) {
	    case TYPE_INT:	join = PQLFactory.SUB_Int(lhs, rhs, var); break;
	    case TYPE_LONG:	join = PQLFactory.SUB_Long(lhs, rhs, var); break;
	    case TYPE_DOUBLE:	join = PQLFactory.SUB_Double(lhs, rhs, var); break;
	    default:
		throw new RuntimeException("Unexpected type in SUB: " + arg_ty);
	    }
	    setResult(join, var);
	    return;
	}
	case JCTree.DIV: {
	    int var = this.freshTempVariable(that.type);
	    switch (arg_ty) {
	    case TYPE_INT:	join = PQLFactory.DIV_Int(lhs, rhs, var); break;
	    case TYPE_LONG:	join = PQLFactory.DIV_Long(lhs, rhs, var); break;
	    case TYPE_DOUBLE:	join = PQLFactory.DIV_Double(lhs, rhs, var); break;
	    default:
		throw new RuntimeException("Unexpected type in DIV: " + arg_ty);
	    }
	    setResult(join, var);
	    return;
	}
	case JCTree.MOD: {
	    int var = this.freshTempVariable(that.type);
	    switch (arg_ty) {
	    case TYPE_INT:	join = PQLFactory.MOD_Int(lhs, rhs, var); break;
	    case TYPE_LONG:	join = PQLFactory.MOD_Long(lhs, rhs, var); break;
	    default:
		throw new RuntimeException("Unexpected type in MOD: " + arg_ty);
	    }
	    setResult(join, var);
	    return;
	}

	case JCTree.BITOR: {
	    int var = this.freshTempVariable(that.type);
	    switch (arg_ty) {
	    case TYPE_INT:	join = PQLFactory.BITOR_Int(lhs, rhs, var); break;
	    case TYPE_LONG:	join = PQLFactory.BITOR_Long(lhs, rhs, var); break;
	    default:
		throw new RuntimeException("Unexpected type in BITOR: " + arg_ty);
	    }
	    setResult(join, var);
	    return;
	}

	case JCTree.BITXOR: {
	    int var = this.freshTempVariable(that.type);
	    switch (arg_ty) {
	    case TYPE_INT:	join = PQLFactory.BITXOR_Int(lhs, rhs, var); break;
	    case TYPE_LONG:	join = PQLFactory.BITXOR_Long(lhs, rhs, var); break;
	    default:
		throw new RuntimeException("Unexpected type in BITXOR: " + arg_ty);
	    }
	    setResult(join, var);
	    return;
	}

	case JCTree.BITAND: {
	    int var = this.freshTempVariable(that.type);
	    switch (arg_ty) {
	    case TYPE_INT:	join = PQLFactory.BITAND_Int(lhs, rhs, var); break;
	    case TYPE_LONG:	join = PQLFactory.BITAND_Long(lhs, rhs, var); break;
	    default:
		throw new RuntimeException("Unexpected type in BITAND: " + arg_ty);
	    }
	    setResult(join, var);
	    return;
	}

	case JCTree.SL: {
	    int var = this.freshTempVariable(that.type);
	    switch (arg_ty) {
	    case TYPE_INT:	join = PQLFactory.BITSHL_Int(lhs, rhs, var); break;
	    case TYPE_LONG:	join = PQLFactory.BITSHL_Long(lhs, rhs, var); break;
	    default:
		throw new RuntimeException("Unexpected type in SHL: " + arg_ty);
	    }
	    setResult(join, var);
	    return;
	}

	case JCTree.SR: {
	    int var = this.freshTempVariable(that.type);
	    switch (arg_ty) {
	    case TYPE_INT:	join = PQLFactory.BITSHR_Int(lhs, rhs, var); break;
	    case TYPE_LONG:	join = PQLFactory.BITSHR_Long(lhs, rhs, var); break;
	    default:
		throw new RuntimeException("Unexpected type in SHR: " + arg_ty);
	    }
	    setResult(join, var);
	    return;
	}

	case JCTree.USR: {
	    int var = this.freshTempVariable(that.type);
	    switch (arg_ty) {
	    case TYPE_INT:	join = PQLFactory.BITSSHR_Int(lhs, rhs, var); break;
	    case TYPE_LONG:	join = PQLFactory.BITSSHR_Long(lhs, rhs, var); break;
	    default:
		throw new RuntimeException("Unexpected type in SSHR: " + arg_ty);
	    }
	    setResult(join, var);
	    return;
	}


	case JCTree.TYPETEST:

	case JCTree.OR:
	    throw new RuntimeException("Unsupported (as of yet) binary operation: " + that);

	case JCTree.SL_ASG:
	case JCTree.SR_ASG:
	case JCTree.USR_ASG:
	case JCTree.PLUS_ASG:
	case JCTree.MINUS_ASG:
	case JCTree.MUL_ASG:
	case JCTree.DIV_ASG:
	case JCTree.MOD_ASG:
	case JCTree.BITOR_ASG:
	case JCTree.BITXOR_ASG:
	case JCTree.BITAND_ASG:
	    throw new RuntimeException("PQLERR: Assignment operators not allowed within PQL queries");

	default:
	    throw new RuntimeException("Unknown binary operator in PQL query: " + that.getTag());
	}
    }

    void
    visitSize(JCExpression container)
    {
	final int body_var = this.doVisit(container).asVar();
	final int result_var = freshTempVariable(context.syms.intType);
	final Join join;

	if (container.type.tag == TypeTags.ARRAY) {
	    final Type.ArrayType at = (Type.ArrayType) container.type;

	    switch (at.elemtype.tag) {
	    case TypeTags.INT:
		join = PQLFactory.INT_ARRAY_SIZE(body_var, result_var);
		break;
	    case TypeTags.SHORT:
		join = PQLFactory.SHORT_ARRAY_SIZE(body_var, result_var);
		break;
	    case TypeTags.LONG:
		join = PQLFactory.LONG_ARRAY_SIZE(body_var, result_var);
		break;
	    case TypeTags.CHAR:
		join = PQLFactory.CHAR_ARRAY_SIZE(body_var, result_var);
		break;
	    case TypeTags.BOOLEAN:
		join = PQLFactory.BOOLEAN_ARRAY_SIZE(body_var, result_var);
		break;
	    case TypeTags.BYTE:
		join = PQLFactory.BYTE_ARRAY_SIZE(body_var, result_var);
		break;
	    case TypeTags.FLOAT:
		join = PQLFactory.FLOAT_ARRAY_SIZE(body_var, result_var);
		break;
	    case TypeTags.DOUBLE:
		join = PQLFactory.DOUBLE_ARRAY_SIZE(body_var, result_var);
		break;
	    default:
		join = PQLFactory.OBJECT_ARRAY_SIZE(body_var, result_var);
	    }
	} else if (container.type.tag == TypeTags.CLASS) {
	    if (this.context.types.isSubtypeUnchecked(container.type, context.types.genericCollectionType))
		join = PQLFactory.SET_SIZE(body_var, result_var);
	    else if (this.context.types.isSubtypeUnchecked(container.type, context.types.genericMapType))
		join = PQLFactory.MAP_SIZE(body_var, result_var);
	    else
		join = PQLFactory.POLY_SIZE(body_var, result_var);
	} else {
	    join = PQLFactory.POLY_SIZE(body_var, result_var);
	}

	setResult(join, result_var);
    }

    @Override
    public void
    visitSelect(JCFieldAccess that)
    {
	if (that.name.toString().equals("length")) {
	    visitSize(that.selected);
	    return;
	}

	final int source_var = this.doVisit(that.selected).asVar();
	final int result_var = freshTempVariable(that.type);
	setResult(PQLFactory.FIELD(that, source_var, result_var), result_var);
    }

    @Override
    public void
    visitApply(final JCMethodInvocation that)
    {
	if (true /*that.meth.getTag() == JCTree.SELECT*/) {
	    class Generator extends PQLMagicSubexpr
	    {
		public void
		handleContains(JCExpression set, JCExpression element)
		{
		    final int set_var = doVisit(set).asVar();
		    final int element_var = doVisit(element).asVar();
		    setBooleanResult(PQLFactory.CONTAINS(set_var, element_var));
		}

		public void
		handleMapGet(JCExpression map, JCExpression key)
		{
		    handleIndexedMagicMap(that, map, key);
		}

		public void
		handleSize(JCExpression c)
		{
		    visitSize(c);
		}

		@Override
		public void
		handleRangeContains(JCExpression first, JCExpression last, JCExpression body)
		{
		    // For `range(x, y).contains(a, b)' it can happen that `range(x, y)' is a
		    // constant.  For purposes of this we don't want that, though, so we un-
		    // constantify `range(x, y)' here, turning `x' and `y' into replacement
		    // constants.
		    final JCExpression range = ((JCFieldAccess)that.meth).selected;
		    if (isConstant(range)) {
			removeConstant(range);
			addConstant(first);
			addConstant(last);
		    }

		    // finally construct the optimised query:
		    setBooleanResult(PQLFactory.INT_RANGE_CONTAINS(doVisit(first).asVar(),
								   doVisit(last).asVar(),
								   doVisit(body).asVar()));
		}

		@Override
		public void
		handleRange(JCExpression first, JCExpression last)
		{
		    final int loop_var = freshTempVariable(context.syms.intType);
		    final int result_var = freshTempVariable(that.type);
		    setResult(PQLFactory.Reduction(PQLFactory.Reductors.SET(loop_var, result_var),
						   PQLFactory.INT_RANGE_CONTAINS(doVisit(first).asVar(),
										 doVisit(last).asVar(),
										 loop_var)),
			      result_var);
		}
	    }

	    if ((new Generator()).process(that))
		return;

	    visitTree(that); // We only detect a handful of special cases
	} else
	    visitTree(that);
    }

    @Override
    public void
    visitParens(JCParens that)
    {
	that.expr.accept(this);
    }


    private final
    void
    handleIndexedMagicMap(JCExpression that,
			  JCExpression map,
			  JCExpression key)
    {
	// A magic map can be either a subtype of java.util.Map, an array type, or a union of both.
	map.type = map.type.elimQTyVars();
	key.type = key.type.elimQTyVars();
	that.type = that.type.elimQTyVars();


	if (map.type.tag == TypeTags.ARRAY) {
	    final int array_var = this.doVisit(map).asVar();
	    final int index_var = this.doVisit(key).asVar();

	    final Type array_type = this.context.varType(array_var);

	    // typechecker will have issued error already
	    if (array_type.getKind() != TypeKind.ARRAY)
		return;

	    final Type element_type = ((Type.ArrayType) array_type).getComponentType();
	    final int result_var = freshTempVariable(element_type);
	    final Join join;

	    switch (element_type.getKind()) {
	    case BYTE:		join = PQLFactory.ARRAY_LOOKUP_Byte(array_var, index_var, result_var);		break;
	    case CHAR:		join = PQLFactory.ARRAY_LOOKUP_Char(array_var, index_var, result_var);		break;
	    case SHORT:		join = PQLFactory.ARRAY_LOOKUP_Short(array_var, index_var, result_var);		break;
	    case INT:		join = PQLFactory.ARRAY_LOOKUP_Int(array_var, index_var, result_var);		break;
	    case LONG:		join = PQLFactory.ARRAY_LOOKUP_Long(array_var, index_var, result_var);		break;
	    case FLOAT:		join = PQLFactory.ARRAY_LOOKUP_Float(array_var, index_var, result_var);		break;
	    case DOUBLE:	join = PQLFactory.ARRAY_LOOKUP_Double(array_var, index_var, result_var);	break;
	    case BOOLEAN:	join = PQLFactory.ARRAY_LOOKUP_Boolean(array_var, index_var, result_var);	break;
	    default:		join = PQLFactory.ARRAY_LOOKUP_Object(array_var, index_var, result_var);	break;
	    }

	    setResult(join, result_var);
	} else if (map.type.tag == TypeTags.CLASS) {
	    // must be regular Map type, if it type-checked
	    final int map_var = doVisit(map).asVar();
	    final int key_var = doVisit(key).asVar();
	    final int result_var = freshTempVariable(that.type);
	    setResult(PQLFactory.LOOKUP(map_var, key_var, result_var), result_var);
	} else {
	    assert map.type.tag == TypeTags.MAGICMAP;
	    final int map_var = doVisit(map).asVar();
	    final int key_var = doVisit(key).asVar();
	    final int result_var = freshTempVariable(that.type);
	    setResult(PQLFactory.POLY_LOOKUP(map_var, key_var, result_var), result_var);
	}
    }


    @Override
    public void
    visitIndexed(JCArrayAccess that)
    {
	handleIndexedMagicMap(that, that.indexed, that.index);
    }

    public int
    quantifyVar(Symbol sym, Type ty)
    {
	int var = getVar(sym);
	Join join;

	switch (ty.getKind()) {
	case BYTE:		join = PQLFactory.BYTE(var);		break;
	case CHAR:		join = PQLFactory.CHAR(var);		break;
	case SHORT:		join = PQLFactory.SHORT(var);		break;
	case INT:		join = PQLFactory.INT(var);		break;
	case LONG:		join = PQLFactory.LONG(var);		break;
	case BOOLEAN:		join = PQLFactory.BOOLEAN(var);		break;
	case FLOAT:
	case DOUBLE:
	default:		join = PQLFactory.JAVA_TYPE(ty, var);	break;
	}
	setBooleanResult(join);
	return var;
    }

    Join
    addImplicitQuantification(PQLExpr that, VarSet implicitly_quantified_vars, Join src, Join join)
    {
	if (!implicitly_quantified_vars.isEmpty()) {
	    // infer range
	    System.err.println("+++ Range inference for: " + implicitly_quantified_vars);
	    System.err.println("src>\t" + src);
	    System.err.println("pre>\t" + join);
	    final Join range_inference = PQLRangeInference.inferRange(src, implicitly_quantified_vars);
	    System.err.println("rng>\t" + range_inference);

	    VarSet defined_vars = new VarSet();
	    for (int v : Optimizer.bindingDependencyMap(range_inference).keySet())
		defined_vars.insert(v);

	    for (JCVariableDecl decl : that.decls)
		if (decl.vartype == null) {
		    // get var
		    final int var = getVar(decl.sym);
		    System.err.println("Checking " + var + "(" + decl.sym + ") <- " + defined_vars);
		    if (!defined_vars.contains(var))
			this.error(that.pos(), "not.inferred.range", decl.sym);
		}

	    final Join extended_body = Optimizer.cse(PQLFactory.ConjunctiveBlock(range_inference, join));
	    System.err.println("cse>\t" + extended_body);
	    return extended_body;
	} else
	    return join;
    }

    public Join // elim double negatoin
    NEGATE(Join j)
    {
	if (j instanceof edu.umass.pql.il.Not)
	    return ((ControlStructure) j).getComponent(0);
	else
	    return PQLFactory.Not(j);
    }

    public void
    visitPQLExpr(PQLExpr that)
    {
	// REFACTOR ME

	final int result_var;

	final ConjunctiveJoinAggregator old_aggregator = this.current_aggregator;

	// switch (that.query_kind) {
	// case EXISTENTIAL:
	// case UNIVERSAL:
	    this.current_aggregator = new ConjunctiveJoinAggregator();
	//     break;
	// default:
	//     break;
	// }

	VarSet quantified_range_inferred_vars = new VarSet();
	for (JCVariableDecl decl : that.decls)
	    if (decl.vartype != null) {
		context.varInfo(getVar(decl.sym)).type_quantified = true;
		quantifyVar(decl.sym, decl.type);
	    } else
		quantified_range_inferred_vars.insert(getVar(decl.sym));

	// ========== Recurse ========================================
	final QVar inner_qvar = this.doVisit(that.expr);
	final int inner_var;
	int inner_var2 = -1;
	int default_var = -1;
	boolean boolean_query = false; // whether to mark this query as `may be turned into a boolean by force-joining the result var with 1'

	switch (that.query_kind) {
	case EXISTENTIAL:
	case UNIVERSAL:
	    result_var = freshTempVariableBoolean();
//	    inner_var = inner_qvar.asFunction(this.current_aggregator, quantified_range_inferred_vars);
	    boolean_query = true;
	    inner_var = -1; // unused
	    break;
	case DEFAULT_MAP:
	    default_var = this.doVisit(that.default_expr).asVar();
	case MAP:
	    inner_var2 = getVar(that.decls.tail.head.sym);
	    inner_var = getVar(that.decls.head.sym);
	    result_var = freshTempVariable(this.context.syms.mapType);
	    break;
	case SET:
	    result_var = freshTempVariable(this.context.syms.setType);
	    inner_var = getVar(that.decls.head.sym);
	    break;
	case REDUCTION:
	    result_var = freshTempVariable(that.type);
	    inner_var = getVar(that.decls.head.sym);
	    break;
	default:	throw new RuntimeException("Unsupported query kind: " + that.query_kind);
	};

	// --------------------------------------------------------------------------------
	// Compute the body of the query: the query computation
	final Join query_body;
	    
	// switch (that.query_kind) {
	// case EXISTENTIAL: {
	    query_body = this.current_aggregator.force();
	//     //	    query = PQLFactory.Reduction(PQLFactory.Reductors.EXISTS(inner_var, result_var), body);
	//     //pre_query = query_body;
	//     break;
	// }
	// case UNIVERSAL: {
	//     query_body = this.current_aggregator.force();
	//     //	    query = PQLFactory.Reduction(PQLFactory.Reductors.FORALL(inner_var, result_var), body);
	//     //pre_query = PQLFactory.Not(PQLFactory.Not(query_body));
	//     break;
	// }
	// case REDUCTION:
	// case DEFAULT_MAP:
	// case MAP:
	// case SET:
	//     query_body = inner_qvar.asJoin();
	//     //pre_query = PQLFactory.Reduction(PQLFactory.Reductors.SET(inner_var, result_var), query_body);
	//     break;
	// default:
	//     throw new RuntimeException("Unsupported query kind: " + that.query_kind);
	// }

	// --------------------------------------------------------------------------------
	// And now construct the query over the full_query_body:
	final Join proto_query;

	System.err.println("[DOM-VARS] " + quantified_range_inferred_vars + " for " + that);

	switch (that.query_kind) {
	case EXISTENTIAL:
	    proto_query = query_body; // no need for implicit quantification
	    break;
	case UNIVERSAL:
	    proto_query = NEGATE(addImplicitQuantification(that, quantified_range_inferred_vars, query_body, NEGATE(query_body)));
	    break;
	case SET:
	    proto_query = PQLFactory.Reduction(PQLFactory.Reductors.SET(inner_var, result_var), addImplicitQuantification(that, quantified_range_inferred_vars, query_body, query_body));
	    break;
	case MAP:
	    assert inner_var2 != -1;
	    proto_query = PQLFactory.Reduction(PQLFactory.Reductors.MAP(inner_var, inner_var2, result_var), addImplicitQuantification(that, quantified_range_inferred_vars, query_body, query_body));
	    break;
	case DEFAULT_MAP:
	    assert inner_var2 != -1;
	    assert default_var != -1;
	    proto_query = PQLFactory.Reduction(PQLFactory.Reductors.DEFAULT_MAP(default_var, inner_var, inner_var2, result_var),
					 addImplicitQuantification(that, quantified_range_inferred_vars, query_body, query_body));
	    break;
	case REDUCTION:
	    proto_query = PQLFactory.Reduction(PQLFactory.Reductors.METHOD_ADAPTER(that.resolved_reductor, inner_var, result_var),
					 query_body);
	    break;
	default:
	    throw new RuntimeException("Unsupported query kind: " + that.query_kind);
	}

	// the following pass has been moved to take place after type check elimination:
	// // --------------------------------------------------------------------------------
	// // Fold inner reductions into newly generated map reductions, if applicable (e.g., "red(MAP(a,b)) : foo; red(SET(x)): bar;" becomes "red(MAP(a,SET(x)): foo; bar"
	final Join query;
	// switch (that.query_kind) {
	// case DEFAULT_MAP:
	// case MAP:
	//     query = Optimizer.tryNestReduction((Reduction) proto_query);
	//     break;
	// default:
	//     query = proto_query;
	//     break;
	// }
	query = proto_query;
	// --------------------------------------------------------------------------------
	// clean up and store result

	this.current_aggregator = old_aggregator;

	if (boolean_query)
	    //	    setResultBooleanReduction(query, result_var);
	    setBooleanResult(query);
	else
	    setResult(query, result_var);
    }

    @Override
    public void
    visitTypeTest(JCInstanceOf that)
    {
	setBooleanResult(PQLFactory.JAVA_TYPE(that.clazz.type, doVisit(that.expr).asVar()));
    }

    @Override
    public void
    visitConditional(JCConditional that)
    {
	final Type type = that.truepart.type.elimQTyVars();
	that.truepart.type = type;
	that.falsepart.type = type;
	that.type = type;
	final int result_var = freshTempVariable(type);

	ConjunctiveJoinAggregator old_aggregator = this.current_aggregator;

	this.current_aggregator = new ConjunctiveJoinAggregator();
	final QVar true_result = this.doVisit(that.truepart);
	final ConjunctiveJoinAggregator true_aggregator = this.current_aggregator;

	this.current_aggregator = new ConjunctiveJoinAggregator();
	final QVar false_result = this.doVisit(that.falsepart);
	final ConjunctiveJoinAggregator false_aggregator = this.current_aggregator;

	this.current_aggregator = new ConjunctiveJoinAggregator();
	final QVar cond_result = this.doVisit(that.cond);
	final ConjunctiveJoinAggregator cond_aggregator = this.current_aggregator;


	if (type.tag == TypeTags.BOOLEAN
	    || this.context.types.unboxedType(type).tag == TypeTags.BOOLEAN) {
	    final Join true_join = true_result.asJoin();
	    final Join false_join = false_result.asJoin();
	    final Join cond_join = cond_result.asJoin();

	    // System.err.println("> Cond > " + cond_join);
	    // System.err.println("> False> " + false_join);
	    // System.err.println("> True > " + true_join);

	    this.current_aggregator = old_aggregator;

	    final Join result = PQLFactory.IfThenElse(cond_join,
						      true_join,
						      false_join);

	    // System.err.println("Result>> " + result + "\n=> " + Env.showVar(result_var));

	    setResult(result, result_var);
	} else {
	    final Join cond_join = cond_result.asJoin();
	    final QVar result_qvar = new VariableQVar(result_var);

	    this.current_aggregator = new ConjunctiveJoinAggregator();
	    setBooleanResult(cond_join);
	    final int true_var = true_result.asVar();
	    final Join true_join = this.current_aggregator.force();
	    //this.current_aggregator.addEquality(result_qvar, true_result);

	    final int final_result_var = true_var;

	    this.current_aggregator = new ConjunctiveJoinAggregator();
	    setBooleanResult(PQLFactory.Not(cond_join));
	    final int false_var = false_result.asVar();
	    setResult(PQLFactory.EQ_Int(false_var, final_result_var), true_var);
	    final Join false_join = this.current_aggregator.force();
	    //this.current_aggregator.addEquality(result_qvar, false_result);

	    this.current_aggregator = old_aggregator;
	    final Join result = PQLFactory.DisjunctiveBlock(true_join,
							    false_join);
	    //this.current_aggregator.addEquality(true_result, false_result);

	    // System.err.println("> Cond > " + cond_join);
	    // System.err.println("> False> " + false_join);
	    // System.err.println("> True > " + true_join);
	    // System.err.println("results = {" + Env.showVar(final_result_var) + "}");

	    setResult(result, final_result_var);
	}
    }


    @Override
    public void
    visitTree(JCTree other)
    {
	throw new RuntimeException("PQLGenerator: don't know how to handle: " + other.getClass() + "(" + other + ")");
    }
}
