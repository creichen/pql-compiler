/* Copyright (C) 2011, Christoph Reichenbach
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */
package com.sun.tools.javac.tree.pql;

import java.util.*;

import com.sun.tools.javac.tree.*;
import com.sun.tools.javac.tree.JCTree.*;
import com.sun.tools.javac.code.Type;
import com.sun.tools.javac.code.TypeTags;
import com.sun.tools.javac.code.Symtab;
import com.sun.tools.javac.code.Types;
import com.sun.source.tree.Tree.Kind;

import edu.umass.pql.*;
import com.sun.tools.javac.code.Symbol;
import javax.lang.model.type.TypeKind;

/**
 * Represents the context of a PQL translation: the set of variables allocated,
 * the type context of the enclosing JCTree, and the set of variables passed into
 * the query from the outside.
 */
public final class PQLContext
{
    final static int MAX_TYPE = VarConstants.MAX_TYPE;
    final static int MIN_TYPE = VarConstants.MIN_TYPE;
    final static int TYPE_INT = VarConstants.TYPE_INT;
    final static int TYPE_LONG = VarConstants.TYPE_LONG;
    final static int TYPE_DOUBLE = VarConstants.TYPE_DOUBLE;
    final static int TYPE_OBJECT = VarConstants.TYPE_OBJECT;
    public Symtab syms;
    Types types;
    VarSet input_var_set = null;

    PQLVarTable[] variable_table; // may contain null

    VarSet
    inputVarSet()
    {
	if (input_var_set == null) {
	    //this.syms = null; // No longer needed
	    //this.types = null; // No longer needed
	    this.input_var_set = new VarSet();

	    final Iterator<PQLVarInfo> context_variables = this.contextVariableIterator();
	    while (context_variables.hasNext()) {
		PQLVarInfo vi = context_variables.next();
		if (vi != null)
		    this.input_var_set.insert(vi.id);
	    }
	    final Iterator<PQLVarInfo> context_exprs = this.contextExpressionIterator();
	    while (context_exprs.hasNext()) {
		PQLVarInfo vi = context_exprs.next();
		if (vi != null)
		    this.input_var_set.insert(vi.id);
	    }
	}
	return this.input_var_set;
    }

    private abstract class VarIterator implements Iterator<PQLVarInfo>
    {
	int table_offset = MIN_TYPE - 1;
	Iterator<PQLVarInfo> ali;
	PQLVarInfo next = find_next();

	protected abstract boolean matchesFilter(PQLVarInfo vi);

	private PQLVarInfo
        find_next()
	{
	    do {
		if (ali == null) {
		    if (++table_offset > MAX_TYPE)
			return null;
		    ali = variable_table[table_offset].table.iterator();
		}

		while (ali.hasNext()) {
		    final PQLVarInfo candidate = ali.next();
		    if (candidate != null && matchesFilter(candidate))
			return candidate;
		}

		ali = null;
	    } while (true);
	}

	@Override
	public boolean
	hasNext()
	{
	    return this.next != null;
	}

	@Override
	public PQLVarInfo
	next()
	{
	    final PQLVarInfo my_result = this.next;
	    this.next = find_next();
	    return my_result;
	}

	@Override
	public void
	remove()
	{
	    throw new UnsupportedOperationException();
	}
    }

    /**
     * Returns all constant Java expressions from the context that are not Java variables
     */
    public Iterator<PQLVarInfo>
    contextExpressionIterator()
    {
	return new VarIterator() {
	    protected boolean
		matchesFilter(PQLVarInfo candidate)
	    {
		return candidate.isExpr();
	    }
	};
    }


    /**
     * Returns all Java variables from the context that are not pql query variables
     */
    public Iterator<PQLVarInfo>
    contextVariableIterator()
    {
	return new VarIterator() {
	    protected boolean
	    matchesFilter(PQLVarInfo candidate)
	    {
		return candidate.isContextVar();
	    }
	};
    }

    public
    PQLContext(Symtab symtab, Types types)
    {
	this.syms = symtab;
	this.types = types;
	variable_table = new PQLVarTable[MAX_TYPE + 1];
	for (int i = MIN_TYPE; i <= MAX_TYPE; i++)
	    variable_table[i] = new PQLVarTable(i);
    }

    public int
    nextVarForTypeTag(int type_tag)
    {
	final Type ty = syms.typeOfTag[type_tag];
	return nextVarForType(ty);
    }

    int
    nextVarForType(Type ty)
    {
	int pql_ty = pqltype(ty);
	int index = variable_table[pql_ty].size();
	int var = Env.encodeReadVar(pql_ty, index);

	return var;
    }

    void
    allocateVariable(PQLVarInfo vi)
    {
	variable_table[Env.varType(vi.id)].add(vi);
    }

    int
    pqltype(Type ty)
    {
	return pqltypeStatic(this.types, ty);
    }

    static int
    pqltypeStatic(Types types, Type ty)
    {
	if (!ty.isPrimitive()) {
	    Type alt_type = types.unboxedType(ty);
	    if (alt_type != Type.noType)
		ty = alt_type;
	}

	if (ty.tag == TypeTags.MAGICMAP
	    || ty.tag == TypeTags.MAGICSIZE)
	    return TYPE_OBJECT;

	switch (ty.getKind()) {
	case BOOLEAN:
	case BYTE:
	case CHAR:
	case SHORT:
	case INT:
	    return TYPE_INT;
	case LONG:
	    return TYPE_LONG;
	case FLOAT:
	case DOUBLE:
	    return TYPE_DOUBLE;
	case OTHER:
	    throw new RuntimeException("PQLERR: failed type inference: " + ty);
	default:
	    return TYPE_OBJECT;
	}
    }

    static int
    pqltypeJoin(int lty, int rty)
    {
	if (lty == TYPE_INT && rty == TYPE_INT)
	    return TYPE_INT;

	if (lty == TYPE_OBJECT || rty == TYPE_OBJECT)
	    return TYPE_OBJECT;

	if (lty == TYPE_DOUBLE || rty == TYPE_DOUBLE)
	    return TYPE_DOUBLE;

	return TYPE_LONG;
    }

    public PQLVarInfo
    varInfo(int var)
    {
	return this.variable_table[Env.varType(var)].get(Env.varIndex(var));
    }

    void
    elimVariable(int var)
    {
	int table = Env.varType(var);
	int index = Env.varIndex(var);

	varInfo(var).id = PQLVarInfo.DISABLED;


	if (index + 1 == this.variable_table[table].size())
	    this.variable_table[table].removeLast();
	else
	    this.variable_table[table].set(index, null);
    }

    Type
    varType(int var)
    {
	return this.varInfo(var).type;
    }
}
