/* Copyright (C) 2011, Christoph Reichenbach
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */
package com.sun.tools.javac.tree.pql;

import edu.umass.pql.*;
import edu.umass.pql.il.*;
import edu.umass.pql.il.meta.*;

public class JoinClassifier extends JoinVisitor
{
    enum JoinClass {
	PRIMITIVE,
	CONJUNCTION,
	DISJUNCTION,
	REDUCTION,
	BOOLEAN_CONTROL, // one of Bool, Not, NoFail
	META // one of SELECT_PATH, SCHEDULE (maybe distinguish those in the future if we should use them explicitly?)
    };

    public static JoinClass
    classify(Join j)
    {
	JoinClassifier jc = new JoinClassifier();
	j.accept(jc);
	return jc.result;
    }

    // ----------------------------------------

    JoinClass result = null;

    @Override
    public void
    visitDefault(Join it)
    {
	result = JoinClass.PRIMITIVE;
    }

    @Override
    public void
    visitControlStructure(ControlStructure sp)
    {
	throw new RuntimeException("Unexpected control structure");
    }

    @Override
    public void
    visitReduction(Reduction red)
    {
	result = JoinClass.REDUCTION;
    }

    @Override
    public void
    visitConjunctiveBlock(AbstractBlock.Conjunctive b)
    {
	result = JoinClass.CONJUNCTION;
    }

    @Override
    public void
    visitDisjunctiveBlock(AbstractBlock.Disjunctive b)
    {
	result = JoinClass.DISJUNCTION;
    }


    @Override
    public void
    visitNot(Not n)
    {
	result = JoinClass.BOOLEAN_CONTROL;
    }

    @Override
    public void
    visitNoFail(NoFail b)
    {
	result = JoinClass.BOOLEAN_CONTROL;
    }

    @Override
    public void
    visitSelectPath(SelectPath sp)
    {
	result = JoinClass.META;
    }

    @Override
    public void
    visitSchedule(Schedule sp)
    {
	result = JoinClass.META;
    }
}