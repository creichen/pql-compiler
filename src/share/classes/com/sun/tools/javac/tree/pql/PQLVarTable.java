/* Copyright (C) 2011, Christoph Reichenbach
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */
package com.sun.tools.javac.tree.pql;

import java.util.*;

import com.sun.tools.javac.tree.*;
import com.sun.tools.javac.tree.JCTree.*;
import com.sun.tools.javac.code.Type;
import com.sun.tools.javac.code.TypeTags;
import com.sun.tools.javac.code.Symtab;
import com.sun.source.tree.Tree.Kind;

import edu.umass.pql.*;
import com.sun.tools.javac.code.Symbol;
import javax.lang.model.type.TypeKind;

/**
 * Table of variables for the same PQL type.
 *
 * Each PQLVarTable represents one PQL type and collects all variables of that type, identified by their index.
 */
public final class PQLVarTable implements VarConstants
{
    int type; // PQL type represented herein
    public ArrayList<PQLVarInfo> table = new ArrayList<PQLVarInfo>(); // may contain null for eliminated variables

    public PQLVarTable(int type)
    {
	this.type = type;
    }

    public Object
    defaultValue()
    {
	switch (type) {
	case TYPE_INT:	return 0;
	case TYPE_LONG:	return 0l;
	case TYPE_DOUBLE:	return 0.0;
	case TYPE_OBJECT:	return null;
	default: throw new RuntimeException("Unexpected PQLVarTable type " + type);
	}
    }

    public Class<?>
    getArrayType()
    {
	switch (type) {
	case TYPE_INT:	return int[].class;
	case TYPE_LONG:	return long[].class;
	case TYPE_DOUBLE:	return double[].class;
	case TYPE_OBJECT:	return Object[].class;
	default: throw new RuntimeException("Unexpected PQLVarTable type " + type);
	}
    }

    public Class<?>
    getType()
    {
	return getArrayType().getComponentType();
    }

    public void
    set(int index, PQLVarInfo vi)
    {
	this.table.set(index, vi);
    }

    public void
    add(PQLVarInfo vi)
    {
	assert vi.id > 0;
	final int offset = Env.varIndex(vi.id);
	if (offset == this.table.size())
	    this.table.add(vi);
	else if (offset < this.table.size())
	    this.table.set(Env.varIndex(vi.id), vi);
	else
	    throw new RuntimeException("Unexpected write-to offset " + offset + " for table of size " + this.table.size());
    }

    public void
    removeLast()
    {
	this.table.remove(this.table.size() - 1);
    }

    public int
    size()
    {
	return this.table.size();
    }

    public PQLVarInfo
    get(int index)
    {
	return this.table.get(index);
    }

    public boolean
    isEmpty()
    {
	return this.table.isEmpty();
    }
}
