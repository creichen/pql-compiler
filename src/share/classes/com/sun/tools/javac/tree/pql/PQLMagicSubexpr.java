/* Copyright (C) 2011, Christoph Reichenbach
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */
package com.sun.tools.javac.tree.pql;

import java.util.*;

import com.sun.tools.javac.tree.JCTree.*;
import com.sun.tools.javac.tree.*;
import com.sun.tools.javac.util.Name;
import com.sun.tools.javac.code.Type;
import com.sun.tools.javac.code.TypeTags;
import com.sun.tools.javac.code.Symtab;
import com.sun.source.tree.Tree.Kind;

import edu.umass.pql.*;
import com.sun.tools.javac.code.Symbol;
import javax.lang.model.type.TypeKind;

/**
 * Detect and identify `magic' subexpressions
 */
public abstract class PQLMagicSubexpr
{
    public static boolean
    isMagic(JCTree tree)
    {
	return (new PQLMagicSubexpr() {
		@Override public void handleContains(JCExpression set, JCExpression element)	{}
		@Override public void handleMapGet(JCExpression map, JCExpression key)		{}
		@Override public void handleRange(JCExpression first, JCExpression last)	{}
		@Override public void handleRangeContains(JCExpression first, JCExpression last, JCExpression body) {}
		@Override public void handleSize(JCExpression container) {}
	    }).process(tree);
    }


    public final boolean
    process(JCTree tree)
    {
	// should be refactored if we add more `magics'.

	if (tree instanceof JCFieldAccess) {
	    final JCFieldAccess fa = (JCFieldAccess) tree;
	    if (fa.name.toString().equals("length")) {
		handleSize(fa.selected);
		return true;
	    }

	    return false;
	}

	if (!(tree instanceof JCMethodInvocation))
	    return false;

	final JCMethodInvocation app = (JCMethodInvocation) tree;

	if (app.meth instanceof JCFieldAccess) {
	    final JCFieldAccess method = (JCFieldAccess) app.meth;

	    final JCExpression container = method.selected;

	    final int args_nr = app.args.size();
	    final String method_name = method.name.toString();

	    if (method_name.equals("contains") && args_nr == 1) {
		class RangeContains extends PQLMagicSubexpr
		{
		    public JCExpression first = null, last = null;

		    @Override public void handleContains(JCExpression set, JCExpression element)	{}
		    @Override public void handleMapGet(JCExpression map, JCExpression key)		{}
		    @Override public void handleRangeContains(JCExpression first, JCExpression last, JCExpression body) {}
		    @Override public void handleSize(JCExpression container) {}
		    @Override public void
		    handleRange(JCExpression first, JCExpression last)
		    {
			this.first = first;
			this.last = last;
		    }
		};
		RangeContains rc = new RangeContains();
		rc.process(container);

		if (rc.first != null)
		    handleRangeContains(rc.first, rc.last, app.args.head);
		else
		    handleContains(container, app.args.head);
	    } else if (method_name.equals("get") && args_nr == 1)
		handleMapGet(container, app.args.head);
	    else if (method_name.equals("size") && args_nr == 0)
		handleSize(container);
	    else
		return false;

	    return true;
	} else if (app.meth instanceof JCIdent) {
	    final JCIdent ident = (JCIdent) app.meth;
	    final String name = ident.name.toString();

	    // `range'?
	    if (app.args.size() != 2)
		return false;

	    if (name.equals("range"))
		handleRange(app.args.head, app.args.tail.head);
	    else
		return false;

	    return true;
	} else
	    return false;
    }

    // ================================================================================
    // magic handlers

    /**
     * set contains
     */
    public abstract void
    handleContains(JCExpression set, JCExpression element);

    /**
     * size check
     */
    public abstract void
    handleSize(JCExpression container);

    /**
     * map get
     */
    public abstract void
    handleMapGet(JCExpression map, JCExpression key);

    /**
     * integral range
     */
    public abstract void
    handleRange(JCExpression first, JCExpression last);

    /**
     * integral range
     */
    public abstract void
    handleRangeContains(JCExpression first, JCExpression last, JCExpression body);

    // /**
    //  * map containsKey
    //  */
    // public abstract void
    // handleMapContainsKey(JCExpression map, JCExpression key);

    // /**
    //  * map containsKey
    //  */
    // public abstract void
    // handleMapContainsValue(JCExpression map, JCExpression value);
}