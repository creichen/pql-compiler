/* Copyright (C) 2011, Christoph Reichenbach
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */
package com.sun.tools.javac.tree.pql;

import java.util.*;

import com.sun.tools.javac.tree.*;
import com.sun.tools.javac.tree.JCTree.*;
import com.sun.tools.javac.code.Type;
import com.sun.tools.javac.code.TypeTags;
import com.sun.tools.javac.code.Symtab;
import com.sun.source.tree.Tree.Kind;

import edu.umass.pql.*;
import com.sun.tools.javac.code.Symbol;
import javax.lang.model.type.TypeKind;
import edu.umass.pql.il.*;
import edu.umass.pql.il.meta.*;


/**
 * Determine whether a given join acts as a binder to one of the variables in a given set (i.e., whether the var occurs in a writing position)
 *
 * Singleton-based, not re-entrant.
 */
public class PQLBinderChecker extends JoinVisitor
{
	VarSet varset;
	boolean result = false;

	private PQLBinderChecker() {};

	static final PQLBinderChecker binder_checker = new PQLBinderChecker();

	public static boolean
	isBinder(Join j, VarSet vars_to_be_bound)
	{
		binder_checker.varset = vars_to_be_bound;
		binder_checker.result = false;
		j.accept(binder_checker);
		return binder_checker.result;
	}

	public void visitDefault(Join it)
	{
		for (int i = it.getReadArgsNr(); i < it.getArgsNr(); i++)
			if (this.varset.contains(it.getArg(i)))
				result = true;
	}

	public void visitControlStructure(ControlStructure sp)
	{
		visitDefault(sp);
	}

	public void visitConjunctiveBlock(AbstractBlock.Conjunctive b)
	{
		// holds for any of them
		visitAnyBlock(b);
		Join[] joins = b.getComponents();
		this.result = checkExists(joins);
	}

	public void visitDisjunctiveBlock(AbstractBlock.Disjunctive b)
	{
		// holds for all of them
		visitAnyBlock(b);
		this.result = checkForall(b.getComponents());
	}


	public void visitNot(Not n) { } // never

	public void visitNoFail(NoFail b) { visitDefault(b); }
	public void visitBool(NoFail.Bool b) { visitNoFail(b); }

	public void visitSelectPath(SelectPath sp)
	{
		Join[] joins = sp.getComponentsToSchedule();
		this.result = checkExists(joins);
	}

	public void visitSchedule(Schedule sp)
	{
		sp.getComponent(0).accept(this);
	}

	// --------------------------------------------------------------------------------
	private boolean
	checkExists(Join[] joins)
	{
		for (Join j : joins) {
			this.result = false;
			j.accept(this);
			if (this.result)
				return true;
		} 
		return false;
	}

	private boolean
	checkForall(Join[] joins)
	{
		for (Join j : joins) {
			this.result = false;
			j.accept(this);
			if (!this.result)
				return false;
		} 
		return true;
	}
}