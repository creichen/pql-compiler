/* Copyright (C) 2011, Christoph Reichenbach
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */
package com.sun.tools.javac.tree.pql;

import java.util.*;

import com.sun.tools.javac.tree.*;
import com.sun.tools.javac.tree.JCTree.*;
import com.sun.tools.javac.code.Type;
import com.sun.tools.javac.code.TypeTags;
import com.sun.tools.javac.code.Symtab;
import com.sun.source.tree.Tree.Kind;

import edu.umass.pql.*;
import com.sun.tools.javac.code.Symbol;
import javax.lang.model.type.TypeKind;

/**
 * Information about a single PQL variable
 */
public class PQLVarInfo
{
    public static final int DISABLED = -1;

    public Type type;
    public int id; // may be DISABLED
    public Symbol sym; // if set: nonquantified variable
    public JCExpression expr; // if set: expression computing this PQLVarInfo
    public boolean type_quantified; // for query variables only: is type-quantified

    /**
     * named variable
     */
    public PQLVarInfo(Type type, int id, Symbol symbol)
    {
	this.type = type;
	this.id = id;
	this.sym = symbol;
    }

    /**
     * constant expression
     */
    public PQLVarInfo(Type type, int id, JCExpression constant)
    {
	this.type = type;
	this.id = id;
	this.expr = constant;
    }

    /**
     * temporary variable
     */
    public PQLVarInfo(Type type, int id)
    {
	this.type = type;
	this.id = id;
    }

    public boolean
    isQueryVar()
    {
	return this.sym != null && this.sym.isQueryVariable();
    }

    public boolean
    isTempVar()
    {
	return this.sym == null && this.expr == null;
    }

    public boolean
    isContextVar()
    {
	return this.sym != null && !this.sym.isQueryVariable();
    }

    public boolean
    isExpr()
    {
	return this.expr != null;
    }

    public boolean
    isConstant()
    {
	return this.isExpr();
    }

    public boolean
    isLiteral()
    {
	return this.isExpr() && this.expr.getTag() == JCTree.LITERAL;
    }

    public String
    toString()
    {
	final String var = Env.showVar(this.id);
	final String ty = "" + this.type;

	if (this.sym != null)
	    return var + " = " + this.sym.toString() + " : " + ty + " (sym)";
	else if (this.expr != null)
	    return var + " = " + this.expr.toString() + " : " + ty + " (expr)";
	else
	    return var + " : " + ty;
    }
}

