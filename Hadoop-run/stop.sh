#! /bin/bash

rm Hadoop-run/conf/mapred-site.xml || (echo "Failed to stop, check directory"; exit 1)
./Hadoop-run/bin/stop-all.sh
