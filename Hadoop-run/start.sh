#! /bin/bash

if [ x$1 == x ]; then
	printf "Usage: $0 <number-of-cores>\nStarts Hadoop.  Make sure to run this from the same directory as Bench.sh!"
	exit 1
fi

cat Hadoop-run/conf/mapred-site.xml.template | sed s/TASKS/$1/ > Hadoop-run/conf/mapred-site.xml || (echo "Failure, check directory"; exit 1)

./Hadoop-run/bin/start-all.sh
