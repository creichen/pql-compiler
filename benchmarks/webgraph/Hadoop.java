package benchmarks.webgraph;

import benchmarks.Evaluator;
import edu.umass.pql.container.PDefaultMap;
import java.io.IOException;
import java.io.File;
import java.io.DataInput;
import java.io.DataOutput;
import java.util.*;
 	
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.*;
import org.apache.hadoop.mapred.lib.aggregate.ValueAggregatorCombiner;
import org.apache.hadoop.mapred.SequenceFileAsBinaryOutputFormat;
import org.apache.hadoop.io.*;
import org.apache.hadoop.fs.*;

import org.apache.log4j.Logger;

public class Hadoop extends Evaluator.HadoopEvaluator
{
	// public static class MiniWebdoc
	// {
	// 	public MiniWebdoc()
	// 	{
	// 		outlinks = new int[0];
	// 	}

	// 	public MiniWebdoc(Webdoc w)
	// 	{
	// 		this.id = w.id;
	// 		this.outlinks = new w.outlinks.size();
	// 		int offset = 0;
	// 		for (Link l : w.outlinks)
	// 			this.outlinks[offset++] = l.destination.id;
	// 	}

	// 	public int id;
	// 	public int[] outlinks;
	// }

	// public static class MiniWebdocWritable implements Writable {
	// 	private MiniWebdoc data = new MiniWebdoc();
	// 	public MiniWebdocWritable()
	// 	{
	// 	}

	// 	public MiniWebdocWritable(MiniWebdoc data)
	// 	{
	// 		this.data = data;
	// 	}

	// 	public void set(MiniWebdoc data)
	// 	{
	// 		this.data = data;
	// 	}

	// 	public MiniWebdoc get()
	// 	{
	// 		return this.data;
	// 	}
       
	// 	public void write(DataOutput out) throws IOException {
	// 		out.writeInt(data.id);

	// 		out.writeInt(data.outlinks.length);
	// 		for (int outlink : data.outlinks)
	// 			out.writeInt(outlink);
	// 	}
       
	// 	public void readFields(DataInput in) throws IOException {
	// 		data.id = in.readInt();

	// 		int links_nr = in.readInt();
	// 		data.outlinks = new int[links_nr];
	// 		for (int i = 0; i < links_nr; i++)
	// 			data.outlinks[i] = in.readInt();
	// 	}
       
	// 	public static MiniWebdocWritable read(DataInput in) throws IOException {
	// 		MiniWebdocWritable w = new MiniWebdocWritable();
	// 		w.readFields(in);
	// 		return w;
	// 	}
	// }


	// We encode outlinks as positive and inlinks as -1-target_doc.  Thus, if a document observes the link 5 and the link -6, it has a cycle through 5.
	public static class MyMapper extends MapReduceBase implements Mapper<IntWritable, IntsWritable, IntWritable, IntWritable> {
		IntWritable eid_src = new IntWritable();
		IntWritable eid_dest = new IntWritable();
		IntWritable rid = new IntWritable();

		@Override
		public void map(IntWritable key, IntsWritable docs, OutputCollector<IntWritable, IntWritable> output, Reporter reporter) throws IOException {
			final int docid = key.get();
			final int[] outlinks = docs.get();

			eid_src.set(docid); // inlink
			eid_dest.set(-1 - docid);

			for (int target : outlinks) {
				rid.set(target);
				output.collect(eid_src, rid);
				output.collect(rid, eid_dest);
			}
		}
	}

	public static class MyReducer extends MapReduceBase implements Reducer<IntWritable, IntWritable, IntWritable, NullWritable> {
		NullWritable nw = NullWritable.get();
		HashSet<Integer> links = new HashSet<Integer>();

		@Override
		public void reduce(IntWritable key, Iterator<IntWritable> values, OutputCollector<IntWritable, NullWritable> output, Reporter reporter) throws IOException {
			links.clear();
			
			while (values.hasNext()) {
				final int link = values.next().get();
				final int inverse_link = -1-link;

				if (links.contains(inverse_link)) {
					// cycle found!
					output.collect(key, nw);
					break;
				}
				links.add(link);
			}
		}
	}
 	


	@Override
	public void
	init()
	{
		pre_init();
		// ----------------------------------------
		// configure job
		conf = new JobConf(Hadoop.class);
		conf.setJobName("webgraph");

		conf.setMapperClass(MyMapper.class);
		// just aggregate, please:
		conf.setReducerClass(MyReducer.class);

		conf.setNumTasksToExecutePerJvm(-1); // don't waste time restarting the JVM

		FileInputFormat.setInputPaths(conf, new Path(INFILE));// + "/" + MapFile.DATA_FILE_NAME));
		FileOutputFormat.setOutputPath(conf, new Path(OUTFILE));


		conf.setInputFormat(SequenceFileInputFormat.class);
		conf.setMapOutputKeyClass(IntWritable.class);
		conf.setMapOutputValueClass(IntWritable.class);

		conf.setOutputFormat(SequenceFileOutputFormat.class);
		conf.setOutputKeyClass(IntWritable.class);
		conf.setOutputValueClass(NullWritable.class);

		// ----------------------------------------
		// write out data
		SequenceFile.Writer writer = null;
		try {

			FileSystem hdfs = FileSystem.get(conf);

			IntWritable key = new IntWritable();
			IntsWritable value = new IntsWritable();

			writer = new SequenceFile.Writer(hdfs,
							 conf,
							 new Path(INFILE),
							 key.getClass(),
							 value.getClass());
			for (int i = 0; i < Generator.documents_array.length; i++) {
				Webdoc doc = Generator.documents_array[i];
				key.set(doc.id);
				int[] outlinks = new int[doc.outlinks.size()];
				int offset = 0;
				for (Link outlink : doc.outlinks)
					outlinks[offset++] = outlink.destination.id;
				value.set(outlinks);
				writer.append(key, value);
			}

		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			if (writer != null)
				IOUtils.closeStream(writer);
		}
	}

	JobConf conf;

	@Override
	public void
	compute()
	{
		try {
			JobClient.runJob(conf);

			SequenceFile.Reader reader = new SequenceFile.Reader(FileSystem.get(conf), new Path(OUTFILE + "/part-00000"), conf);
			IntWritable key = new IntWritable();
			DoubleWritable value = new DoubleWritable();

			HashSet<Webdoc> results = new HashSet<Webdoc>();
			while (reader.next(key)) {
				results.add(Generator.documents_array[key.get()]);
			}

			result = results;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}