package benchmarks;

import java.util.*;
import java.lang.reflect.Array;
import java.io.File;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

// ----------------------------------------
//
//     FIXME: many not implemented yet
//
// ----------------------------------------
public class Bench
{
	public static class Result
	{
		public long init_delta, run_delta, finish_delta;
		public long delta;
		private long size = 1;

		public Result(long start_time, long init_time, long stop_time, long finish_time)
		{
			this.init_delta = (init_time - start_time);
			this.run_delta = (stop_time - init_time);
			this.finish_delta = (finish_time - stop_time);
			this.delta = (finish_time - start_time);
		}

		private Result(Result a, Result b)
		{
			this.init_delta = a.init_delta + b.init_delta;
			this.run_delta = a.run_delta + b.run_delta;
			this.finish_delta = a.finish_delta + b.finish_delta;
			this.delta = a.delta + b.delta;
			this.size = a.size + b.size;
		}

		public long getDelta() { return delta / size; }
		public long getInitDelta() { return init_delta / size; }
		public long getRunDelta() { return run_delta / size; }
		public long getFinishDelta() { return finish_delta / size; }

		public Result
		add(Result r)
		{
			return new Result(this, r);
		}

		public String
		toString()
		{
			return getDelta() + "\t=\t" + getInitDelta() + " + " + getRunDelta() + " + " + getFinishDelta();
		}
	}

	public static class StatisticAggregator
	{
		public ArrayList<Long> data = new ArrayList<Long>();
		public StatisticAggregator() {};

		public void add(long d)
		{
			data.add(d);
		}

		public String
		toString()
		{
			if (data.size() == 0)
				return "0";

			long size = (long) data.size();
			double sum = 0.0;
			for (long l : data)
				sum += l;
			final double mean = sum / size;
			double variance = 0l;
			for (long l : data) {
				double delta = (l - mean);
				variance += delta * delta;
			}
			variance /= size;
			final double std_deviation = Math.sqrt(variance * 1.0);
			final double confidence_delta = std_deviation / Math.sqrt(size * 1.0);
			final long upper_interval = Math.round(mean + confidence_delta);
			final long lower_interval = Math.round(mean - confidence_delta);
			return mean + " [ " + upper_interval + " -.- " + lower_interval + " ]stddev: " + Math.round(std_deviation);
		}
	}

	public static class AggregateResult
	{
		public ArrayList<Result> data = new ArrayList<Result>();
		public AggregateResult() {};

		public void
		add(Result result)
		{
			data.add(result);
		}

		public String
		toString()
		{
			StatisticAggregator all_stat = new StatisticAggregator();
			StatisticAggregator init_stat = new StatisticAggregator();
			StatisticAggregator run_stat = new StatisticAggregator();
			StatisticAggregator finish_stat = new StatisticAggregator();
			for (Result result : data) {
				all_stat.add(result.getDelta());
				init_stat.add(result.getInitDelta());
				run_stat.add(result.getRunDelta());
				finish_stat.add(result.getFinishDelta());
			}
			return all_stat + "   =   init:( " + init_stat + "),  run:( " + run_stat + "),  finish:(" + finish_stat + ")";
		}
	}


	public static final void
	put16(BufferedWriter w, String s) throws IOException
	{
		w.write(s);
		if (s.length() < 8)
			w.write("\t");
		if (s.length() < 16)
			w.write("\t");
	}

	public static final void
	put16(String s)
	{
		System.out.print(s);
		if (s.length() < 8)
			System.out.print("\t");
		if (s.length() < 16)
			System.out.print("\t");
	}


	// ================================================================================
	static class Benchmark
	{
		public Evaluator[] evals;
		public String name;

		public Benchmark(String name, Evaluator ... base_evals)
		{
			this.name = name;
			int evals_nr = 0;

			for (Evaluator e : base_evals)
				evals_nr += e.getMaxParallelism();

			this.evals = new Evaluator[evals_nr];
			int offset = 0;

			for (Evaluator e : base_evals) {
				for (int i = 1; i <= e.getMaxParallelism(); i++) {
					Evaluator ev = e.clone();
					ev.setParallelism(i);
					this.evals[offset++] = ev;
				}
			}
		}
	}

	public static Benchmark[] benchmarksa = new Benchmark[] {
		new Benchmark("threegrep",
			      new benchmarks.threegrep.Manual(),
			      new benchmarks.threegrep.PQL(),
			      new benchmarks.threegrep.ParaManual(),
			      // new benchmarks.threegrep.Hadoop(),
			      // new benchmarks.threegrep.SQLBench(SQL.MYSQL),
			      // new benchmarks.threegrep.SQLBench(SQL.POSTGRESQL)
			      )
		,
		new Benchmark("bonus",
			      new benchmarks.bonus.Manual(),
			      new benchmarks.bonus.PQL(),
			      new benchmarks.bonus.ParaManual(),
			      // new benchmarks.bonus.Hadoop(),
			      // new benchmarks.bonus.SQLBench(SQL.POSTGRESQL),
			      // new benchmarks.bonus.SQLBench(SQL.MYSQL)
			      )
		,
		new Benchmark("webgraph",
			      new benchmarks.webgraph.Manual(),
			      new benchmarks.webgraph.PQL(),
			      new benchmarks.webgraph.ParaManual(),
			      // new benchmarks.webgraph.Hadoop(),
			      // new benchmarks.webgraph.SQLBench(SQL.MYSQL),
			      // new benchmarks.webgraph.SQLBench(SQL.POSTGRESQL)
			      )
		,
		new Benchmark("idf",
			      new benchmarks.idf.Manual(),
			      new benchmarks.idf.PQL(),
			      new benchmarks.idf.ParaManual(),
			      // new benchmarks.idf.Hadoop(),
			      // new benchmarks.idf.SQLBench(SQL.MYSQL),
			      // new benchmarks.idf.SQLBench(SQL.POSTGRESQL)
			      )
	};

	static final Object NO_RESULT = new Object();
	static Evaluator last_evaluator;
	static Object last_result = NO_RESULT;

	static boolean
	objectsEqual(Object o1, Object o2)
	{
		if (o1 == null)
			return o2 == null;

		if (o1.getClass().isArray() != o2.getClass().isArray())
			return false;

		if (o1.getClass().isArray()) {
			if (Array.getLength(o1) !=  Array.getLength(o1))
				return false;
			for (int j = 0; j < Array.getLength(o1); j++)
				if (!objectsEqual(Array.get(o1, j),
						  Array.get(o2, j)))
					return false;
			return true;
		}

		return o1.equals(o2);
	}

	public static Result
	runBenchmark(Evaluator eva)
	{
		System.gc();
		try {
			final long start_time = System.nanoTime();
			eva.init();
			final long init_time = System.nanoTime();
			eva.compute();
			final long stop_time = System.nanoTime();
			Object new_result = eva.obtainResult();
			final long finish_time = System.nanoTime();

			if (last_result != NO_RESULT) {
				if (!objectsEqual(new_result, last_result)) {
					System.err.println("UNEXPECTED RESULTS");
					System.err.println("this eva: " + eva.getClass());
					System.err.println("this result: " + new_result);
					System.err.println("last eva: " + last_evaluator.getClass());
					System.err.println("last result: " + last_result);
					throw new RuntimeException("Result mismatch");
				}
			}

			last_result = new_result;
			last_evaluator = eva;

			return new Result(start_time, init_time, stop_time, finish_time);
		} catch (RuntimeException e) {
			e.printStackTrace();
			throw e;
		} finally {
			eva.cleanup();
		}
	}

	public static void
	printHeader(Benchmark bench)
	{
		System.out.println("\n" + bench.name + "\n" + "----------------\n");
		put16("#impl");
		put16("time");
		System.out.println();
		for (int i = 0 ; i < benchmarksa.length; i++)
			put16(benchmarksa[i].name);
		System.out.println();
	}

	static int DRY_RUNS = 3;
	static int BENCHMARK_RUNS = 10;
	static HashSet<String> evaluators = null;

	public static void
	printTimings(Benchmark bench)
	{
		for (Evaluator eva : bench.evals)
			if (evaluators == null || evaluators.contains(eva)) {
				put16("" + eva.getName());
				put16("" + (doBenchmark(bench, eva)));
				System.out.println();
			}
	}

	static final String rundate = (new java.text.SimpleDateFormat("yyyy-MM-dd--HH:mm:ss")).format(new Date());

	public static void
	storeResult(String bench, String eva, String result)
	{
		String filename = bench + "-" + eva;
		try {
			(new File("results")).mkdir();
			(new File("results/" + rundate)).mkdir();
			BufferedWriter w = new BufferedWriter(new FileWriter("results/" + rundate + "/" + filename));
			put16(w, bench);
			put16(w, eva);
			w.write(result + "\n");
			w.close();
		} catch (IOException e) {
		}
	}

	public static void
	printTimingsCompact(Benchmark bench)
	{
		for (Evaluator eva : bench.evals)
			if (evaluators == null || evaluators.contains(eva.getName())) {
				put16("[BENCH]");
				put16("" + bench.name);
				put16("" + eva.getName());
				String benchresult = (doBenchmark(bench, eva)).toString();
				put16("" + benchresult);
				System.out.println();

				storeResult(bench.name, eva.getName(), benchresult);
			}
	}

	public static AggregateResult
	doBenchmark(Benchmark benchmark, Evaluator eva)
	{
		AggregateResult result = new AggregateResult();

		for (int k = 0; k < DRY_RUNS; k++)
			runBenchmark(eva); // discard

		edu.umass.pql.ParallelQuery.clearTimings();

		for (int k = 0; k < BENCHMARK_RUNS; k++)
			result.add(runBenchmark(eva));

		edu.umass.pql.ParallelQuery.writeAndResetTimings("subtiming." + benchmark.name + "." + eva.getName());

		return result;
	}

	public static void
	printHelp()
	{
		System.out.println("Usage: Bench [options] <command>");
		System.out.println("  options :");
		System.out.println("\t--bench-runs <# of benchmark runs: set to " + BENCHMARK_RUNS +">");
		System.out.println("\t--dry-runs <# of dry runs: set to " + DRY_RUNS +">");
		System.out.println("\t--small\t\t(run on small datasets, for debugging)");
		System.out.println("  commands:\n\tlist benchmarks");
		System.out.println("\tlist evaluators");
		System.out.println("\trun <benchmark> <evaluator>");
	}

	public static void
	main(String[] args)
	{
		int arg_index = 0;

		// process options

		if (arg_index >= args.length) {
			printHelp();
			return;
		}

		while (args[arg_index].startsWith("--")) {
			String option = args[arg_index++].substring(2);
			if (option.equals("bench-runs") && arg_index < args.length)
				BENCHMARK_RUNS = Integer.parseInt(args[arg_index++]);
			else if (option.equals("small"))
				GeneratorBase.SMALL_BENCHMARKS = true;
			else if (option.equals("dry-runs") && arg_index < args.length)
				DRY_RUNS = Integer.parseInt(args[arg_index++]);
			else {
				System.err.println("Unknown option: `--" + option + "'");
				printHelp();
				System.exit(1);
			}
		}

		if (args[arg_index].equals("list")) {
			++arg_index;
			if (arg_index >= args.length) {
				System.err.println("List what?");
				return;
			}
			if (args[arg_index].equals("benchmarks")) {
				for (Benchmark bench : benchmarksa)
					System.out.println(bench.name);
				return;
			} else if (args[arg_index].equals("evaluators")) {
				for (Evaluator eva : benchmarksa[0].evals)
					System.out.println(eva.getName());
				return;
			} else {
				System.err.println("Can't list `" + args[arg_index] + "'");
				return;
			}
		} else if (args[arg_index].equals("run")) {
			if (arg_index >= args.length) {
				System.err.println("Run what?");
				return;
			}
			++arg_index;
			HashSet<String> benchmark_set = null;

			if (!(args[arg_index].equals("all") || args[arg_index].equals("*"))) {
				benchmark_set = new HashSet<String>();
				for (String s : args[arg_index].split(",")) {
					benchmark_set.add(s);
				}
			}
			++arg_index;
			if (!(arg_index >= args.length || args[arg_index].equals("all") || args[arg_index].equals("*"))) {
				evaluators = new HashSet<String>();
				for (String s : args[arg_index].split(",")) {
					evaluators.add(s);
				}
			}

			benchmarks.bonus.Generator.init();
			benchmarks.threegrep.Generator.init();
			benchmarks.webgraph.Generator.init();
			for (Benchmark bench : benchmarksa) {
				last_result = NO_RESULT;
				if (benchmark_set == null || benchmark_set.contains(bench.name)) {
					printTimingsCompact(bench);
				}
			}
		} else {
			System.err.println("Unknown command `" + args[arg_index] + "'");
			printHelp();
			System.exit(1);
		}
	}
}