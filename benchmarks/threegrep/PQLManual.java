package benchmarks.threegrep;
import benchmarks.Evaluator;
import edu.umass.pql.*;
import static edu.umass.pql.PQLFactory.*;
import java.util.*;
import edu.umass.pql.container.*;

public class PQLManual extends Evaluator
{

	Join jbody;

	@Override
	public void
	init()
	{
		PQLFactory.setParallelisationMode(PQLFactory.ParallelisationMode.NONPARALLEL);

		final Join j0 = ARRAY_LOOKUP_Object(41, 0, 24);
		final Join j1 = INT_RANGE_CONTAINS(115, 131, 98);
		final Join j2 = ADD_Int(99, 19, 162);
		final Join j3 = ADD_Int(99, 51, 194);
		final Join j4 = ARRAY_LOOKUP_Byte(25, 195, 67);
		final Join j5 = ARRAY_LOOKUP_Byte(25, 163, 35);
		final Join j6 = ARRAY_LOOKUP_Byte(25, 99, 3);
		final Join j7 = JAVA_TYPE(byte[].class, 25);

		jbody = new AbstractBlock.PreConjunctive(j0, j1, j2, j3, j4, j5, j6, j7) {

				int state = 0;
				int range_offset = 0;
				int source_array_offset = 0;
				byte[][] source;
				byte[] data;

				public void reset(Env env)
				{
					state = 0;
					source_array_offset = 0;
					source = (byte[][]) env.getObject(41);
				}

				@Override
				public Join
				copyRecursively()
				{
					return this; // we is stateless
				}

				@Override
				public boolean next(Env env)
				{
					int index;
					loop: do {
						switch (state) {
						case 0:
							//j0.reset(env);
							state = 1;
						case 1: // ARRAY_LOOKUP_Object(41, 0, 24);
							//if (!j0.next(env)) { return false; }
							//j1.reset(env);
							if (source_array_offset >= source.length)
								return false;
							data = source[source_array_offset++];
							range_offset = 0;
							state = 2;
						case 2: // INT_RANGE_CONTAINS(115, 131, 98); (binding)
							//if (!j1.next(env))
							if (range_offset == 97)
								{ state = 1; continue loop; }
							index = range_offset++;
							// env.v_int[10] = env.v_int[6] + 1;
							// env.v_int[12] = env.v_int[6] + 2;
							// env.setInt(162, env.getInt(99) + env.getInt(19));
							// env.setInt(194, env.getInt(99) + env.getInt(51));
						// 	j2.reset(env);
						// 	state = 3;
						// case 3: // ADD_Int(99, 19, 162);
						// 	if (!j2.next(env)) { state = 2; continue loop; }
						// 	j3.reset(env);
						// 	state = 4;
						// case 4: // ADD_Int(99, 51, 194);
						// 	if (!j3.next(env)) { state = 3; continue loop; }
							//index = env.getInt(99);

							if (data[index] != '0')
							    continue loop;
							if (data[index + 1] != '1')
							    continue loop;
							if (data[index + 2] != '2')
							    continue loop;
							env.setObject(24, data);
						// 	j4.reset(env);
						// 	state = 5;
						// case 5: // ARRAY_LOOKUP_Byte(25, 195, 67);
						// 	if (!j4.next(env)) { state = 2; continue loop; }
						// 	j5.reset(env);
						// 	state = 6;
						// case 6: // ARRAY_LOOKUP_Byte(25, 163, 35);
						// 	if (!j5.next(env)) { state = 5; continue loop; }
						// 	j6.reset(env);
						// 	state = 7;
						// case 7: // ARRAY_LOOKUP_Byte(25, 99, 3);
						// 	if (!j6.next(env)) { state = 6; continue loop; }
						// 	j7.reset(env);
						// 	state = 8;
						// case 8: // JAVA_TYPE(byte[].class, 25);
						// 	if (!j7.next(env)) { state = 7; continue loop; }
							return true;
							//set.add(env.getObject(25));
							//continue;
						default: throw new RuntimeException("Invalid state " + state);
						}

					} while (true);
				}

				@Override public void
				accept(JoinVisitor _) { throw new RuntimeException("Not supported"); } 
			};


	}

		// final Join join =
		// 	Reduction(Reductors.SET(25, 72),
		// 		  ConjunctiveBlock(ARRAY_LOOKUP_Object(41, 0, 24),
		// 				   INT_RANGE_CONTAINS(115, 131, 98),
		// 				   ADD_Int(99, 19, 162),
		// 				   ADD_Int(99, 51, 194),
		// 				   ARRAY_LOOKUP_Byte(25, 195, 67),
		// 				   ARRAY_LOOKUP_Byte(25, 163, 35),
		// 				   ARRAY_LOOKUP_Byte(25, 99, 3),
		// 				   JAVA_TYPE(byte[].class, 25)));

		// Env env = new Env(18, new Object[]{new int[]{'0', 1, '1', 2, '2', 0, 0, 0, Generator.RECORD_SIZE - 4, 0, 0, 0, 0, 0, 0, 0},
		// 				   new Object[]{null, null, Generator.data_array, null, null}});

	public void
	compute()
	{
		Env env = new Env(18, new Object[]{new int[]{'0', 1, '1', 2, '2', 0, 0, 0, Generator.RECORD_SIZE - 3, 0, 0, 0, 0, 0, 0, 0},
						   new Object[]{null, null, Generator.data_array, null, null}});


		// public static class RJoin extends Join
		// {
		// }

		PSet<Object> set = new PSet<Object>();
		jbody.reset(env);
		while (jbody.next(env))
			set.add(env.getObject(25));
		result = set;
 
		// final Join join =
		// 	Reduction(Reductors.SET(25, 72),
		// 		  jbody);
		// join.reset(env);
		// join.next(env);
		// result = env.getObject(73);




		// j0.reset(env);
		// while (j0.next(env)) {
		// 	j1.reset(env);
		// 	while (j1.next(env)) {
		// 		j2.reset(env);
		// 		while (j2.next(env)) {
		// 			j3.reset(env);
		// 			while (j3.next(env)) {
		// 				j4.reset(env);
		// 				while (j4.next(env)) {
		// 					j5.reset(env);
		// 					while (j5.next(env)) {
		// 						j6.reset(env);
		// 						while (j6.next(env)) {
		// 							j7.reset(env);
		// 							while (j7.next(env)) {
		// 								set.add(env.getObject(25));
		// 							}
		// 						}
		// 					}
		// 				}
		// 			}
		// 		}
		// 	}
		// }

		// result = set;
	}
}
