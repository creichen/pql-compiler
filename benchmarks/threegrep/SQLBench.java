package benchmarks.threegrep;
import benchmarks.Evaluator;
import edu.umass.pql.container.*;
import benchmarks.SQL;
import java.sql.*;

import java.util.*;

public class SQLBench extends Evaluator
{
	public SQLBench(SQL sql)
	{
		this.sql = sql;
	}

	public SQL sql;

	PreparedStatement query;

	@Override
	public void
	init()
	{
		try {
			sql.open("threegrep");
			if (sql.isMySQL()) {
				sql.stmt("USE " + SQL.MYSQL_LOGIN + ";"); // use CREATE DATABASE to set this up
			}
			sql.createTable("data", "ID INT NOT NULL PRIMARY KEY, BODY CHAR("+Generator.RECORD_SIZE+") NOT NULL");

			PreparedStatement putter = sql.insertStatement("data", "?, ?");
			for (int offset = 0; offset < Generator.data_array.length; offset++) {
				byte[] data = Generator.data_array[offset];
				char[] d = new char[data.length];
				for (int i = 0 ; i < data.length; i++)
					d[i] = (char) data[i];
				String s = new String(d);

				putter.setInt(1, offset);
				putter.setString(2, s);
				putter.addBatch();
			}
			sql.runBatchUpdate(putter);

			if (sql.isMySQL())
				this.query = sql.prepareQuery("data", "ID", "WHERE (LOCATE (?, BODY) > 0)");
			else
				this.query = sql.prepareQuery("data", "ID", "WHERE (POSITION (? IN BODY) > 0)");
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void
	cleanup()
	{
		sql.deleteTable("data");
		sql.close();
	}

	@Override
	public void
	compute()
	{
		try {
			PSet<byte[]> results = new PSet<byte[]>();
			query.setString(1, "012");
			final ResultSet res = sql.runQuery(query);

			while (res.next()) {
				results.add(Generator.data_array[res.getInt(1)]);
			}

			this.result = results;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public String
	getName()
	{
		return this.sql.getDriverName();
	}
}
