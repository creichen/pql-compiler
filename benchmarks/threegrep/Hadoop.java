package benchmarks.threegrep;

import benchmarks.Evaluator;
import edu.umass.pql.container.PDefaultMap;
import java.io.IOException;
import java.io.File;
import java.io.DataInput;
import java.io.DataOutput;
import java.util.*;
 	
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.*;
import org.apache.hadoop.mapred.lib.aggregate.ValueAggregatorCombiner;
import org.apache.hadoop.mapred.SequenceFileAsBinaryOutputFormat;
import org.apache.hadoop.io.*;
import org.apache.hadoop.fs.*;

import org.apache.log4j.Logger;

public class Hadoop extends Evaluator.HadoopEvaluator
{
	public static class MyMapper extends MapReduceBase implements Mapper<IntWritable, BytesWritable, IntWritable, NullWritable> {
		NullWritable nw = NullWritable.get();

		@Override
		public void map(IntWritable key, BytesWritable docs, OutputCollector<IntWritable, NullWritable> output, Reporter reporter) throws IOException {

			byte[] data = docs.get();

			for (int i = 0; i < data.length - 2; i++)
				if (data[i] == '0'
				    && data[i + 1] == '1'
				    && data[i + 2] == '2') {
					output.collect(key, nw);
					break;
				}
		}
	}

	public static class MyReducer extends MapReduceBase implements Reducer<IntWritable, NullWritable, IntWritable, NullWritable> {
		private final static IntWritable result = new IntWritable(0);

		@Override
		public void reduce(IntWritable key, Iterator<NullWritable> values, OutputCollector<IntWritable, NullWritable> output, Reporter reporter) throws IOException {
			NullWritable nw = NullWritable.get();
			output.collect(key, nw);
		}
	}
 	


	@Override
	public void
	init()
	{
		pre_init();
		// ----------------------------------------
		// configure job
		conf = new JobConf(Hadoop.class);
		conf.setJobName("threegrep");

		conf.setMapperClass(MyMapper.class);
		// just aggregate, please:
		conf.setReducerClass(MyReducer.class);

		conf.setNumTasksToExecutePerJvm(-1); // don't waste time restarting the JVM

		FileInputFormat.setInputPaths(conf, new Path(INFILE));// + "/" + MapFile.DATA_FILE_NAME));
		FileOutputFormat.setOutputPath(conf, new Path(OUTFILE));


		conf.setInputFormat(SequenceFileInputFormat.class);
		conf.setMapOutputKeyClass(IntWritable.class);
		conf.setMapOutputValueClass(NullWritable.class);

		conf.setOutputFormat(SequenceFileOutputFormat.class);
		conf.setOutputKeyClass(IntWritable.class);
		conf.setOutputValueClass(NullWritable.class);

		// ----------------------------------------
		// write out data
		SequenceFile.Writer writer = null;
		try {

			FileSystem hdfs = FileSystem.get(conf);

			IntWritable key = new IntWritable();
			BytesWritable value = new BytesWritable();
			int bytesRead;

			int wc = 0;

			writer = new SequenceFile.Writer(hdfs,
							 conf,
							 new Path(INFILE),
							 key.getClass(),
							 value.getClass());
			for (int i = 0; i < Generator.data_array.length; i++) {
				key.set(i);
				value.set(Generator.data_array[i], 0, Generator.RECORD_SIZE);
				writer.append(key, value);
			}

		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			if (writer != null)
				IOUtils.closeStream(writer);
		}
	}

	JobConf conf;

	@Override
	public void
	compute()
	{
		try {
			JobClient.runJob(conf);

			SequenceFile.Reader reader = new SequenceFile.Reader(FileSystem.get(conf), new Path(OUTFILE + "/part-00000"), conf);
			IntWritable key = new IntWritable();

			HashSet<byte[]> results = new HashSet<byte[]>();
			while (reader.next(key)) {
				results.add(Generator.data_array[key.get()]);
			}

			result = results;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}