package benchmarks.threegrep;
import benchmarks.Evaluator;
import edu.umass.pql.container.*;
import java.util.*;

public class ParaManual extends Evaluator.ParaEvaluator
{
	Set<byte[]> results;

	class EvaluatorThread extends Thread
	{
		public EvaluatorThread(int start, int stop)
		{
			this.start = start;
			this.stop = stop;
		}
		int start, stop;

		public void run()
		{
			for (int i = start; i < stop; i++) {
				byte[] ba = Generator.data_array[i];
				for (int k = 0; k <= Generator.RECORD_SIZE - 3; k++)
					if (ba[k] == '0' && ba[k + 1] == '1' && ba[k + 2] == '2') {
						results.add(ba);
						break;
					}
			}
		}
	}

	@Override
	public Thread
	gen_thread(int _, int start, int stop)
	{
		return new EvaluatorThread(start, stop);
	}

	@Override
	public int
	get_size()
	{
		return Generator.data_array.length;
	}

	@Override
	public void
	compute_prepare()
	{
		results = Collections.synchronizedSet(new PSet<byte[]>());
	}

	@Override
	public Object
	compute_finish()
	{
		return results;
	}
}
