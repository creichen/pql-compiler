package benchmarks.threegrep;
import benchmarks.Evaluator;
import edu.umass.pql.container.*;
import java.util.*;

public class Manual extends Evaluator
{
	public void
	compute()
	{
		final PSet<byte[]> results = new PSet<byte[]>();

		for (byte[] ba : Generator.data_array) {
			for (int k = 0; k <= Generator.RECORD_SIZE - 3; k++)
				if (ba[k] == '0' && ba[k + 1] == '1' && ba[k + 2] == '2') {
					results.add(ba);
					break;
				}
		}

		this.result = results;
	}

	public String
	getName()
	{
		return "manual";
	}
}
