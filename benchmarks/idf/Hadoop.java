package benchmarks.idf;

import benchmarks.Evaluator;
import edu.umass.pql.container.PDefaultMap;
import java.io.IOException;
import java.io.File;
import java.io.DataInput;
import java.io.DataOutput;
import java.util.*;
 	
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.*;
import org.apache.hadoop.mapred.lib.aggregate.ValueAggregatorCombiner;
import org.apache.hadoop.mapred.SequenceFileAsBinaryOutputFormat;
import org.apache.hadoop.io.*;
import org.apache.hadoop.fs.*;

import org.apache.log4j.Logger;

import benchmarks.webgraph.Generator;
import benchmarks.webgraph.Webdoc;

public class Hadoop extends Evaluator.HadoopEvaluator
{
	public static class MyMapper extends MapReduceBase implements Mapper<IntWritable, IntsWritable, IntWritable, IntWritable> {
		private final static IntWritable one = new IntWritable(1);
		private final static IntWritable word = new IntWritable(0);

		@Override
		public void map(IntWritable key, IntsWritable docs, OutputCollector<IntWritable, IntWritable> output, Reporter reporter) throws IOException {

			int[] data = docs.get();

			for (int word_v : data) {
				word.set(word_v);
				output.collect(word, one);
			}
		}
	}

	public static class MyReducer extends MapReduceBase implements Reducer<IntWritable, IntWritable, IntWritable, IntWritable> {
		private final static IntWritable result = new IntWritable(0);

		@Override
		public void reduce(IntWritable key, Iterator<IntWritable> values, OutputCollector<IntWritable, IntWritable> output, Reporter reporter) throws IOException {
			int sum = 0;
			while (values.hasNext()) {
				sum += values.next().get();
			}
			result.set(sum);
			output.collect(key, result);
		}
	}
 	


	@Override
	public void
	init()
	{
		pre_init();
		// ----------------------------------------
		// configure job
		conf = new JobConf(Hadoop.class);
		conf.setJobName("idf");
 	
		conf.setMapperClass(MyMapper.class);
		// just aggregate, please:
		conf.setReducerClass(MyReducer.class);

		conf.setNumTasksToExecutePerJvm(-1); // don't waste time restarting the JVM

		FileInputFormat.setInputPaths(conf, new Path(INFILE));// + "/" + MapFile.DATA_FILE_NAME));
		FileOutputFormat.setOutputPath(conf, new Path(OUTFILE));


		conf.setInputFormat(SequenceFileInputFormat.class);
		conf.setMapOutputKeyClass(IntWritable.class);
		conf.setMapOutputValueClass(IntWritable.class);

		conf.setOutputFormat(MapFileOutputFormat.class);
		conf.setOutputKeyClass(IntWritable.class);
		conf.setOutputValueClass(IntWritable.class);

		// ----------------------------------------
		// write out data
		SequenceFile.Writer writer = null;
		try {

			FileSystem hdfs = FileSystem.get(conf);

			IntWritable key = new IntWritable();
			IntsWritable value = new IntsWritable();
			int bytesRead;

			int wc = 0;
			writer = new SequenceFile.Writer(hdfs,
							 conf,
							 new Path(INFILE),
							 key.getClass(),
							 value.getClass());
			for (Webdoc doc : Generator.documents_array) {
				key.set(doc.id);
				value.set(doc.words);
				writer.append(key, value);
			}
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			if (writer != null)
				IOUtils.closeStream(writer);
		}
	}

	JobConf conf;

	@Override
	public void
	compute()
	{
		try {
			JobClient.runJob(conf);

			MapFile.Reader reader = new MapFile.Reader(FileSystem.get(conf), OUTFILE + "/part-00000", conf);
			IntWritable key = new IntWritable();
			IntWritable value = new IntWritable();

			Map<Integer, Integer> results = new PDefaultMap<Integer, Integer>(0);
			while (reader.next(key, value)) {
				int count = value.get();
				if (count > 0)
					results.put(key.get(), count);
			}

			result = results;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}