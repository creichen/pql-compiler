package benchmarks.idf;
import benchmarks.Evaluator;
import edu.umass.pql.container.PDefaultMap;
import java.util.*;
import benchmarks.webgraph.Webdoc;
import benchmarks.webgraph.Generator;

public class Manual extends Evaluator
{
	public void
	compute()
	{
		final Map<Integer, Integer> results = new PDefaultMap<Integer, Integer>(0);

		for (Webdoc doc : Generator.documents_array) {
			for (int word_id : doc.words) {
				results.put(edu.umass.pql.Env.canonicalInteger(word_id),
					    results.get(word_id) + 1);
			}
		}

		result = results;
	}

	public String
	getName()
	{
		return "manual";
	}
}
