package benchmarks.idf;
import benchmarks.Evaluator;
import static edu.umass.pql.Query;
import static edu.umass.pql.Query.sumInt;
import static edu.umass.pql.Query.range;
import java.util.*;
import benchmarks.webgraph.Generator;
import benchmarks.webgraph.Webdoc;
import benchmarks.webgraph.Link;

public class PQL extends Evaluator.PQLEvaluator
{
	public void
	compute()
	{
		Set<Webdoc> documents = Generator.documents;
		int WORDS_NR = Generator.WORDS_NR;

		result = query(Map.get(int word_id) == int idf default 0):
		           // The explicit `range' here is required due to range inference.  This also slows down
		           // query execution, unfortunately.  While the IL would allow us the `efficient' version,
		           // the source language does not support this feature yet.
		           // range(0, WORDS_NR).contains(word_id)
			   // &&
		           idf == reduce(sumInt) one over doc: one == 1
                                                  && documents.contains(doc)
			                          && exists i: doc.words[i] == word_id;
	}
}
