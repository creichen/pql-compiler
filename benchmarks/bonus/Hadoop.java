package benchmarks.bonus;

import benchmarks.Evaluator;
import edu.umass.pql.container.PDefaultMap;
import java.io.IOException;
import java.io.File;
import java.io.DataInput;
import java.io.DataOutput;
import java.util.*;
 	
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.*;
import org.apache.hadoop.mapred.lib.aggregate.ValueAggregatorCombiner;
import org.apache.hadoop.mapred.SequenceFileAsBinaryOutputFormat;
import org.apache.hadoop.io.*;
import org.apache.hadoop.fs.*;

import org.apache.log4j.Logger;

public class Hadoop extends Evaluator.HadoopEvaluator
{
	static Department[] departments = Generator.genDepartments();

	public static class EmployeeWritable implements Writable {
		private Employee data = new Employee(null);
		public EmployeeWritable()
		{
		}

		public EmployeeWritable(Employee data)
		{
			this.data = data;
		}

		public void set(Employee data)
		{
			this.data = data;
		}

		public Employee get()
		{
			return this.data;
		}
       
		public void write(DataOutput out) throws IOException {
			out.writeInt(data.id);
			out.writeInt(data.dept.id);
			out.writeInt(data.bonusSet.size());

			for (Bonus bonus : data.bonusSet) {
				out.writeDouble(bonus.bonus_base);
			}
		}
       
		public void readFields(DataInput in) throws IOException {
			data.id = in.readInt();
			final int dept_id = in.readInt();
			data.dept = Hadoop.departments[dept_id];
			final int boni_nr = in.readInt();

			final HashSet<Bonus> set = new HashSet<Bonus>(boni_nr);
			data.bonusSet = set;

			for (int i = 0; i < boni_nr; i++) {
				double d = in.readDouble();
				set.add(new Bonus("", d));
			}
		}
       
		public static EmployeeWritable read(DataInput in) throws IOException {
			EmployeeWritable w = new EmployeeWritable();
			w.readFields(in);
			return w;
		}
	} // 38


	public static class MyMapper extends MapReduceBase implements Mapper<EmployeeWritable, NullWritable, IntWritable, DoubleWritable> {
		IntWritable eid = new IntWritable();
		DoubleWritable rid = new DoubleWritable();

		@Override
		public void map(EmployeeWritable key, NullWritable docs, OutputCollector<IntWritable, DoubleWritable> output, Reporter reporter) throws IOException {
			double sum = 0.0;
			final Employee e = key.get();
			for (Bonus b : e.bonusSet)
				sum += b.bonus_base;
			eid.set(e.id);
			rid.set(sum * e.dept.bonus_factor);
			output.collect(eid, rid);
		}
	}

	public static class MyReducer extends MapReduceBase implements Reducer<IntWritable, DoubleWritable, IntWritable, DoubleWritable> {

		@Override
		public void reduce(IntWritable key, Iterator<DoubleWritable> values, OutputCollector<IntWritable, DoubleWritable> output, Reporter reporter) throws IOException {
			output.collect(key, values.next());
		}
	}
 	


	@Override
	public void
	init()
	{
		pre_init();
		// ----------------------------------------
		// configure job
		conf = new JobConf(Hadoop.class);
		conf.setJobName("bonus");

		conf.setMapperClass(MyMapper.class);
		// just aggregate, please:
		conf.setReducerClass(MyReducer.class);

		conf.setNumTasksToExecutePerJvm(-1); // don't waste time restarting the JVM

		FileInputFormat.setInputPaths(conf, new Path(INFILE));// + "/" + MapFile.DATA_FILE_NAME));
		FileOutputFormat.setOutputPath(conf, new Path(OUTFILE));


		conf.setInputFormat(SequenceFileInputFormat.class);
		conf.setMapOutputKeyClass(IntWritable.class);
		conf.setMapOutputValueClass(DoubleWritable.class);

		conf.setOutputFormat(MapFileOutputFormat.class);
		conf.setOutputKeyClass(IntWritable.class);
		conf.setOutputValueClass(DoubleWritable.class);

		// ----------------------------------------
		// write out data
		SequenceFile.Writer writer = null;
		try {

			FileSystem hdfs = FileSystem.get(conf);

			EmployeeWritable key = new EmployeeWritable();
			NullWritable value = NullWritable.get();
			int bytesRead;

			int wc = 0;

			writer = new SequenceFile.Writer(hdfs,
							 conf,
							 new Path(INFILE),
							 key.getClass(),
							 value.getClass());
			for (int i = 0; i < Generator.employees_array.length; i++) {
				Employee employee = Generator.employees_array[i];
				key.set(employee);
				writer.append(key, value);
			}

		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			if (writer != null)
				IOUtils.closeStream(writer);
		}
	}

	JobConf conf;

	@Override
	public void
	compute()
	{
		try {
			JobClient.runJob(conf);

			MapFile.Reader reader = new MapFile.Reader(FileSystem.get(conf), OUTFILE + "/part-00000", conf);
			IntWritable key = new IntWritable();
			DoubleWritable value = new DoubleWritable();

			HashMap<Employee, Double> results = new HashMap<Employee, Double>();
			int sanity = 0;
			while (reader.next(key, value)) {
				sanity++;
				results.put(Generator.employees_array[key.get()], value.get());
			}
			if (sanity == 0)
				throw new RuntimeException("Failed sanity check");

			result = results;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}