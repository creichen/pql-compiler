package benchmarks.bonus;

import java.util.*;

public class Employee
{
	private static int idc = 0;
	public int id = idc++;
	Department dept;
	Set<Bonus> bonusSet;

	static Comparator comp = new Comparator()
		{
			public int compare(Object o1, Object o2)
			{
				return ((Bonus)o1).bonus_descr.compareTo(((Bonus)o2).bonus_descr);
			}
			public boolean equals (Object o)
			{
				return o == this;
			}
		};

	public Employee(Department dept)
	{
		this.dept = dept;
		bonusSet = new TreeSet<Bonus>(comp);
	}

	@Override
	public int
	hashCode()
	{
		return this.id;
	}
}