package benchmarks;
import java.util.Map;

public class GeneratorBase
{
	public static boolean SMALL_BENCHMARKS = false;

	public static int getVar(String varname, int default_value)
	{
		Map<String, String> env = System.getenv();
		final String limit_str = env.get(varname);
		if (limit_str == null)
			return default_value;
		else try {
				return Integer.decode(limit_str);
			} catch (Exception _) {
				return default_value;
			}
	}
}