package benchmarks;

import java.io.File;
import java.io.IOException;
import edu.umass.pql.*;
/*
import org.apache.hadoop.conf.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.fs.*;
*/

import java.io.DataInput;
import java.io.DataOutput;

public abstract class Evaluator implements Cloneable
{
	protected Object result;
	protected int para = 1;

	public Evaluator
	clone()
	{
		try {
			return (Evaluator) super.clone();
		} catch (CloneNotSupportedException e) {
			throw new RuntimeException(e);
		}
	}

	public void
	init()
	{
	}

	public void
	cleanup()
	{
	}

	/**
	 * Trigger computation and force it to conclude
	 *
	 * If no transfer time is needed, just write the result to `result'
	 */
	public abstract void
	compute();

	/**
	 * Transfer data to querying program (if needed)
	 */
	public Object
	obtainResult()
	{
		return this.result;
	}

	public int
	getMaxParallelism()
	{
		return 1;
	}

	public void
	setParallelism(int i)
	{
	}

	public String
	getName()
	{
		return this.getClass().getSimpleName() + this.para;
	}

	public static abstract class ParaEvaluator extends Evaluator
	{
		public int
		getMaxParallelism()
		{
			return Env.DEFAULT_THREADS_NR;
		}

		public void
		setParallelism(int i)
		{
			this.para = i;
		}

		public void
		compute_prepare() {}; 

		public abstract Object
		compute_finish();

		public abstract int
		get_size();

		public abstract Thread
		gen_thread(int index, int start, int stop);

		public void
		compute()
		{
			compute_prepare();

			Thread[] threads = new Thread[para];

			final int total = get_size();
			int start = 0;
			int size = total / para;
			int excess = total - (size * para);
			++size;

			for (int i = 0; i < threads.length; i++) {
				if (excess-- == 0)
					--size;
				final int stop = start + size;
				threads[i] = gen_thread(i, start, stop);
				threads[i].start();
				start = stop;
			}

			boolean retry;
			do {
				retry = false;
				try {
					for (int i = 0; i < threads.length; i++)
						threads[i].join();
				} catch (InterruptedException _) {
					retry = true;
				}
			} while (retry);

			this.result = compute_finish();
		}

		public String
		getName()
		{
			return "para-manual-" + para;
		}
	}

	public static abstract class PQLEvaluator extends Evaluator
	{
		public final void
		init()
		{
			if (this.para == 1) {
				PQLFactory.setParallelisationMode(PQLFactory.ParallelisationMode.NONPARALLEL);
				Env.setThreadsNr(1);
			} else {
				PQLFactory.setParallelisationMode(PQLFactory.ParallelisationMode.SEGMENTED);
				Env.setThreadsNr(this.para);
			}
		}

		public int
		getMaxParallelism()
		{
			return Env.DEFAULT_THREADS_NR;
		}

		public void
		setParallelism(int i)
		{
			this.para = i;
		}

		public String
		getName()
		{
			return "pql-" + para;
		}
	}

	// public static abstract class HadoopEvaluator extends Evaluator
	// {
	// 	public static class IntsWritable implements Writable {
	// 		private int[] data;
	// 		public IntsWritable()
	// 		{
	// 			data = new int[0];
	// 		}

	// 		public IntsWritable(int[] data)
	// 		{
	// 			this.data = data;
	// 		}

	// 		public void set(int[] data)
	// 		{
	// 			this.data = data;
	// 		}

	// 		public int[] get()
	// 		{
	// 			return this.data;
	// 		}
       
	// 		public void write(DataOutput out) throws IOException {
	// 			out.writeInt(data.length);
	// 			for (int i = 0; i < data.length; i++)
	// 				out.writeInt(data[i]);
	// 		}
       
	// 		public void readFields(DataInput in) throws IOException {
	// 			final int length = in.readInt();
	// 			final int[] d = new int[length];
	// 			for (int i = 0; i < length; i++)
	// 				d[i] = in.readInt();
	// 			this.data = d;
	// 		}
       
	// 		public static IntsWritable read(DataInput in) throws IOException {
	// 			IntsWritable w = new IntsWritable();
	// 			w.readFields(in);
	// 			return w;
	// 		}
	// 	}


	// 	public static class DoublesWritable implements Writable {
	// 		private double[] data;
	// 		public DoublesWritable()
	// 		{
	// 			data = new double[0];
	// 		}

	// 		public DoublesWritable(double[] data)
	// 		{
	// 			this.data = data;
	// 		}

	// 		public void set(double[] data)
	// 		{
	// 			this.data = data;
	// 		}

	// 		public double[] get()
	// 		{
	// 			return this.data;
	// 		}
       
	// 		public void write(DataOutput out) throws IOException {
	// 			out.writeInt(data.length);
	// 			for (int i = 0; i < data.length; i++)
	// 				out.writeDouble(data[i]);
	// 		}
       
	// 		public void readFields(DataInput in) throws IOException {
	// 			final int length = in.readInt();
	// 			final double[] d = new double[length];
	// 			for (int i = 0; i < length; i++)
	// 				d[i] = in.readDouble();
	// 			this.data = d;
	// 		}
       
	// 		public static DoublesWritable read(DataInput in) throws IOException {
	// 			DoublesWritable w = new DoublesWritable();
	// 			w.readFields(in);
	// 			return w;
	// 		}
	// 	}

	// 	static void delete(File f) throws IOException {
	// 		if (f.isDirectory()) {
	// 			for (File c : f.listFiles())
	// 				delete(c);
	// 		}
	// 		if (!f.delete()) {
	// 			System.err.println("Failed to delete file: " + f);
	// 		}
	// 	}


	// 	public static int THREADS_NR = 0;
	// 	public static int LAST_THREADS_NR = -1;

	// 	public static final String HDDATA = "./hadoop-data";
	// 	public static final String INFILE = HDDATA + "/intable"; // needn't strictly be a table
	// 	public static final String OUTFILE = HDDATA + "/outtable";

	// 	public void
	// 	pre_init()
	// 	{
	// 		try {
	// 			delete(new File(HDDATA));
	// 			(new File(HDDATA)).mkdir();
	// 		} catch (IOException e) {
	// 			throw new RuntimeException(e);
	// 		}

	// 		// full pre-initialisation takes a lot of time, since we may have to wait for the servers to come up.
	// 		// Hence we only restart them if the number of threads changed.
	// 		if (LAST_THREADS_NR == THREADS_NR)
	// 			return;

	// 		LAST_THREADS_NR = THREADS_NR; // time to update

	// 		try {
	// 			System.err.println("[] Shutting down Hadoop...");
	// 			Runtime.getRuntime().exec("Hadoop-run/stop.sh");
	// 			Thread.sleep(3000); // should suffice to bring them down
	// 			int decaseconds = 6;  // 60 seconds to bring them up; a mite conservative.
	// 			System.err.println("[] Starting Hadoop (takes " + decaseconds + "0s...");
	// 			Runtime.getRuntime().exec("Hadoop-run/start.sh " + THREADS_NR);
	// 			for (; decaseconds > 0; --decaseconds) {
	// 				System.err.print(decaseconds + " . . . ");
	// 				Thread.sleep(10000);
	// 			}
	// 			System.err.println("lift-off!");
	// 		} catch (Exception e) {
	// 			e.printStackTrace();
	// 			throw new RuntimeException(e);
	// 		}
	// 	}

	// 	public int
	// 	getMaxParallelism()
	// 	{
	// 		return Env.DEFAULT_THREADS_NR;
	// 	}

	// 	public void
	// 	setParallelism(int i)
	// 	{
	// 		THREADS_NR = i;
	// 		this.para = i;
	// 	}

	// 	public String
	// 	getName()
	// 	{
	// 		return "hadoop-" + para;
	// 	}
	// }
}
