#! /bin/bash

ILTEST=./il-test.sh
JUNITTEST=./junit-test.sh
SFAILTEST=./static-fail.sh

COUNT=0
FAILURES=0

ILTESTS="y"
EVALTESTS="y"
SFAILTESTS="y"

if [ x$1 == xeval ]; then
    ILTESTS="n"
    SFAILTESTS="n"
fi

if [ x$1 == xil ]; then
    EVALTESTS="n"
    SFAILTESTS="n"
fi

if [ x$1 == xsfail ]; then
    ILTESTS="n"
    EVALTESTS="n"
fi

if ls -1 t/*.java > /dev/null; then printf ""; else ILTESTS=n; fi
if [ $ILTESTS == y ]; then
    for j in t/*.java; do
	COUNT=$((COUNT + 1))
	if ${ILTEST} $j; then
	    printf "  %s \033[1;32mOK\033[0m\n" $j
	else
	    printf "  \033[1;31mFAILURE\033[0m on %s\n" $j
	    FAILURES=$((1 + FAILURES))
	fi
    done
fi

if ls -1 u/*.java > /dev/null; then printf ""; else EVALTESTS=n; fi
if [ $EVALTESTS == y ]; then
    for j in u/*.java; do
	COUNT=$((COUNT + 1))
	if ${JUNITTEST} $j; then
	    printf "  %s \033[1;32mOK\033[0m\n" $j
	else
	    printf "  \033[1;31mFAILURE\033[0m on %s\n" $j
	    FAILURES=$((1 + FAILURES))
	fi
    done
fi

if ls -1 e/*.java > /dev/null; then printf ""; else SFAILTESTS=n; fi
if [ ${SFAILTESTS} == y ]; then
    for j in e/*.java; do
	COUNT=$((COUNT + 1))
	if ${SFAILTEST} $j; then
	    printf "  %s \033[1;32mOK\033[0m\n" $j
	else
	    printf "  \033[1;31mFAILURE\033[0m on %s\n" $j
	    FAILURES=$((1 + FAILURES))
	fi
    done
fi

if [ $FAILURES == 0 ]; then
    echo "All tests succeeded."
else
    printf "%d / %d tests failed.\n" $FAILURES $COUNT 
fi
