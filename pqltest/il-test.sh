#! /bin/bash
ASM=../lib/asm-4.1.jar
PQLIB=../../pqlib/pqlib.jar
PQLC="../dist/bootstrap/bin/javac -attrparseonly -dumppql -cp ${ASM}:${CLASSPATH}:${PQLIB}"
JAVAFILE=$1
EXPECTED=${JAVAFILE}.expected

if ! [ -e ${EXPECTED} ]; then
    echo "Missing: ${EXPECTED}"
    exit 1
fi

TEMP=`mktemp /tmp/actual.XXXXXX`

${PQLC} ${JAVAFILE} > ${TEMP}
if diff -u ${TEMP} ${EXPECTED}; then
    rm -f ${TEMP}
    exit 0
else
    rm -f ${TEMP}
    exit 1
fi