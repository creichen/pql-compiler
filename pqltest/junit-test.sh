#! /bin/bash
ASM=../lib/asm-4.1.jar
PQLIB2=../pqlib/pqlib.jar
PQLIB=../${PQLIB2}
#JAVA=./jikes/rvm
JAVA=`which java`
JUNITLIB="./junit.jar"
SUPPORTFILES="u/ContainerGen.java"
CP="${ASM}:${CLASSPATH}:${PQLIB}:${JUNITLIB}"
CP2="${ASM}:${CLASSPATH}:${PQLIB2}:${JUNITLIB}"
PQLC="../dist/bootstrap/bin/javac -printpqlgen -dumppql -cp ${CP}"
#PQLC="../dist/bootstrap/bin/javac -interpretpql -printpqlgen -dumppql -cp ${CP}"
JUNIT="${JAVA} -classpath ${CP2} -DPQL_THREADS=4 -DPQL_PARALLELISM_MODE=segmented org.junit.runner.JUnitCore"
#JUNIT="${JAVA} -DPQL_PARALLELISM_MODE=none -classpath ${CP2} org.junit.runner.JUnitCore"
JAVAFILE="$1"

if ${PQLC} ${JAVAFILE} ${SUPPORTFILES}
then printf "" 
else exit 1
fi
cd ..

# if [ ! -e ${JAVA} ]; then
#     printf "Jikes RVM runtime (%s/%s) expected\n" `pwd` ${JAVA}
#     exit 1
# else
#     printf "Found jikes at %s/%s\n"  `pwd` ${JAVA}
# fi

echo "Testing: ${JAVAFILE}"
echo "Classpath = " ${CP2}
${JUNIT} pqltest.`echo ${JAVAFILE} | sed 's/.java//1' | tr '/' '.'`
