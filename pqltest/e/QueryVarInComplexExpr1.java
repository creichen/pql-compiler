package pqltest.u;

import static edu.umass.pql.Query;

public class QueryVarInComplexExpr1
{
	boolean b = exists int x: x > 1 && (new Integer(x) == null);
}