package pqltest.u;

import static edu.umass.pql.Query;
import static edu.umass.pql.Query.*;
public class ReductionMethodIllTyped3
{
	int i = reduce (sumInt) i: range(1, 10).contains(i);

	public static final int
	sumInt(int i, double d)
	{
		return i;
	}
}