package pqltest.u;

import static edu.umass.pql.Query;
import static edu.umass.pql.Query.*;
public class ReductionMethodIllTyped2
{
	int i = reduce (sumInt) i: range(1, 10).contains(i);

	public static final int
	sumInt(int i)
	{
		return i;
	}
}