package pqltest.u;

import static edu.umass.pql.Query;
import static edu.umass.pql.Query.*;
public class ReductionMethodNotStatic0
{
	int i = reduce (sumInt) i: range(1, 10).contains(i);

	public final int
	sumInt(int i, int j)
	{
		return i + j;
	}
}