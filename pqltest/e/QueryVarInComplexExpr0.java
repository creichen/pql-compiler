package pqltest.u;

import static edu.umass.pql.Query;

public class QueryVarInComplexExpr0
{
	static int f(int x)
	{
		System.out.println(x);
		return x+1;
	} 

	boolean b = exists int x: x > 1 && f(x) == 0;
}