package pqltest.u;

import static edu.umass.pql.Query;
import static edu.umass.pql.Query.*;
public class ReductionMethodIllTyped4
{
	int i = reduce (sumInt) i: range(1, 10).contains(i);

	public static final long
	sumInt(int i, int d)
	{
		return i;
	}
}