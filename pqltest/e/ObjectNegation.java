package pqltest.u;

import static edu.umass.pql.Query;

public class ObjectNegation
{
	Object[] oa = new Object[] { "foo" };
	boolean b = exists int x: -oa[x] == 0;
}