#! /bin/bash
PQLIB2=../pqlib/pqlib.jar
PQLIB=../${PQLIB2}
JAVA=./jikes/rvm
JUNITLIB="./junit.jar"
CP="${CLASSPATH}:${PQLIB}:${JUNITLIB}"
CP2="${CLASSPATH}:${PQLIB2}:${JUNITLIB}"
PQLC="../dist/bootstrap/bin/javac -printpqlgen -dumppql -cp ${CP}"
JUNIT="${JAVA} -classpath ${CP2} org.junit.runner.JUnitCore"
JAVAFILE=$1
DUMPFILE=`mktemp foo.XXXXXX`
EXPECTED_FILES="${JAVAFILE}.?"
EXPECTEDCOUNT=`ls -l ${EXPECTED_FILES} | wc -l`

if [ ${EXPECTEDCOUNT} -gt 0 ]; then
    echo "Expecting ${EXPECTEDCOUNT} errors"
else
    echo "Found no error specification files!"
    exit 1
fi


if ${PQLC} ${JAVAFILE} 2> ${DUMPFILE}
then
    echo "======= Failed to fail: " ${JAVAFILE}
    cat ${DUMPFILE}
    rm -f ${DUMPFILE} ${DUMPFILE}.*
    exit 1
fi

cat ${DUMPFILE} | awk '/'`echo ${JAVAFILE} | sed 's/\\//\\\\\\//'`'/ { out=1; filenr++; filename="'${DUMPFILE}'" "." filenr; } /^[^^]*\^/ { out = 0 } { if (out) print > filename }'

if [ ! x${DEBUG} == x ]; then
    echo "/====="
    cat ${DUMPFILE}
    echo "\\====="
fi

ACTUAL_FILES="${DUMPFILE}.*"
ACTUALCOUNT=`ls -1 ${ACTUAL_FILES} | wc -l`

echo "Found ${ACTUALCOUNT} errors, comparing..."

ERROR=false
if [ ! ${ACTUALCOUNT} -eq ${EXPECTEDCOUNT} ]; then
    printf "Expected ${EXPECTEDCOUNT} errors but observed ${ACTUALCOUNT}\n" >&2
    ERROR=true
fi

for n in ${EXPECTED_FILES}; do
    FOUND=false
    for m in ${ACTUAL_FILES}; do
	if [ ! ${FOUND} == true ]; then
	    if diff $m $n > /dev/null; then
		rm -f $m
		FOUND=true
		ACTUALCOUNT=$((ACTUALCOUNT - 1))
	    else
		printf ""
	    fi
	fi
    done
    if [ ${FOUND} == false ]; then
	echo "%%% missed expected error:"
	cat $n
	echo ""
	ERROR=true
    fi
done

if [ ! ${ACTUALCOUNT} == 0 ]; then
    for m in ${ACTUAL_FILES}; do
	echo "%%% unexpected error:"
	cat $m
	echo ""
	ERROR=true
    done
fi

rm -f ${DUMPFILE} ${DUMPFILE}.*

if [ ${ERROR} == true ]; then
    exit 1
else
    exit 0
fi