package pqltest.u;
import org.junit.*;
import static edu.umass.pql.Query;
import static org.junit.Assert.*;

public class Equality
{
	@Test
	public void
	testSimpleEquals()
	{
		assertTrue(exists x: (2 = x) && (x = 2));
	}

	// PQL semantics (as of now):
	// * Strings compared with .equals
	// * all else compared with ref equality
	//  - except in containers

	@Test
	public void
	testStringEquals()
	{
		String s = "test";
		String[] objects = new String[] { "foo", "bar", "quux", new String(s) };
		assertTrue(exists x : (objects[x] = s));
	}

	@Test
	public void
	testObjectEquals()
	{
		Object s = new Equality();
		Object[] objects = new Object[] { "foo", "bar", "quux", new Equality(), 42 };
		assertFalse(exists x : (objects[x] = s));
	}

	public boolean
	equals(Object other)
	{
		throw new RuntimeException("Noooo");
	}
}