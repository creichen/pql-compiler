package pqltest.u;
import org.junit.*;
import static edu.umass.pql.Query;
import static org.junit.Assert.*;
import java.util.*;

public class Arithmetic
{
	public int[] ints = new int[] { 1, 2, 3, 4, 5 };
	public long[] longs = new long[] { 1000000000l, 2000000000l };
	public double[] doubles = new double[] { 1.1, 1.2, 1.3 };

	@Test
	public void
	testIntPlus()
	{
		assertTrue(forall x: ints[x] + 1 >= 2);
		assertFalse(forall x: ints[x] + 1 <= 5);
	}

	@Test
	public void
	testLongPlus()
	{
		assertTrue(forall x: longs[x] + 1 > 1000000000l);
	}

	@Test
	public void
	testDoublePlus()
	{
		assertTrue(forall x: doubles[x] + 1.0 > 1.0 );
		assertTrue(forall x: doubles[x] + 1 > 1.0 );
	}


	@Test
	public void
	testIntMinus()
	{
		assertTrue(forall x: ints[x] - 1 >= 0 );
		assertFalse(forall x: ints[x] - 1 <= 3);
	}

	@Test
	public void
	testLongMinus()
	{
		assertTrue(forall x: longs[x] - 1 < 2000000000l);
		assertTrue(forall x: longs[x] - 1 >= 999999999l);
	}

	@Test
	public void
	testDoubleMinus()
	{
		assertTrue(forall x: doubles[x] - 1 > 0);
		assertFalse(forall x: doubles[x] - 1.0 < 0.25);
	}


	@Test
	public void
	testIntTimes()
	{
		assertTrue(forall x: ints[x] * 3 <= 20);
		assertFalse(forall x: ints[x] * ints[x] <= 20);
	}

	@Test
	public void
	testLongTimes()
	{
		assertTrue(forall x: longs[x] * 1 == longs[x] );
		assertTrue(forall x: longs[x] * 0xff > longs[x] );
	}

	@Test
	public void
	testDoubleTimes()
	{
		assertTrue(forall x: doubles[x] * doubles[x] < 2);
		assertTrue(forall x: doubles[x] * doubles[x] > 1);
	}


	@Test
	public void
	testIntDiv()
	{
		assertTrue(forall x: ints[x] / 2 <= 2);
		assertFalse(forall x: 2 / ints[x] < 2);
	}

	@Test
	public void
	testLongDiv()
	{
		assertTrue(forall x: longs[x] / longs[x] == 1);
		assertFalse(forall x: longs[x] / 10 < 10000000l);
	}

	@Test
	public void
	testDoubleDiv()
	{
		assertTrue(forall x: doubles[x] / doubles[x] == 1.0);
		assertFalse(forall x: doubles[x] / 2.0 >= 1.0);
	}


	@Test
	public void
	testIntMod()
	{
		assertTrue(forall x: ints[x] % 2 < 2);
		assertFalse(forall x: ints[x] % 2 <= 0);
	}

	@Test
	public void
	testLongMod()
	{
		assertTrue(forall x: longs[x] % 999999999l <= 5l);
		assertFalse(forall x: longs[x] % 2 >= 2);
	}


	// ----------------------------------------

	@Test
	public void
	testIntNeg()
	{
		assertTrue(forall x : -ints[x] < 0);
	}

	@Test
	public void
	testLongNeg()
	{
		assertTrue(forall x : -longs[x] < 0);
	}

	@Test
	public void
	testDoubleNeg()
	{
		assertTrue(forall x : -doubles[x] < 0);
	}
}
