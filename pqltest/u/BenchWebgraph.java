package pqltest.u;
import org.junit.*;
import static edu.umass.pql.Query;
import static edu.umass.pql.Query.range;
import static org.junit.Assert.*;
import edu.umass.pql.container.PSet;
import java.util.*;


public class BenchWebgraph
{
	@Test
	public void
	test()
	{
		Generator.init();
		assertTrue(compute().equals(computeManual()));
	}

	public Object
	compute()
	{
		Set<Webdoc> documents = Generator.documents;

		Object result = query(Set.contains(Webdoc doc)):
		           documents.contains(doc)
			&& exists link: doc.outlinks.contains(link)
			&& exists link2 : link.destination.outlinks.contains(link2) && link2.destination == doc;
		return result;
	}


	public Object
	computeManual()
	{
		HashSet<Webdoc> results = new HashSet<Webdoc>();

		for (Webdoc doc : Generator.documents) {
			if (results.contains(doc))
				continue;

			for (Link l : doc.outlinks) {
				for (Link l2 : l.destination.outlinks)
					if (l2.destination == doc) {
						results.add(doc);
						results.add(l.destination);
					}
			}
		}
		return results;
	}

	public static class Webdoc
	{
		public Set<Link> outlinks = new HashSet<Link>();
		private static int idc = 0;
		public int id = idc++;
		public int[] words;

		public Webdoc(int id) { }
	}

	public static class Link
	{
		private static int lid = 0;
		public int linkid = lid++;
		public Webdoc source;
		public Webdoc destination;

		public Link(Webdoc s, Webdoc t)
		{
			this.source = s;
			this.destination = t;

			source.outlinks.add(this);
		}
	}

	public static class Generator
	{
		public static int WEBDOCS_NR;
		public static final int WORDS_NR = 1000; // total number of words in the language.  Lower-id words are more frequent than higher-id ones.
		public static int AVG_WORDS_PER_DOC;
		public static int MAX_LINKS;
		public static final int MEAN_LINKS = 5;

		public static Webdoc[] documents_array;
		public static Set<Webdoc> documents;

		static Random rand;

		public static void init()
		{
			WEBDOCS_NR = 10;
			AVG_WORDS_PER_DOC = 20;
			MAX_LINKS = 50;
			documents_array = new Webdoc[WEBDOCS_NR];
			rand = new Random(42);
			documents = genDocuments();
			genLinks();
		}

		public static final int
		nextWord()
		{
			double v = rand.nextGaussian() * WORDS_NR / 4.0;
			if (v < 0) v = -v;
			int word_nr = (int) v;
			if (v >= WORDS_NR)
				v = WORDS_NR - 1;
			return (int) v;
		}

		public static Set<Webdoc>
		genDocuments()
		{
			Set<Webdoc> result = new PSet<Webdoc>();
			for (int i = 0; i < WEBDOCS_NR; i++) {
				Webdoc doc = new Webdoc(i);
				final int len = rand.nextInt(AVG_WORDS_PER_DOC) + rand.nextInt(AVG_WORDS_PER_DOC + 1);
				doc.words = new int[len];
				for (int k = 0; k < len; k++)
					doc.words[k] = nextWord();
				result.add(doc);
				documents_array[i] = doc;
			}
			return result;
		}

		public static void
		genLinks()
		{
			for (int i = 0; i < WEBDOCS_NR; i++) {
				Webdoc doc = documents_array[i];
				if (doc == null)
					throw new NullPointerException();
				int links_nr = (int) ((rand.nextGaussian() * MAX_LINKS / 5.0) + MEAN_LINKS);
				if (links_nr < 0)
					links_nr = -links_nr;
				if (links_nr >= MAX_LINKS)
					links_nr = MAX_LINKS - 1;

				for (int l = 0; l < links_nr; l++)
					new Link(doc, documents_array[rand.nextInt(WEBDOCS_NR)]);
			}
		}

	}

}