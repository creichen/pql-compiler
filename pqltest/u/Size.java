package pqltest.u;
import org.junit.*;
import static edu.umass.pql.Query;
import static edu.umass.pql.Query.*;
import static org.junit.Assert.*;
import java.util.*;

public class Size extends pqltest.u.ContainerGen
{
	@Test
	public void
	arrayLength()
	{
		Set<int[]> arrays = set(new int[] {1}, new int[] {1, 1}, new int[] {1, 1, 1});
		Set<Integer> expected = set(1, 2, 3);
		assertEquals(expected, query(Set.contains(byte l)): exists s: arrays.contains(s) && l == s.length);
	}

	@Test
	public void
	arraySize()
	{
		Set<int[]> arrays = set(new int[] {1}, new int[] {1, 1}, new int[] {1, 1, 1});
		Set<Integer> expected = set(1, 2, 3);
		assertEquals(expected, query(Set.contains(byte l)): exists s: arrays.contains(s) && l == s.size());
	}

	@Test
	public void
	setSize()
	{
		Set<Set<Integer>> sets = set(set(1), set(1, 2), set(1, 2, 3));
		Set<Integer> expected = set(1, 2, 3);
		assertEquals(expected, query(Set.contains(byte l)): exists s: sets.contains(s) && l == s.size());
	}

	@Test
	public void
	setLength()
	{
		Set<Set<Integer>> sets = set(set(1), set(1, 2), set(1, 2, 3));
		Set<Integer> expected = set(1, 2, 3);
		assertEquals(expected, query(Set.contains(byte l)): exists s: sets.contains(s) && l == s.length);
	}

	@Test
	public void
	mapSize()
	{
		Set<Map<Integer, Integer>> sets = set(map(1, 0), map(1, 1, 2, 2), map(1, 2, 2, 3, 3, 4));
		Set<Integer> expected = set(1, 2, 3);
		assertEquals(expected, query(Set.contains(byte l)): exists s: sets.contains(s) && l == s.size());
	}

	@Test
	public void
	multiSize()
	{
		Set<int[]> arrays = set(new int[] {1}, new int[] {1, 1}, new int[] {1, 1, 1});
		Set<Set<Integer>> sets = set(set(1, 2, 3, 4, 5), set(1, 2, 3, 4, 5, 6), set(1, 2, 3, 4, 5, 6, 7));
		Set<Integer> expected = set(1, 2, 3, 5, 6, 7);
		assertEquals(expected, query(Set.contains(byte l)): exists s: (sets.contains(s) || arrays.contains(s)) && l == s.size());
	}
}