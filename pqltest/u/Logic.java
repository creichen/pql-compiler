package pqltest.u;
import org.junit.*;
import static edu.umass.pql.Query;
import static org.junit.Assert.*;
import java.util.*;

public class Logic
{
	int[] data = new int[] { 1, 2, 3, 4, 5 };

	@Test
	public void
	testTrue()
	{
		assertTrue(exists x: x == 1 && true);
	}

	@Test
	public void
	testFalse()
	{
		assertFalse(exists x: x == 1 && false);
	}

	@Test
	public void
	testConjunctionNegLhs()
	{
		assertFalse(exists x: data[x] < 0 && true);
	}

	@Test
	public void
	testConjunctionNegRhs()
	{
		assertFalse(exists x: true && data[x] < 0);
	}

	@Test
	public void
	testConjunctionTrue()
	{
		assertTrue(exists x: data[x] > 1 && data[x] > 0);
	}

	@Test
	public void
	testImplicationPremise()
	{
		assertTrue(forall x: (data[x] > 10) -> data[x] == 42);
	}

	@Test
	public void
	testImplicationConclusion()
	{
		assertTrue(forall x: data[x] > 4 -> data[x] == 5);
	}

	@Test
	public void
	testNegImplication()
	{
		assertTrue(forall x: data[x] > 5 -> data[x] > 10);
	}

	@Test
	public void
	testIffTrueSuccess()
	{
		assertTrue(forall x: (data[x] > 0) == (data[x] > -10));
	}

	@Test
	public void
	testIffFalseSuccess()
	{
		assertTrue(forall x: (data[x] > 10) == (data[x] > 20));
	}

	@Test
	public void
	testIffMismatch()
	{
		assertFalse(forall x: (data[x] > 3) == (data[x] > 20));
	}

	@Test
	public void
	testIffMismatchSymmetry()
	{
		assertFalse(forall x: (data[x] > 20) == (data[x] > 3));
	}

	// -- negation

	@Test
	public void
	testNegatedtrue()
	{
		assertFalse(exists x: !(x == 1 && true));
	}

	@Test
	public void
	testNegatedFalse()
	{
		assertTrue(exists x: !(x == 1 && false));
	}

	@Test
	public void
	testNegNegProperty()
	{
		assertTrue(forall x : !(!(data[x] > 0)));
		assertFalse(forall x : !(data[x] > 0));
		assertFalse(forall x : !(!(!(data[x] > 0))));
	}

	// -- disjunction

	// @Test
	// public void
	// testDisjunctionAlpha()
	// {
	// 	assertTrue(forall x: ((data[x] & 1) == 0 || ((data[x] & 1) == 1)));
	// }

	@Test
	public void
	testDisjunctionBeta()
	{
		int[] a = new int[] { 1, 2, 3, 4 };
		int[] b = new int[] { 7, 8, 9, 10 };
		int[] c = new int[] { 1, 2, -1, 4 };

		assertFalse(forall byte x: ((exists y: a[y] == x) || (exists y: b[y] == x) || (exists y: c[y] == x)) -> x > 0);
		assertFalse(forall byte x: ((exists y: a[y] == x) || (exists y: c[y] == x) || (exists y: b[y] == x)) -> x > 0);
		assertFalse(forall byte x: ((exists y: c[y] == x) || (exists y: b[y] == x) || (exists y: a[y] == x)) -> x > 0);
		assertTrue(forall byte x: ((exists y: a[y] == x) || (exists y: b[y] == x) || (exists y: data[y] == x)) -> x > 0);
	}
}
