package pqltest.u;
import org.junit.*;
import static edu.umass.pql.Query;
import static edu.umass.pql.Query.*;
import static org.junit.Assert.*;
import java.util.*;

public class ContainerConstruction extends ContainerGen
{
	@Test
	public void
	testSingleton()
	{
		Set<Integer> set = query(Set.contains(short x)): x == 1;
		System.err.println("set = " + set);
		assertEquals(1, set.size());
		assertTrue(set.contains((short) 1));
	}

	@Test
	public void
	testSquares()
	{
		Set<Integer> set = query(Set.contains(int x)): exists byte y: x == y * y;

		for (int i = 0; i < 10; i++)
			assertTrue(set.contains(i * i));
		System.err.println(set);
		assertFalse(set.contains(5));

		for (int e : set) {
			int i = (int) Math.round(Math.sqrt(e));
			assertEquals(e, i * i);
		}
	}

	@Test
	public void
	testSquares2()
	{
		Set<Integer> set = query(Set.contains(int x)): exists y: x == y * y && range(1, 100).contains(y);

		System.err.println("squares: " + set);

		for (int i = 1; i < 10; i++)
			assertTrue(set.contains(i * i));
		System.err.println(set);
		assertFalse(set.contains(5));

		for (int e : set) {
			int i = (int) Math.round(Math.sqrt(e));
			assertEquals(e, i * i);
		}
	}

	@Test
	public void
	testSquareMap()
	{
		Map<Integer, Integer> expected = map(1, 1,
						     2, 4,
						     3, 9,
						     4, 16);

		assertEquals(expected,
			     query(Map.get(k) = v): range(1, 42).contains(v) && range(1, 4).contains(k) && v == k*k);
	}

	@Test
	public void
	testSimpleMap()
	{
		Set<Integer> s = set(1, 3, 5, 7, 9);

		Map<Integer, Integer> map = query(Map.get(k) = v): range(1, 10).contains(k) && s.contains(v) -> v == k*k;
		System.err.println("m: " + map);
		assertEquals(1, (int) map.get(1));
		assertEquals(9, (int) map.get(3));
		assertEquals(2, map.size());
	}

	@Test
	public void
	testMapMismatch()
	{
		Set<Integer> s = set(1, 3, 5, -3, 9);

		try {
			Map<Integer, Integer> map = query(Map.get(int k) = int v): s.contains(v) && k == v*v;
		} catch (edu.umass.pql.AmbiguousMapKeyException _) { return; }
		fail();
	}

	@Test
	public void
	testDefaultMap()
	{
		Set<Integer> s = set(1, 3, 5, 7, 9);

		Map<Integer, Integer> map = query(Map.get(k) = v default 0): range(1, 10).contains(k) && s.contains(v) -> v == k*k;
		assertEquals(0, (int) map.get(2));
		assertEquals(0, (int) map.get(4));
		assertEquals(1, (int) map.get(1));
		assertEquals(9, (int) map.get(3));
	}

	@Test
	public void
	testTypeFilter()
	{
		Set<Object> s = set((Object)1, (Object)2, "foo", (Object)3, "bar");
		Set<Object> expected = set((Object)1, (Object)2, (Object)3);
		Set<Object> sf = query(Set.contains(x)): s.contains(x) && x instanceof Integer;
		assertEquals(expected, sf);
	}

	// @Test
	// public void
	// testDefaultMap2()
	// {
	// 	Set<Integer> s = set(1, 3, 5, 7, 9);

	// 	Map<Integer, Map<Integer, Integer>> map = query(Map.get(int r) = Map<Integer, Integer> m) :
	// 	         range(1, 10).contains(r)
	// 			-> (m == query(Map.get(k) = v default r + 1): range(0, 11).contains(k) && s.contains(v) -> v == k*k);

	// 	System.err.println("Result map = " + map);
	// 	assertEquals(10, map.size());
	// 	for (int i = 1; i <= 10; i++) {
	// 		Map<Integer, Integer> smap = map.get(i);

	// 		assertEquals(1, (int) smap.get(1));
	// 		assertEquals(i+1, (int) smap.get(2));
	// 		assertEquals(9, (int) smap.get(3));
	// 		assertEquals(i+1, (int) smap.get(4));
	// 	}
	// }

	// @Test
	// public void
	// testDefaultMap3()
	// {
	// 	Set<Integer> s = set(1, 3, 5, 7, 9);

	// 	Map<Integer, Map<Integer, Integer>> map = query(Map.get(r) = Map<Integer, Integer> m) : range(1, 10).contains(r)
	// 		&& m == query(Map.get(k) = v default r + 1): range(0, 11).contains(k) && s.contains(v) -> v == k*k;

	// 	assertEquals(10, map.size());
	// 	for (int i = 1; i <= 10; i++) {
	// 		Map<Integer, Integer> smap = map.get(i);

	// 		assertEquals(1, (int) smap.get(1));
	// 		assertEquals(i+1, (int) smap.get(2));
	// 		assertEquals(9, (int) smap.get(3));
	// 		assertEquals(i+1, (int) smap.get(4));
	// 	}
	// }

	// @Test
	// public void
	// testDefaultMap4()
	// {
	// 	Set<Integer> s = set(1, 3, 5, 7, 9);

	// 	Map<Integer, Map<Integer, Integer>> map = query(Map.get(byte r) = Map<Integer, Integer> m) : range(1, 10).contains(r)
	// 		&& m == query(Map.get(k) = v default r + 1): range(0, 11).contains(k) && s.contains(v) -> v == k*k;

	// 	assertEquals(10, map.size());
	// 	for (int i = 1; i <= 10; i++) {
	// 		Map<Integer, Integer> smap = map.get(i);

	// 		assertEquals(1, (int) smap.get(1));
	// 		assertEquals(i+1, (int) smap.get(2));
	// 		assertEquals(9, (int) smap.get(3));
	// 		assertEquals(i+1, (int) smap.get(4));
	// 	}
	// }
}
