package pqltest.u;
import org.junit.*;
import static edu.umass.pql.Query;
import static org.junit.Assert.*;
import static edu.umass.pql.Query.sumDouble;
import edu.umass.pql.container.PSet;
import java.util.*;


public class BenchBonus
{
	@Test
	public void
	test()
	{
		Generator.init();
		final Object p_result = compute();
		final Object m_result = computeManual();
		if (!p_result.equals(m_result)) {
			System.err.println("pql:    " + p_result);
			System.err.println("manual: " + m_result);
		}
		assertTrue(p_result.equals(m_result));
	}

	public Object
	compute()
	{
		Set<Employee> employees = Generator.employees;

		System.err.println("threads = " + edu.umass.pql.Env.THREADS_NR + " in mode = " + edu.umass.pql.PQLFactory.getParallelisationMode());
		Object result = query(Map.get(employee) == double bonus):
		           employees.contains(employee)
				   && bonus ==
				       employee.dept.bonus_factor
				       * (reduce(sumDouble) v:
					  exists Bonus b: employee.bonusSet.contains(b)
					  && v == b.bonus_base);
		return result;
	}

	public Object
	computeManual()
	{
		final HashMap<Employee, Double> bonus_map = new HashMap<Employee, Double>();
		for (Employee employee : Generator.employees) {
			final double dept_bonus_factor = employee.dept.bonus_factor;
			double total_bonus = 0.0;
			for (Bonus b : employee.bonusSet)
				total_bonus += b.bonus_base;
			bonus_map.put(employee, total_bonus * dept_bonus_factor);
		}
		return bonus_map;
	}

	public static class Employee
	{
		private static int idc = 0;
		public int id = idc++;
		public Department dept;
		public Set<Bonus> bonusSet;

		public Employee(Department dept)
		{
			this.dept = dept;
			bonusSet = new HashSet<Bonus>();
		}

		@Override
		public int
		hashCode()
		{
			return this.id;
		}
	}

	public static class Bonus
	{
		public String bonus_descr;
		public double bonus_base;
		public Bonus(String descr, double base)
		{
			this.bonus_descr = descr;
			this.bonus_base = base;
		}
	}

	public static class Department
	{
		public int id;
		public double bonus_factor;
		public Department(int id)
		{
			this.id = id;
		}
	}


	public static class Generator
	{
		// join employee + department tables, compute employee bonuses
		//  emp-table: (emp-id, dept-id, bonus-descr, bonus-value)
		//  dept-table: (dept-id, bonus-factor)
		//  (Sum and multiply boni)

		public static int EMPLOYEES_NR;
		public static final int DEPARTMENTS_NR = 10;
		public static final int AVG_BONI = 3;

		public static Set<Employee> employees;
		public static Employee[] employees_array;
		public static Department[] departments;

		static Random rand = null;
		static int bonusid;

		public static final void init()
		{
			EMPLOYEES_NR = 32; // power of 2
			bonusid = 0;
			departments = genDepartments();
			employees = genEmployees();
		}

		public static Department[]
		genDepartments()
		{
			Random rand = new Random(42);
			Department[] result = new Department[DEPARTMENTS_NR];
			for (int i = 0; i < result.length; i++) {
				result[i] = new Department(i);
			if (result[i].id != i)
				throw new RuntimeException();
			result[i].bonus_factor = 0.5 + (rand.nextInt(16) * 0.1);
			}
			return result;
		}

		public static Set<Employee>
		genEmployees()
		{
			Random rand = new Random(42);
			Set<Employee> result = new PSet<Employee>();
			employees_array = new Employee[EMPLOYEES_NR];
			for (int i = 0; i < EMPLOYEES_NR; i++) {
				Employee e = new Employee(departments[rand.nextInt(DEPARTMENTS_NR)]);
				employees_array[i] = e;

				if (e.id != i)
					throw new RuntimeException();
				result.add(e);
				int boni = rand.nextInt(AVG_BONI) + rand.nextInt(AVG_BONI + 1);
				while (boni-- > 0) {
				Bonus b = new Bonus("Bonus "+(bonusid++), rand.nextInt(100) * 100);
				e.bonusSet.add(b);
				}
			}
			return result;
		}
	}
}