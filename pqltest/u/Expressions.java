package pqltest.u;
import org.junit.*;
import static edu.umass.pql.Query;
import static edu.umass.pql.Query.range;
import static org.junit.Assert.*;
import java.util.*;

public class Expressions extends ContainerGen
{
	@Ignore
	@Test
	public void
	testAbsolute0()
	{
		Set<Integer> input = set(1, -2, 3, -4, 5, -6);
		Set<Integer> expected = set(1, 2, 3, 4, 5, 6);
		Set<Integer> output = query(Set.contains(byte i)): exists v: input.contains(v) && (v < 0? i == -v : i == v);
		assertEquals(expected, output);
	}

	@Test
	public void
	testAbsolute1()
	{
		Set<Integer> input = set(1, -2, 3, -4, 5, -6);
		Set<Integer> expected = set(1, 2, 3, 4, 5, 6);
		Set<Integer> output = query(Set.contains(byte i)): exists v: input.contains(v) && i == (v < 0? -v : v);
		assertEquals(expected, output);
	}


	@Test
	public void
	filter()
	{
		Set<String> expected = set("foo", "bar");
		Set<Object> input = set(1, "foo", new Object(), "bar", 42.0, true);

		Set<Object> result = query(Set.contains(v)): input.contains(v) && v instanceof String;

		assertEquals(expected, result);
	}
}