package pqltest.u;
import org.junit.*;
import static edu.umass.pql.Query;
import static org.junit.Assert.*;
import java.util.*;

/**
 * Tests the type-to-expr conversion in the PQL optimiser
 */
public class TypeToExprTest
{
	public int[] ints = new int[] { 1, 2, 3, 4, 5 };
	public Object[] objects = new Object[] { null, null, null };

	public static int
	f0(byte v)
	{
		return 1;
	}

	@Test
	public void
	testConstant0()
	{
		byte myvar = 1;
		assertTrue(forall x: ints[x] + f0(myvar) >= 2);
	}

	// ----------------------------------------

	public static int[]
	f1()
	{
		return null;
	}

	@Test
	public void
	testConstant1()
	{
		assertTrue(forall x: exists y: objects[x] == y && y == null && y == f1());
	}

	// ----------------------------------------

	public static Map.Entry
	f2()
	{
		return null;
	}

	@Test
	public void
	testConstant2()
	{
		assertTrue(forall x: exists y: objects[x] == y && y == null && y == f2());
	}

	// ----------------------------------------

	public static Map.Entry<String, Boolean>
	f3()
	{
		return null;
	}

	@Test
	public void
	testConstant3()
	{
		assertTrue(forall x: exists y: objects[x] == y && y == null && y == f3());
	}

	// ----------------------------------------

	public static <A> Map.Entry<? extends String, A>
	f4()
	{
		return null;
	}

	@Test
	public void
	testConstant4()
	{
		assertTrue(forall x: exists y: objects[x] == y && y == null && y == f4());
	}

	// ----------------------------------------

	public static <A extends Set> Map.Entry<?, A>
	f5()
	{
		return null;
	}

	@Test
	public void
	testConstant5()
	{
		assertTrue(forall x: exists y: objects[x] == y && y == null && y == f5());
	}

	// ----------------------------------------

	public static <A> A
	f6()
	{
		return null;
	}

	@Test
	public <A> void
	testConstant6()
	{
		assertTrue(forall x: exists y: objects[x] == y && y == null && y == f6());
	}
}
