package pqltest.u;
import org.junit.*;
import static edu.umass.pql.Query;
import static org.junit.Assert.*;
import java.util.*;

public class Bitops
{
	public int[] ints = new int[] { 1, 2, 3, 4, 5 };
	public long[] longs = new long[] { 0x100000010l, 0x200000010l };

	public int[] signed_ints = new int[] { -3, 3, -2, 2 };
	public long[] signed_longs = new long[] { 0x100000000l, -0x100000000l,
						  0x200000000l, -0x200000000l };

	@Test
	public void
	testIntOr()
	{
		assertTrue(forall x: (ints[x] | 0x10) > 0x10);
		assertFalse(forall x: (ints[x] | 0x3) == 0x3 );
	}

	@Test
	public void
	testLongOr()
	{
		assertTrue(forall x: (longs[x] | 0x10) == longs[x]);
		assertFalse(forall x: (longs[x] | 0x01) == longs[x]);
	}

	@Test
	public void
	testIntAnd()
	{
		assertTrue(forall x: (ints[x] & 0x8) == 0);
		assertFalse(forall x: (ints[x] & 0x4) == 0x4);
	}

	@Test
	public void
	testLongAnd()
	{
		assertTrue(forall x: (longs[x] & 0x10) == 0x10 );
		assertFalse(forall x: (longs[x] & 0x8) != 0 );
	}

	@Test
	public void
	testIntXor()
	{
		assertTrue(forall x: (ints[x] ^ 0x5) <= 0x7);
		assertTrue(forall x: (ints[x] ^ 0x8) > 0x8);
		assertFalse(forall x: (ints[x] ^ 0x1) == ints[x]);
	}

	@Test
	public void
	testLongXor()
	{
		assertTrue(forall x: (longs[x] ^ 0x10) < longs[x]);
		assertFalse(forall x: (longs[x] ^ 0x10) < 0x10 );
	}

	@Test
	public void
	testIntShl()
	{
		assertTrue(forall x: (1 << ints[x]) >= 2);
		assertFalse(exists x: ((ints[x] << 1) & 0x1) != 0);
	}

	@Test
	public void
	testLongShl()
	{
		assertTrue(forall x: (longs[x] << 2) > 0 );
		assertFalse(exists x: (longs[x] << 10) < longs[x] );
	}

	@Test
	public void
	testIntShr()
	{
		assertTrue(forall x: (signed_ints[x] >> 1) != 0);
		assertFalse(forall x: (signed_ints[x] >> 1) > 0);
		assertFalse(exists x: (1 >> ints[x]) != 0);
	}

	@Test
	public void
	testLongShr()
	{
		assertTrue(forall x: (signed_longs[x] >> 1) != 0);
		assertFalse(forall x: (signed_longs[x] >> 1) > 0);
	}

	@Test
	public void
	testIntUshr()
	{
		assertTrue(forall x: (signed_ints[x] >>> 1) > 0);
		assertFalse(exists x: (signed_ints[x] >>> 1) == 0 );
	}

	@Test
	public void
	testLongUshr()
	{
		assertTrue(forall x: (signed_longs[x] >>> 1) > 0);
		assertFalse(exists x: (signed_longs[x] >>> 32) == 0 );
	}

	// ----------------------------------------

	@Test
	public void
	testIntInv()
	{
		assertTrue(forall x : ~ints[x] < 0);
		assertTrue(exists x : ~ints[x] < 0 && (~x == -1));
	}

	@Test
	public void
	testLongInv()
	{
		assertTrue(forall x : ~longs[x] < 0);
		assertTrue(exists x : ~longs[x] < 0 && (~x == -1));
	}
}