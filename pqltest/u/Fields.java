package pqltest.u;
import org.junit.*;
import static edu.umass.pql.Query;
import static org.junit.Assert.*;
import java.util.*;

public class Fields
{
	@SuppressWarnings("UncheckedCast")
	<T>
	Set<T> set(T... args)
	{
		Set<T> set = (Set<T>) new HashSet<Object>();
		for (T t : args)
			set.add(t);
		return set;
	}

	// @Test
	// public void
	// testAlphaHeap()
	// {
	// 	Set s = set(new Alpha(4), new Alpha(5), new Alpha(7));

	// 	assertFalse(exists Alpha a : a.x == 5);

	// 	Alpha a = new Alpha(5);

	// 	assertTrue(exists Alpha a : a.x == 5);
	// }

	@Test
	public void
	testAlphaSimpleTrue()
	{
		Alpha[] a = new Alpha[] { new Alpha(2),
					  new Alpha(3),
					  new Alpha(5),
					  new Alpha(7) };

		assertTrue(exists x: a[x].x == 5);
	}

	@Test
	public void
	testAlphaChainedTrue()
	{
		Alpha[] a = new Alpha[] { new Alpha(2),
					  new Alpha(3),
					  new Alpha(5),
					  new Alpha(7) };

		assertTrue(forall i: a[i].self.self == a[i]);
	}

	@Test
	public void
	testAlphaSimpleFalse()
	{
		Alpha[] a = new Alpha[] { new Alpha(2),
					  new Alpha(3),
					  new Alpha(5),
					  new Alpha(7) };

		assertFalse(exists x: a[x].x == 19);
	}

	public static class Alpha
	{
		public int x;
		public Alpha self;

		public Alpha(int v)
		{
			this.x = v;
			this.self = this;
		}
	}


	// public static class Beta
	// {
	// 	public String s;

	// 	public Beta(String s)
	// 	{
	// 		this.s = s;
	// 	}
	// }

	// public static class Gamma
	// {
	// 	public Alpha a;
	// 	public int z;

	// 	public Gamma(Alpha a, int z)
	// 	{
	// 		this.a = a;
	// 		this.z = z;
	// 	} 
	// }
}