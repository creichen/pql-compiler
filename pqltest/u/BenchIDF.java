package pqltest.u;
import org.junit.*;
import static edu.umass.pql.Query;
import static edu.umass.pql.Query.sumInt;
import static edu.umass.pql.Query.range;
import static org.junit.Assert.*;
import edu.umass.pql.container.PSet;
import edu.umass.pql.container.PDefaultMap;
import java.util.*;


public class BenchIDF
{
	@Test
	public void
	test()
	{
		Generator.init();
		final Object p_result = compute();
		final Object m_result = computeManual();
		if (!p_result.equals(m_result)) {
			System.err.println("pql:    " + p_result);
			System.err.println("manual: " + m_result);
		}
		assertTrue(p_result.equals(m_result));
	}

	public Object
	compute()
	{
		Set<Webdoc> documents = Generator.documents;
		int WORDS_NR = Generator.WORDS_NR;

		Object result = query(Map.get(int word_id) == int idf default 0):
		           // The explicit `range' here is required due to range inference.  This also slows down
		           // query execution, unfortunately.  While the IL would allow us the `efficient' version,
		           // the source language does not support this feature yet.
		           // range(0, WORDS_NR).contains(word_id)
			   // &&
		           idf == reduce(sumInt) one over doc: one == 1
                                                  && documents.contains(doc)
			                          && exists i: doc.words[i] == word_id;
		return result;
	}

	public Object
	computeManual()
	{
		int[] counts = new int[Generator.WORDS_NR];

		for (Webdoc doc : Generator.documents_array) {
			for (int word_id : doc.words) {
				++counts[word_id];
			}
		}

		Map<Integer, Integer> results = new PDefaultMap<Integer, Integer>(0);
		for (int i = 0; i < Generator.WORDS_NR; i++)
			if (counts[i] > 0)
				results.put(i, counts[i]);

		return results;
	}

	public static class Webdoc
	{
		public Set<Link> outlinks = new HashSet<Link>();
		private static int idc = 0;
		public int id = idc++;
		public int[] words;

		public Webdoc(int id) { }
	}

	public static class Link
	{
		private static int lid = 0;
		public int linkid = lid++;
		public Webdoc source;
		public Webdoc destination;

		public Link(Webdoc s, Webdoc t)
		{
			this.source = s;
			this.destination = t;

			source.outlinks.add(this);
		}
	}

	public static class Generator
	{
		public static int WEBDOCS_NR;
		public static final int WORDS_BASE = 1000; // `zeroeth' word has this ID
		public static final int WORDS_COUNT = 10; // total number of words in the language.  Lower-id words are more frequent than higher-id ones.
		public static final int WORDS_NR = WORDS_BASE + WORDS_COUNT;
		public static int AVG_WORDS_PER_DOC;
		public static int MAX_LINKS;
		public static final int MEAN_LINKS = 5;

		public static Webdoc[] documents_array;
		public static Set<Webdoc> documents;

		static Random rand;

		public static void init()
		{
			WEBDOCS_NR = 10;
			AVG_WORDS_PER_DOC = 40;
			MAX_LINKS = 50;
			documents_array = new Webdoc[WEBDOCS_NR];
			rand = new Random(42);
			documents = genDocuments();
			genLinks();
		}

		public static final int
		nextWord()
		{
			double v = rand.nextGaussian() * WORDS_COUNT / 4.0;
			if (v < 0) v = -v;
			int word_nr = (int) v;
			if (v >= WORDS_COUNT)
				v = WORDS_COUNT - 1;
			return ((int) v) + WORDS_BASE;
		}

		public static Set<Webdoc>
		genDocuments()
		{
			Set<Webdoc> result = new PSet<Webdoc>();
			for (int i = 0; i < WEBDOCS_NR; i++) {
				Webdoc doc = new Webdoc(i);
				final int len = rand.nextInt(AVG_WORDS_PER_DOC) + rand.nextInt(AVG_WORDS_PER_DOC + 1);
				doc.words = new int[len];
				for (int k = 0; k < len; k++)
					doc.words[k] = nextWord();
				result.add(doc);
				documents_array[i] = doc;
			}
			return result;
		}

		public static void
		genLinks()
		{
			for (int i = 0; i < WEBDOCS_NR; i++) {
				Webdoc doc = documents_array[i];
				if (doc == null)
					throw new NullPointerException();
				int links_nr = (int) ((rand.nextGaussian() * MAX_LINKS / 5.0) + MEAN_LINKS);
				if (links_nr < 0)
					links_nr = -links_nr;
				if (links_nr >= MAX_LINKS)
					links_nr = MAX_LINKS - 1;

				for (int l = 0; l < links_nr; l++)
					new Link(doc, documents_array[rand.nextInt(WEBDOCS_NR)]);
			}
		}

	}

}