package pqltest.u;
import org.junit.*;
import static edu.umass.pql.Query;
import static org.junit.Assert.*;
import java.util.*;

public class MapSyntaxExtension
{
	@SuppressWarnings("UncheckedCast")
	<T>
	Set<T> set(T... args)
	{
		Set<T> set = (Set<T>) new HashSet<Object>();
		for (T t : args)
			set.add(t);
		return set;
	}

	@SuppressWarnings("UncheckedCast")
	<T>
	Map<T, T> map(T... args)
	{
		Map<T, T> map = (Map<T, T>) new HashMap<Object, Object>();
		for (int i = 0; i < args.length; i+= 2)
			map.put(args[i], args[i+1]);
		return map;
	}

	// ================================================================================

	@Test
	public void
	testArrayGet()
	{
		Set<int[]> s = set(new int[0], new int[] {1, 3, 5, 7, 9, 22}, new int[] { 2, 17 });

		assertTrue(forall a: s.contains(a) && forall x: a.get(x) > 0);
	}

	@Test
	public void
	testMapBrackets()
	{
		Map<Integer, Integer> map = map(1, 2,
						2, 1,
						100, 42,
						42, 100,
						-17, 124,
						124, -17);

		assertTrue(forall k: exists v: map.get(k) == v -> map[v] == k);
	}

	@Test
	public void
	testPolyMap() // In this rare case, `s' can be an array OR a map.
	{
		Set<int[]> s0 = set(new int[0], new int[] {1, 3, 5, 7, 9, 22}, new int[] { 2, 17 });
		Set<Map<Integer, Integer>> s1 = set(map(1, 2, 2, 3, 3, 4));

		assertTrue(forall s: (s0.contains(s) || s1.contains(s)) -> forall x: s.get(x) > 0);
	}
}