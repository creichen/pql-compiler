package pqltest.u;
import org.junit.*;
import static edu.umass.pql.Query;
import static org.junit.Assert.*;

public class A
{
	@Test
	public void
	test0()
	{
		boolean data[] = new boolean[] { true, true };

		assertTrue(forall x : data[x]);
	}

	// @Test
	// public void
	// testX()
	// {
	// 	int[] array = new int[] { 1, 2, 3 };
	// 	assertTrue(exists x: array[x] == 3 && array[x-1] == 2);
	// }

	// boolean b[] = new boolean[] { true, true };
	// boolean all = forall x: b[x];
	// Object dummy = System.getenv("PATH");

	// @Test
	// public void
	// testA()
	// {
	// 	assertTrue(all);
	// }

	// @Test
	// public void
	// testB()
	// {
	// 	int data[] = new int[] { 1, 2, 3 };

	// 	assertFalse(exists d : d == data && d.length > 1 && forall x: data[x] < 3);
	// }

	// @Test
	// public void
	// testB2()
	// {
	// 	int data[] = new int[] { 1, 2, 3 };

	// 	assertTrue(forall x: data[x] < 4);
	// }

	// @Test
	// public void
	// testC()
	// {
	// 	Object data[] = new Object[] { null, new A(), 7 };
	// 	assertFalse(forall x: data[x] == null);
	// }

	// @Test
	// public void
	// testD()
	// {
	// 	assertFalse(exists x: 2 == x && x == 3);
	// }

	// @Test
	// public void
	// testE()
	// {
	// 	int a[] = new int[] { 1, 2, 3 };
	// 	boolean b[] = new boolean[] { true, true };
	// 	assertTrue(forall x : (a[x] > 0 && forall y : b[y]));
	// }

	// // @Test public void zero() {}

}