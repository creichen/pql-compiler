package pqltest.u;
import org.junit.*;
import java.util.*;
import edu.umass.pql.container.PSet;
import edu.umass.pql.container.PMap;

public class ContainerGen
{
	@Test
	public void zero() {}

	static boolean set_gen_count = false;
	static boolean map_gen_count = false;

	public static
	@SuppressWarnings("UncheckedCast")
	<T>
	Set<T> pset(T... args)
	{
		Set<T> set;
		set = (Set<T>) new PSet<Object>();
		
		for (T t : args)
			set.add(t);
		return set;
	}

	public static
	@SuppressWarnings("UncheckedCast")
	<T>
	Set<T> hashset(T... args)
	{
		Set<T> set;
		set = (Set<T>) new HashSet<Object>();
		
		for (T t : args)
			set.add(t);
		return set;
	}

	public static
	@SuppressWarnings("UncheckedCast")
	<T>
	Set<T> set(T... args)
	{
		
		Set<T> retval;

		if (set_gen_count)
			retval = pset(args);
		else
			retval = hashset(args);
		set_gen_count = !set_gen_count;

		return retval;
	}


	public static
	@SuppressWarnings("UncheckedCast")
	<T>
	Map<T, T> map(T... args)
	{
		Map<T, T> map;
		if (map_gen_count)
			map = (Map<T, T>)(new HashMap<Object, Object>());
		else
			map = (Map<T, T>)(new PMap<Object, Object>());

		map_gen_count = !map_gen_count;

		for (int i = 0; i < args.length; i+= 2)
			map.put(args[i], args[i+1]);
		return map;
	}
}