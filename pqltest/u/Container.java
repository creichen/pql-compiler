package pqltest.u;
import org.junit.*;
import static edu.umass.pql.Query;
import static edu.umass.pql.Query.range;
import static org.junit.Assert.*;
import java.util.*;

public class Container extends ContainerGen
{
	@Test
	public void
	testSetProp()
	{
		Set<Integer> a = set(1, 3, 5, 7, 9, 22);

		assertTrue(forall x: a.contains(x) && x > 0);
	}

	@Test
	public void
	testArraySetProp()
	{
		Set<int[]> s = set(new int[0], new int[] {1, 3, 5, 7, 9, 22}, new int[] { 2, 17 });

		assertTrue(forall a: s.contains(a) && forall x: a[x] > 0);
	}

	@Test
	public void
	testContainment()
	{
		Set<Integer> a = set(1, 3, 5, 7, 9, 22);
		Set<Integer> b = set(1, 3, 5, 7, 9, 22);

		assertTrue(forall x: a.contains(x) == b.contains(x));
	}

	@Test
	public void
	testInterestingContainment()
	{
		Set<Integer> a = set(1, 3, 5);
		Set<Integer> b = set(2, 6, 10);
		java.util.Collection<Integer> a2 = a;
		java.util.Collection<Integer> b2 = b;

		assertTrue(forall x: a2.contains(x) -> b2.contains(x * 2));
	}


	@Test
	public void
	testMapCyclicity()
	{
		Map<Integer, Integer> map = map(1, 2,
						2, 1,
						100, 42,
						42, 100,
						-17, 124,
						124, -17);

		assertTrue(forall k: exists v: map.get(k) == v -> map.get(v) == k);
	}

	@Test
	public void
	testMapAcyclicity()
	{
		Map<Integer, Integer> map = map(1, 2,
						2, 1,
						100, 42,
						42, 100,
						-17, 1024,
						1024, -99917);

		assertFalse(forall k: exists v: map.get(k) == v && map.get(v) == k);
	}

	@Test
	public void
	testMapKeySet()
	{
		Map<Integer, Integer> map = map(1, 2,
						3, 4,
						5, 6,
						7, 8,
						17, 23);
		Set<Integer> keyset = set(1, 3, 5, 7, 17);

		assertEquals(keyset, query(Set.contains(k)): exists byte v:  map.get(k) == v); 
	}

	@Test
	public void
	testMapValueSet()
	{
		Map<Integer, Integer> map = map(1, 2,
						3, 4,
						5, 6,
						7, 8,
						17, 23);
		Set<Integer> valueset = set(2, 4, 6, 8, 23);

		assertEquals(valueset, query(Set.contains(byte v)): exists k:  map.get(k) == v); 
	}

	// ----------------------------------------

	// @Test
	// public void
	// testConstructedRange()
	// {
	// 	assertEquals(set(5, 6, 7, 8),
	// 		     query(Set.contains(v)): range(5, 8).contains(v));
	// }

	// @Test
	// public void
	// testManyConstructedRanges()
	// {
	// 	Set<Integer> s = set(0, 10, 21);

	// 	assertFalse(exists a : s.contains(a) && range(a, a+9).contains(20));
	// 	assertTrue(exists a : s.contains(a) && range(a, a+10).contains(20));
	// }

	// @Test
	// public void
	// testRangeSetConstruction()
	// {
	// 	assertEquals(set(set(1), set(1, 2), set(1, 2, 3)),
	// 		     query(Set.contains(/*Set<Integer>*/ v)): exists max: range(1, 3).contains(max) -> v == range(1, max));
	// }


}