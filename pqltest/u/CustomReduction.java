package pqltest.u;
import org.junit.*;
import static edu.umass.pql.Query;
import edu.umass.pql.DefaultValueLong;
import edu.umass.pql.DefaultValueDouble;
import edu.umass.pql.DefaultValueInt;
import static edu.umass.pql.Query.range;
import static org.junit.Assert.*;
import java.util.*;

public class CustomReduction extends ContainerGen
{
	public static String
	longestString(String a, String b)
	{
		if (a == null)
			return b;
		if (a.length() > b.length())
			return a;
		else
			return b;
	}

	@DefaultValueLong(0l)
	public static long
	sumLong(long a, long b)
	{
		return a+b;
	}

	@DefaultValueInt(0)
	public static int
	sumInt(int a, int b)
	{
		return a+b;
	}

	@DefaultValueDouble(1.0)
	public static double
	sumDouble(double a, double b)
	{
		return a + b;
	}

	/* ================================================================================ */
	/* ================================================================================ */


	@Test
	public void
	sumTestInt()
	{
		Set<Integer> iset = set(1, 10, 12, 8, 7, 4);
		assertEquals(42, (int) reduce (sumInt) i: iset.contains(i));
	}

	@Test
	public void
	sumTestLong()
	{
		Set<Long> lset = set(0x0ffffffffl, 0x2l, 0x5l, 0x1l, 0x8l);
		assertEquals(0x10000000fl, (long) reduce (sumLong) l: lset.contains(l));
	}

	@Test
	public void
	sumTestDouble()
	{
		Set<Double> dset = set(0.25, 0.5, 1.5, 0.75);
		assertEquals(4.0, reduce (sumDouble) d: dset.contains(d), 0.1);
	}

	// // @Test
	// // public void
	// // sumTestInnerDouble()
	// // {
	// // 	Set<Integer> aset = set(1, 2);
	// // 	Set<Double> dset = set(0.25, 0.5, 1.5, 0.75);
	// // 	Object r = query(Map.get(int key) == double value) : aset.contains(key) && value == key * reduce (sumDouble) d: dset.contains(d);
	// // System.err.println(r);

	// // }

	// @Test
	// public void
	// sumTestInnerDouble2()
	// {
	// 	Set<Double> dset = set(0.25, 0.5, 1.5, 0.75);
	// 	Object r = query(Map.get(int key) == double value) : range(1,10).contains(key) && value == key * reduce (sumDouble) d: dset.contains(d);
	// System.err.println(r);
	// throw new RuntimeException();
	// }

	@Test
	public void
	productTestIntRemote()
	{
		B.t();
	}

	@Test
	public void
	sumTestLongestString()
	{
		Set<String> strings = set("blurb", "longest-string", "foo", "bar", "quux");
		assertEquals("longest-string", reduce (longestString) s: strings.contains(s));
	}

	@Test
	public void
	countTest()
	{
		Set<String> strings = set("blurb", "longest-string", "foo", "bar", "quux");
		assertEquals(5, reduce (sumInt) byte i over s: strings.contains(s) && i == 1);
	}

	@Test
	public void
	countTestMulti()
	{
		Set<String> strings = set("blurb", "longest-string", "foo", "bar", "quux");
		assertEquals(5 * 256 /* byte */, reduce (sumInt) byte i over s, byte b: strings.contains(s) && i == 1);
	}

	static class A
	{
		@DefaultValueInt(1)
		public static int
		productInt(int alpha, int bravo)
		{
			return alpha * bravo;
		}
	}

	static class B
	{
		static void t()
		{
			Set<Integer> iset = set(1, 2, 4, 8, 256);
			assertEquals(16384, (int) reduce (A.productInt) i: iset.contains(i));
		}
	}
}
