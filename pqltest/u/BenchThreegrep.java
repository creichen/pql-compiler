package pqltest.u;
import static edu.umass.pql.Query;
import static edu.umass.pql.Query.range;
import java.util.*;
import org.junit.*;
import static org.junit.Assert.*;

public class BenchThreegrep
{
	public Object
	compute()
	{
		byte[][] array = Generator.data_array;
		final int RECORD_SIZE = Generator.RECORD_SIZE;

		Object result = query(Set.contains(byte[] ba)): exists i: array[i] == ba
			&& exists j:
			   ba[j] == ((byte)'0')
			&& ba[j + 1] == ((byte)'1')
			&& ba[j + 2] == ((byte)'2')
			&& range(0, RECORD_SIZE - 3).contains(j)
				   ;

		return result;
	}


	@Test
	public void
	test()
	{
		Generator.init();
		final Object result = compute();
		final Object bresult = manualCompute();
		if (!result.equals(bresult)) {
			System.err.println("MISMATCH!");
			System.err.println("Expected: " + bresult);
			System.err.println("Actual  : " + result);
		}
		assertTrue(result.equals(bresult));
	}


	public Object
	manualCompute()
	{
		final HashSet<byte[]> results = new HashSet<byte[]>();

		for (byte[] ba : Generator.data_array) {
			for (int k = 0; k <= Generator.RECORD_SIZE - 3; k++)
				if (ba[k] == '0' && ba[k + 1] == '1' && ba[k + 2] == '2') {
					results.add(ba);
					break;
				}
		}

		return results;
	}

	public static class Generator
	{
		public static final int RECORD_SIZE = 100;
		public static byte[][] data_array;
		public static HashSet<byte[]> data_set;

		public static void init()
		{
			data_array = generateData(RECORD_SIZE, 10000);
			data_set = set(data_array);
		}

		public static HashSet<byte[]>
		set(byte[][] data)
		{
			HashSet<byte[]> retval = new HashSet<byte[]>();
			for (byte[] doc : data)
				retval.add(doc);
			return retval;
		}

		public static byte[][]
		generateData(int size, int count)
		{
			byte[][] data = new byte[count][];
			Random rand = new Random(42);

			for (int i = 0; i < count; i++) {
				byte[] d = new byte[size];
				for (int j = 0; j < RECORD_SIZE; j++)
					d[j] = (byte) (32 + rand.nextInt(64));
				data[i] = d;
			}

			return data;
		}
	}

}
