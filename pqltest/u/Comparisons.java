package pqltest.u;
import org.junit.*;
import static edu.umass.pql.Query;
import static org.junit.Assert.*;
import java.util.*;

public class Comparisons
{
	public int[] ints = new int[] { 1, 2, 3, 4, 5 };
	final String foo = "foo";
	public String[] strings = new String[] { new String(foo) };

	@Test
	public void
	testLTE()
	{
		assertTrue(forall x: ints[x] <= 5);
		assertTrue(forall x: ints[x] <= 5.0);
		assertFalse(forall x: ints[x] <= 4);
		assertFalse(forall x: ints[x] <= 4.5);
	}

	@Test
	public void
	testGTE()
	{
		assertTrue(forall x: ints[x] >= 1);
		assertTrue(forall x: ints[x] >= 1.0);
		assertFalse(forall x: ints[x] >= 2);
		assertFalse(forall x: ints[x] >= 1.5);
	}

	@Test
	public void
	testNE()
	{
		assertTrue(forall x: ints[x] != 7);
		assertFalse(forall x: ints[x] != 3);
	}

	@Test
	public void
	testStringEq()
	{
		assertTrue(forall x: strings[x] == foo);
	}

	@Test
	public void
	testStringNeq()
	{
		String[] other_strings = new String[] { "alpha", "bravo", "charlie" };
		assertTrue(forall x: other_strings[x] != foo);
		assertFalse(forall x: strings[x] != foo);
	}

}
