#! /bin/bash

# if [ ! -e hadoop ]; then
#     echo "Can't build:  install (or symlink) your hadoop installation to './hadoop' ."
#     echo "Tested with hadoop-0.20.205.0 ."
#     exit 1
# fi

PQL_SOURCE="benchmarks/threegrep/PQL.java benchmarks/bonus/PQL.java benchmarks/webgraph/PQL.java benchmarks/idf/PQL.java"
BENCHMARK_THREEGREP="benchmarks/threegrep/Generator.java benchmarks/threegrep/Manual.java"
BENCHMARK_BONUS="benchmarks/bonus/Generator.java benchmarks/bonus/Manual.java benchmarks/bonus/SQLBench.java"
BENCHMARK_WEBGRAPH="benchmarks/webgraph/Manual.java benchmarks/webgraph/Generator.java benchmarks/webgraph/SQLBench.java"
BENCHMARK_IDF="benchmarks/idf/Manual.java benchmarks/idf/SQLBench.java benchmarks/idf/Hadoop.java"
BENCHMARKS="${BENCHMARK_THREEGREP} ${BENCHMARK_BONUS} ${BENCHMARK_WEBGRAPH} ${BENCHMARK_IDF}"
BENCHMARKS_JAR=benchmarks.jar
HADOOP=`echo ./hadoop/hadoop-core-0.20.*.0.jar`:`echo ./hadoop/lib/commons-logging-1.*.jar`:`echo ./hadoop/lib/commons-logging-api-1.*.jar`:`echo ./hadoop/lib/log4j-1.*.jar`:`echo ./hadoop/lib/jackson-mapper-asl-1.*.jar`:`echo ./hadoop/lib/jackson-core-asl-1.*.jar`:`echo ./hadoop/lib/avro-1.*.jar`:`echo ./hadoop/lib/commons-httpclient-3.*.jar`:`echo ./hadoop/lib/commons-configuration-*.jar`:`echo ./hadoop/lib/commons-lang-*.jar`
export HADOOP_LOG_DIR=`pwd`/Hadoop-run/logs
export HADOOP_CONF_DIR=`pwd`/Hadoop-run/conf
DRIVERS="./drivers/postgresql.jar:./drivers/mysql.jar:${HADOOP}"
if  javac -classpath ${CLASSPATH}:${DRIVERS}:../pqlib/pqlib.jar benchmarks/GeneratorBase.java benchmarks/Evaluator.java benchmarks/*/Generator.java \
    && ./dist/bootstrap/bin/javac -printpqlgen  -classpath ${HADOOP}:${CLASSPATH}:${DRIVERS}:../pqlib/pqlib.jar ${PQL_SOURCE} ${BENCHMARKS} \
    && javac -classpath ${CLASSPATH}:${DRIVERS}:../pqlib/pqlib.jar benchmarks/*.java benchmarks/*/Generator.java benchmarks/*/Hadoop.java; then
    echo "Compile OK"
else
    exit 1
fi
jar cvf ${BENCHMARKS_JAR} `find benchmarks -name "*.class"`

BENCHMARKS=`java  -classpath ${CLASSPATH}:.:${DRIVERS}:../pqlib/pqlib.jar benchmarks.Bench list benchmarks`
EVALS=`java  -classpath ${CLASSPATH}:.:${DRIVERS}:../pqlib/pqlib.jar benchmarks.Bench list evaluators`

if [ x$1 == x--help ]; then
    echo "Usage: $0 <benchmarks> <evaluators>"
    echo "benchmarks: ${BENCHMARKS}" | tr '\n' ,
    echo
    echo "evaluators: ${EVALS}" | tr '\n' ,
    echo
    echo "To run multiple benchmarks and evaluators, separate them by commas."
    exit 0
fi

if [ x$1 != x ]; then
    BENCHMARKS="$1"
fi

if [ x$2 != x ]; then
    EVALS="$2"
fi

shift 2;

echo "BENCH\t= ${BENCHMARKS}" | tr '\n' ','
echo ""
echo "EVAL\t= ${EVALS}" | tr '\n' ','
echo ""

for BENCH in $BENCHMARKS; do
    for EVAL in $EVALS; do
	echo java -Xmx2048M -Xms2048M -classpath ${CLASSPATH}:.:${DRIVERS}:../pqlib/pqlib.jar benchmarks.Bench $@ run ${BENCH} ${EVAL}
	java -Xmx2048M -Xms2048M -classpath ${CLASSPATH}:.:${DRIVERS}:../pqlib/pqlib.jar benchmarks.Bench $@ run ${BENCH} ${EVAL}
    done
done
