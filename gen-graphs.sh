#! /bin/bash

BENCHMARKS="bonus threegrep webgraph wordcount"
MONO_EVALS="postgresql mysql manual"
POLY_EVALS="hadoop pql para-manual"

ONEPLOTS="hadoop postgresql mysql pql para-manual manual"
SERIESPLOTS="pql para-manual manual"

MAX_CPU=1

make_output()
{
    if [ $EVAL == "hadoop" ];		then POS=1; fi
    if [ $EVAL == "postgresql" ];	then POS=2; fi
    if [ $EVAL == "mysql" ];		then POS=3; fi
    if [ $EVAL == "pql" ];		then POS=4; fi
    if [ $EVAL == "manual" ];		then POS=5; fi
    if [ $EVAL == "para-manual" ];	then POS=6; fi

    cat $FNAME | sed 's/finish:(/finish/' | awk '{ printf "%d\t%f\t%f\n", '$CPU', ($3 - $28) / 1000000.0, ($5 - $3) / 1000.0; }' >> ${FULLTIME_FILE}
    cat $FNAME | sed 's/finish:(/finish/' | awk '{ printf "%d\t%f\t%f\n", '$CPU', ($12) / 1000000.0, ($14 - $12) / 1000.0; }' >> ${INITTIME_FILE}
    cat $FNAME | sed 's/finish:(/finish/' | awk '{ printf "%d\t%f\t%f\n", '$CPU', ($20) / 1000000.0, ($22 - $20) / 1000.0; }' >> ${EVALTIME_FILE}

    if [ $CPU -eq 1 ]; then
	cat $FNAME | sed 's/finish:(/finish/' | awk '{ printf "%d\t%f\t%f\n", '$POS', ($3 - $28) / 1000000.0, ($5 - $3) / 1000.0; }' > ${FULLTIME_FILE}.1
	cat $FNAME | sed 's/finish:(/finish/' | awk '{ printf "%d\t%f\t%f\n", '$POS', ($12) / 1000000.0, ($14 - $12) / 1000.0; }' > ${INITTIME_FILE}.1
	cat $FNAME | sed 's/finish:(/finish/' | awk '{ printf "%d\t%f\t%f\n", '$POS', ($20) / 1000000.0, ($22 - $20) / 1000.0; }' > ${EVALTIME_FILE}.1
    fi
}

OCOUNT=6

compute_data() {
  for BENCHMARK in ${BENCHMARKS}; do

    if [ $BENCHMARK == "wordcount" ]; then
	BNAME=idf
    else
	BNAME=$BENCHMARK
    fi

    for EVAL in $POLY_EVALS; do
	FULLTIME_FILE=data/${BENCHMARK}-${EVAL}.full
	INITTIME_FILE=data/${BENCHMARK}-${EVAL}.init
	EVALTIME_FILE=data/${BENCHMARK}-${EVAL}.eval

	rm -f ${FULLTIME_FILE} ${INITTIME_FILE} ${EVALTIME_FILE}
	rm -f ${FULLTIME_FILE}.1 ${INITTIME_FILE}.1 ${EVALTIME_FILE}.1

	for CPU in `seq 1 256`; do
	    FNAME=${BNAME}-${EVAL}-${CPU}

	    if [ -e $FNAME ]; then
		if [ $CPU -gt $MAX_CPU ]; then
		    MAX_CPU=$CPU
		fi

		make_output;
	    fi
	done
    done

    for EVAL in $MONO_EVALS; do
	FULLTIME_FILE=data/${BENCHMARK}-${EVAL}.full
	INITTIME_FILE=data/${BENCHMARK}-${EVAL}.init
	EVALTIME_FILE=data/${BENCHMARK}-${EVAL}.eval

	rm -f ${FULLTIME_FILE} ${INITTIME_FILE} ${EVALTIME_FILE}

	FNAME=${BNAME}-${EVAL}

	for CPU in 1 ${MAX_CPU}; do
	    if [ -e $FNAME ]; then
		make_output;
	    fi
	done
    done
  done
}

mkdir data
echo "Generating..."

compute_data;

mkdir graphs
echo "Plotting ${OCOUNT}..."

# First plot:  one-core overview with error bars
for BENCH in ${BENCHMARKS}; do
    CONF="plot.conf"
    echo "set terminal pdfcairo dashed;" > $CONF
    echo "set output \"graphs/${BENCH}-singlethreaded.pdf\"" >> $CONF
    #echo "set xtics 1,1;" >> $CONF
    echo "set xtics (\"hadoop\" 1, \"postgresql\" 2, \"mysql\" 3, \"pql\" 4, \"para-manual\" 5, \"manual\" 6)" >> $CONF
    echo "set style fill pattern 1;" >> $CONF
    echo "set log y 10" >> $CONF
    echo "set nokey" >> $CONF
    printf "%s" "plot [0.5:${OCOUNT}.5] [1:] " >> $CONF

    unset SUCCESS

    for EVAL in ${ONEPLOTS}; do
	BASEFILE=data/${BENCH}-${EVAL}.full.1
	EVALFILE=data/${BENCH}-${EVAL}.eval.1
	C=red
	if [ -e $BASEFILE ]; then
	    if [ x$SUCCESS == xtrue ]; then
		printf ","  >> $CONF
	    fi
	    SUCCESS=true
	    printf "%s" " \""$BASEFILE"\" title \"\" with boxerrorbars fs empty linecolor rgbcolor \"black\", " >> $CONF
	    printf "%s" " \""$EVALFILE"\" title \"$EVAL\" with boxerrorbars linecolor rgbcolor \"black\" fs pattern" >> $CONF
	fi
    done
    echo ";" >> $CONF
    if [ x$SUCCESS == xtrue ]; then
	gnuplot $CONF
    fi
done

# Second plot:  time series for all CPUs
for BENCH in ${BENCHMARKS}; do
    CONF="plot.conf"
    echo "set terminal pdfcairo dashed;" > $CONF
    echo "set output \"graphs/${BENCH}-cores.pdf\"" >> $CONF
    echo "MAX_CPU = $MAX_CPU"
    if [ $MAX_CPU == 12 ]; then
	echo "set xtics (\"1\" 1, \"2\" 2, \"4\" 4, \"6\" 6, \"12\" 12)" >> $CONF
    else
	echo "set xtics (\"1\" 1, \"2\" 2, \"4\" 4, \"8\" 8, \"16\" 16, \"32\" 32, \"64\" 64)" >> $CONF
    fi
    echo "set log x 2" >> $CONF
    echo "set key box" >> $CONF
    echo "set key left bottom" >> $CONF
    echo "set title \"${BENCH}\"" >> $CONF
    echo "set grid" >> $CONF
    echo "set xlabel \"Number of Java threads\"" >> $CONF
    echo "set ylabel \"Average execution time [ms]\"" >> $CONF
    printf "%s" "plot [0.75:$(((MAX_CPU * 4) / 3))] [0:] " >> $CONF

    unset SUCCESS

    for EVAL in ${SERIESPLOTS}; do
	FILE=data/${BENCH}-${EVAL}.full
	if [ -e $FILE ]; then
	    if [ x$SUCCESS == xtrue ]; then
		printf ","  >> $CONF
	    fi
	    SUCCESS=true
	    printf "%s" " \""$FILE"\" title \"$EVAL\" with linespoints " >> $CONF
	fi
    done
    echo ";" >> $CONF
    if [ x$SUCCESS == xtrue ]; then
	gnuplot $CONF
    fi
done


#Finally: generate table
# for BENCH in ${BENCHMARKS}; do
#     TABLE="table.tex"
#     echo "\\begin{tabular}{|l|r|r|r

#     for EVAL in ${SERIESPLOTS}; do
# 	FILE=data/${BENCH}-${EVAL}.full
# 	if [ -e $FILE ]; then
# 	    if [ x$SUCCESS == xtrue ]; then
# 		printf ","  >> $CONF
# 	    fi
# 	    SUCCESS=true
# 	    printf "%s" " \""$FILE"\" title \"$EVAL\" with histeps " >> $CONF
# 	fi
#     done
#     echo ";" >> $CONF
#     if [ x$SUCCESS == xtrue ]; then
# 	gnuplot $CONF
#     fi
# done
